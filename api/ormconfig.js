module.exports = {
  type: process.env.POSTGRES_TYPE,
  host: process.env.POSTGRES_HOST,
  port: process.env.POSTGRES_PORT,
  username: process.env.POSTGRES_USER,
  password: process.env.POSTGRES_PASSWORD,
  database: process.env.POSTGRES_DATABASE,
  entities: ['./src/**/*.entity{.ts,.js}'],
  migrationsTableName: 'migrations',
  migrations: ['./src/config/migrations/*{.ts,.js}'],
  cli: {
    migrationsDir: './src/config/migrations'
  },
  ssl: false,
  synchronize: false
}
