import { Module } from '@nestjs/common'
import { JwtModule } from '@nestjs/jwt'
import { JsonWebTokenService } from '../service/jsonWebToken.service'

// todo get JWT_SECRET from configService

@Module({
  imports: [
    JwtModule.register({
      secret: process.env.JWT_SECRET,
      signOptions: { expiresIn: '7d' }
    })
  ],
  providers: [JsonWebTokenService],
  exports: [JsonWebTokenService]
})
export class JsonWebTokenModule {}
