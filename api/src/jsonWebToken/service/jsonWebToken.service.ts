import { Injectable } from '@nestjs/common'
import { JwtService } from '@nestjs/jwt'
import UserTokenVerifiedDto from '../dto/user-token-verified.dto'

@Injectable()
export class JsonWebTokenService {
  constructor(private jwtService: JwtService) {}

  sign(id: string): string {
    const payload = { sub: id }
    return this.jwtService.sign(payload)
  }

  verify(token: string): UserTokenVerifiedDto {
    return this.jwtService.verify(token)
  }
}
