import { Module } from '@nestjs/common'
import { AuthService } from '../service/auth.service'
import { UserModule } from '../../user/module/user.module'
import { HashingModule } from '../../hashing/module/hashing.module'
import { JsonWebTokenModule } from '../../jsonWebToken/module/jsonWebToken.module'
import { LocalStrategy } from '../strategy/local/local.strategy'
import { JwtStrategy } from '../strategy/jwt/jwt.strategy'
import { AuthController } from '../controller/auth.controller'

@Module({
  imports: [UserModule, HashingModule, JsonWebTokenModule],
  exports: [AuthService],
  providers: [AuthService, LocalStrategy, JwtStrategy],
  controllers: [AuthController]
})
export class AuthModule {}
