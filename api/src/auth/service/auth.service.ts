import {
  Injectable,
  NotFoundException,
  ForbiddenException
} from '@nestjs/common'
import { JsonWebTokenService } from '../../jsonWebToken/service/jsonWebToken.service'
import { HashingService } from '../../hashing/service/hashing.service'
import { UserService } from '../../user/service/user.service'
import LoginInput from '../../user/dto/login.input'
import UserEntity from '../../user/entity/user.entity'
import LoginType from '../../user/type/login.type'

@Injectable()
export class AuthService {
  constructor(
    private hashingService: HashingService,
    private jsonWebTokenService: JsonWebTokenService,
    private userService: UserService
  ) {}

  async validateUser(input: LoginInput): Promise<UserEntity> {
    const user = await this.userService.findUserByEmail(input.email)

    if (!user) {
      throw new NotFoundException('User with this email not found')
    }

    const isPasswordCorrect = this.hashingService.compareSync({
      str: input.password,
      hash: user.password
    })

    if (!isPasswordCorrect) {
      throw new ForbiddenException('Password not correct')
    }

    return user
  }

  // aka login ???
  async getSecretById(id: string): Promise<string> {
    const secret = this.jsonWebTokenService.sign(id)

    if (!secret) {
      throw new ForbiddenException('Unable to generate secret')
    }

    return secret
  }

  // aka verify ???
  async findUserByToken(token: string): Promise<UserEntity> {
    const { sub: id } = this.jsonWebTokenService.verify(token)

    const user = await this.userService.findUserById(id)

    if (!user) {
      throw new NotFoundException('User with this email not found')
    }

    return user
  }
}
