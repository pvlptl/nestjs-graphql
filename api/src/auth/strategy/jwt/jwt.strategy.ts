import { ExtractJwt, Strategy } from 'passport-jwt'
import { PassportStrategy } from '@nestjs/passport'
import { Injectable, UnauthorizedException } from '@nestjs/common'
import UserTokenVerifiedDto from '../../../jsonWebToken/dto/user-token-verified.dto'
import { UserService } from '../../../user/service/user.service'
// import { ConfigService } from '@nestjs/config'

// todo get JWT_SECRET from configService

@Injectable()
export class JwtStrategy extends PassportStrategy(Strategy) {
  constructor(
    private readonly userService: UserService // private configService: ConfigService
  ) {
    super({
      jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken(),
      ignoreExpiration: false,
      // secretOrKey: this.configService.get<string>('JWT_SECRET')
      secretOrKey: process.env.JWT_SECRET
    })
  }

  async validate(data: UserTokenVerifiedDto) {
    const user = await this.userService.findUserById(data.sub)

    if (!user) {
      throw new UnauthorizedException()
    }

    return user
  }
}
