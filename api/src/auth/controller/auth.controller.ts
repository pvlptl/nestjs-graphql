import { Controller, Post, UseGuards, ForbiddenException } from '@nestjs/common'
import { Public } from '../../common/decorators/public.decorator'
import UserEntity from '../..//user/entity/user.entity'
import { LocalAuthGuard } from '../guard/local/local-auth.guard'
import { AuthService } from '../service/auth.service'
import { UserDecorator } from '../../user/decorator/user.decorator'
import { LoginResponseDto } from '../dto/login-response.dto'

@Controller('auth')
export class AuthController {
  constructor(private readonly authService: AuthService) {}

  @Public()
  @UseGuards(LocalAuthGuard)
  @Post('/login')
  async login(@UserDecorator() user: UserEntity): Promise<LoginResponseDto> {
    const secret = await this.authService.getSecretById(user.id)

    if (!secret) {
      throw new ForbiddenException('Unable to get secret')
    }

    return {
      secret
    }
  }
}
