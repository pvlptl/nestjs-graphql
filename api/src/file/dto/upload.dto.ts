export class UploadDto {
  readonly path!: string
  readonly mimetype!: string
  readonly buffer!: Buffer
}
