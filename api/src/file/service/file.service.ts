import { Injectable } from '@nestjs/common'
import { config, S3 } from 'aws-sdk'
import { UploadDto } from '../dto/upload.dto'
import imageExtensions from '../imageExtensions'
import { ConfigService } from '@nestjs/config'

@Injectable()
export class FileService {
  constructor(private configService: ConfigService) {
    config.update({
      accessKeyId: this.configService.get<string>('AWS_ACCESS_KEY'),
      secretAccessKey: this.configService.get<string>('AWS_SECRET_KEY'),
      region: this.configService.get<string>('AWS_REGION')
    })
  }

  async upload(dto: UploadDto) {
    const { mimetype, buffer, path } = dto
    const s3 = new S3()

    return await s3
      .upload({
        Bucket: this.configService.get<string>('AWS_S3_BUCKET_NAME'),
        Body: buffer,
        Key: path,
        ACL: 'public-read',
        ContentType: mimetype
      })
      .promise()
  }

  async remove(key: string) {
    const s3 = new S3()

    await s3
      .deleteObject({
        Bucket: this.configService.get<string>('AWS_S3_BUCKET_NAME'),
        Key: key
      })
      .promise()
  }

  isValidImage(file: Express.Multer.File): boolean {
    return file && imageExtensions.includes(file.mimetype.split('/')[1])
  }
}
