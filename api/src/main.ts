import { NestFactory } from '@nestjs/core'
import { ValidationPipe } from '@nestjs/common'
import cookieParser from 'cookie-parser'

// import helmet from 'helmet'
// import { json, urlencoded } from 'body-parser'

import { AppModule } from './app.module'

async function bootstrap() {
  const app = await NestFactory.create(AppModule)

  app.useGlobalPipes(
    new ValidationPipe({
      whitelist: true,
      transform: true,
      transformOptions: {
        enableImplicitConversion: true
      }
    })
  )

  app.setGlobalPrefix(process.env.GLOBAL_PREFIX)
  app.enableCors({
    origin: process.env.WEB_URL,
    credentials: true
  })
  app.use(cookieParser(process.env.COOKIE_PARSER_SECRET))

  // app.use(helmet())
  // app.use(json({ limit: '50mb' }))
  // app.use(urlencoded({ limit: '50mb', extended: true }))

  await app.listen(process.env.PORT)

  console.log(`Application is running on: ${await app.getUrl()}`)
}

bootstrap()
