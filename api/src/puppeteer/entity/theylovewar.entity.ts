import { Column, Entity, PrimaryGeneratedColumn } from 'typeorm'

import { Field, ID, ObjectType } from '@nestjs/graphql'

@ObjectType('TheyLoveWar')
@Entity('theylovewar')
export default class TheylovewarEntity {
  @Field(() => ID)
  @PrimaryGeneratedColumn('uuid')
  id: string

  @Field()
  @Column()
  name: string

  @Field()
  @Column()
  comment: string

  @Field()
  @Column()
  country_code: string

  @Field()
  @Column()
  main_image: string
}
