import { Module } from '@nestjs/common'
import PuppeteerService from '../service/puppeteer.service'
import PuppeteerResolver from '../resolver/puppeteer.resolver'
import { TypeOrmModule } from '@nestjs/typeorm'
import TheylovewarRepository from '../repository/theylovewar.repository'

@Module({
  imports: [TypeOrmModule.forFeature([TheylovewarRepository])],
  providers: [PuppeteerResolver, PuppeteerService],
  controllers: [],
  exports: [PuppeteerService]
})
export default class PuppeteerModule {}
