import { Query, Resolver } from '@nestjs/graphql'
import PuppeteerService from '../service/puppeteer.service'
import TheyLoveWarNodeDto from '../dto/they-love-war-node.dto'

@Resolver()
export default class PuppeteerResolver {
  constructor(private readonly puppeteerService: PuppeteerService) {}

  @Query(() => [TheyLoveWarNodeDto])
  async theyLoveWar(): Promise<TheyLoveWarNodeDto[]> {
    return this.puppeteerService.theyLoveWar()
  }
}
