import { Injectable } from '@nestjs/common'
// import puppeteer from 'puppeteer'
import axios from 'axios'
import cheerio from 'cheerio'
import TheyLoveWarNodeDto from '../dto/they-love-war-node.dto'
import TheyLoveWarSocialLinkDto from '../dto/they-love-war-social-link.dto'
import TheyLoveWarImageDto from '../dto/they-love-war-image.dto'
import { v4 as uuidv4 } from 'uuid'
import { InjectRepository } from '@nestjs/typeorm'
import TheylovewarRepository from '../repository/theylovewar.repository'

// todo get result and save to db
// todo cron job

@Injectable()
export default class PuppeteerService {
  constructor(
    @InjectRepository(TheylovewarRepository)
    private readonly theylovewarRepository: TheylovewarRepository
  ) {}

  public async theyLoveWar(): Promise<TheyLoveWarNodeDto[]> {
    // const browser = await puppeteer.launch()
    // const page = await browser.newPage()
    // await page.goto('https://example.com')
    // await page.screenshot({ path: '/screenshot.png' })
    // await browser.close()

    const normalizeCountry = (str = ''): string => {
      let res = ''

      if (str.includes('ru')) res = 'ru'
      else if (str.includes('uk')) res = 'ua'

      return res
    }

    const getSocialLinkType = (str = ''): string => {
      let res = ''

      if (str.includes('instagram')) res = 'instagram'
      if (str.includes('facebook')) res = 'facebook'
      else if (str.includes('youtube')) res = 'youtube'
      else if (str.includes('vk')) res = 'vkontakte'
      else if (str.includes('t.me')) res = 'telegram'

      return res
    }

    const DOMAIN_URL = 'https://theylovewar.com'

    const nodes: TheyLoveWarNodeDto[] = []

    const { data: html } = await axios.get(DOMAIN_URL)
    const $ = cheerio.load(html)

    $('.brand', html).each((i, brand) => {
      const name = $('.name', brand).text().replace(/\n/g, '').trim()

      const country = $('.name img', brand).attr('src')

      const comment = $('.comment', brand).text().replace(/\n/g, '').trim()

      const images: TheyLoveWarImageDto[] = []
      let main_image = ''
      $('.a-image', brand).each((idx, x) => {
        if (idx === 0) main_image = DOMAIN_URL + $('img', x).attr('src')

        images.push({
          id: uuidv4(),
          src: DOMAIN_URL + $('img', x).attr('src')
        })
      })

      const social_links: TheyLoveWarSocialLinkDto[] = []
      $('.social_links', brand).each((idx, i) => {
        const url = $('a', i).attr('href')
        if (url && url.length > 0) {
          social_links.push({
            id: uuidv4(),
            url,
            type: getSocialLinkType(url)
          })
        }
      })

      nodes.push({
        id: uuidv4(),
        name,
        comment,
        main_image,
        country_code: normalizeCountry(country),
        social_links,
        images
      })
    })

    for (const node of nodes) {
      const found = await this.theylovewarRepository.findOne({
        where: { name: node.name }
      })

      if (found) {
        await this.theylovewarRepository.update(
          {
            id: found.id
          },
          {
            name: node.name,
            comment: node.comment,
            main_image: node.main_image,
            country_code: node.country_code
          }
        )
      } else {
        await this.theylovewarRepository.createOne({
          name: node.name,
          comment: node.comment,
          main_image: node.main_image,
          country_code: node.country_code
        })
      }
    }

    console.log('PuppeteerService -> theyLoveWar called')

    return nodes
  }
}
