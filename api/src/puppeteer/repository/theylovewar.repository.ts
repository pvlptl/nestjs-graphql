import { EntityRepository, Repository } from 'typeorm'
import { ConflictException } from '@nestjs/common'
import TheylovewarEntity from '../entity/theylovewar.entity'

@EntityRepository(TheylovewarEntity)
export default class TheylovewarRepository extends Repository<TheylovewarEntity> {
  async createOne(input: any): Promise<TheylovewarEntity> {
    const found = await this.findOne({ where: { name: input.name } })

    if (found) {
      throw new ConflictException(
        'TheylovewarEntity with this name already exists'
      )
    }

    const created = this.create(input)

    return (await this.save(created)) as any
  }
}
