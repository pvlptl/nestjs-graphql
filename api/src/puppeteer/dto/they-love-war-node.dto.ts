import { Field, ObjectType } from '@nestjs/graphql'
import TheyLoveWarSocialLinkDto from './they-love-war-social-link.dto'
import TheyLoveWarImageDto from './they-love-war-image.dto'

@ObjectType()
export default class TheyLoveWarNodeDto {
  @Field()
  readonly id: string

  @Field()
  readonly name: string

  @Field()
  readonly comment: string

  @Field()
  readonly country_code: string

  @Field()
  readonly main_image: string

  @Field(() => [TheyLoveWarSocialLinkDto])
  readonly social_links: TheyLoveWarSocialLinkDto[]

  @Field(() => [TheyLoveWarImageDto])
  readonly images: TheyLoveWarImageDto[]
}
