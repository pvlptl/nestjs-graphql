import { Field, ObjectType } from '@nestjs/graphql'

@ObjectType()
export default class TheyLoveWarSocialLinkDto {
  @Field()
  readonly id: string

  @Field()
  readonly url: string

  @Field()
  readonly type: string
}
