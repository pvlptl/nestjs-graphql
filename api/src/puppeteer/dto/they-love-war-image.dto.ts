import { Field, ObjectType } from '@nestjs/graphql'

@ObjectType()
export default class TheyLoveWarImageDto {
  @Field()
  readonly id: string

  @Field()
  readonly src: string
}
