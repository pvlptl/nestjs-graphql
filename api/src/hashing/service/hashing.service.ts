import { Injectable } from '@nestjs/common'
import { toNumber } from 'lodash'
import { hashSync, compareSync } from 'bcryptjs'
import HashSyncDto from '../dto/hash-sync.dto'
import CompareSyncDto from '../dto/compare-sync.dto'
import { ConfigService } from '@nestjs/config'

@Injectable()
export class HashingService {
  constructor(private configService: ConfigService) {}

  compareSync(dto: CompareSyncDto): boolean {
    const { str, hash } = dto
    return compareSync(str, hash)
  }

  hashSync(dto: HashSyncDto): string {
    const { str } = dto
    return hashSync(str, toNumber(this.configService.get<string>('SALT')))
  }
}
