import { IsNotEmpty } from 'class-validator'

export default class HashSyncDto {
  @IsNotEmpty()
  readonly str!: string
}
