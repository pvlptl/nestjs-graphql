import { IsNotEmpty } from 'class-validator'

export default class CompareSyncDto {
  @IsNotEmpty()
  readonly str!: string

  @IsNotEmpty()
  readonly hash!: string
}
