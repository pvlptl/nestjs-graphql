import {
  MigrationInterface,
  QueryRunner,
  TableColumn,
  TableForeignKey
} from 'typeorm'

export class addUserIdToUserAvatar1636978343775 implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.addColumn(
      'user-avatar',
      new TableColumn({
        name: 'userId',
        type: 'uuid',
        isNullable: true
      })
    )

    await queryRunner.createForeignKey(
      'user-avatar',
      new TableForeignKey({
        columnNames: ['userId'],
        referencedColumnNames: ['id'],
        referencedTableName: 'user',
        onDelete: 'RESTRICT'
      })
    )
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    const table = await queryRunner.getTable('user-avatar')

    if (table) {
      const foreignKey = table.foreignKeys.find(
        fk => fk.columnNames.indexOf('userId') !== -1
      )

      if (foreignKey) {
        await queryRunner.dropForeignKey('userId', foreignKey)
      }
    }

    await queryRunner.dropColumn('user-avatar', 'userId')
  }
}
