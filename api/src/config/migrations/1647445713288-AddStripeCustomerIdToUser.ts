import { MigrationInterface, QueryRunner, TableColumn } from 'typeorm'

export class AddStripeCustomerIdToUser1647445713288
  implements MigrationInterface
{
  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.addColumn(
      'user',
      new TableColumn({
        name: 'stripeCustomerId',
        type: 'varchar',
        isNullable: true,
        isUnique: true
      })
    )
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.dropColumn('user', 'stripeCustomerId')
  }
}
