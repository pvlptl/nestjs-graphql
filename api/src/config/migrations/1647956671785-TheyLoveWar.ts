import { MigrationInterface, QueryRunner, Table } from 'typeorm'

export class TheyLoveWar1647956671785 implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<void> {
    return queryRunner.createTable(
      new Table({
        name: 'theylovewar',
        columns: [
          {
            name: 'id',
            type: 'uuid',
            isPrimary: true,
            isGenerated: true,
            isUnique: true,
            isNullable: false,
            generationStrategy: 'uuid',
            default: `uuid_generate_v4()`
          },
          {
            name: 'name',
            type: 'varchar'
          },
          {
            name: 'comment',
            type: 'varchar'
          },
          {
            name: 'country_code',
            type: 'varchar',
            length: '2'
          },
          {
            name: 'main_image',
            type: 'varchar'
          },
          {
            name: 'createdAt',
            type: 'timestamp',
            default: 'CURRENT_TIMESTAMP',
            isNullable: false
          },
          {
            name: 'updatedAt',
            type: 'timestamp',
            default: 'CURRENT_TIMESTAMP',
            isNullable: false
          }
        ]
      }),
      true
    )
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    return queryRunner.dropTable('theylovewar')
  }
}
