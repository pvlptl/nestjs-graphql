import { MigrationInterface, QueryRunner, Table } from 'typeorm'

export class createUserTable1636466631149 implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<void> {
    return queryRunner.createTable(
      new Table({
        name: 'user',
        columns: [
          {
            name: 'id',
            type: 'uuid',
            isPrimary: true,
            isGenerated: true,
            isUnique: true,
            isNullable: false,
            generationStrategy: 'uuid',
            default: `uuid_generate_v4()`
          },
          {
            name: 'email',
            type: 'varchar',
            length: '320',
            isNullable: false,
            isUnique: true
          },
          {
            name: 'password',
            type: 'varchar',
            isNullable: false
          },
          {
            name: 'firstName',
            type: 'varchar',
            length: '55'
          },
          {
            name: 'lastName',
            type: 'varchar',
            length: '55'
          },
          {
            name: 'createdAt',
            type: 'timestamp',
            default: 'CURRENT_TIMESTAMP',
            isNullable: false
          },
          {
            name: 'updatedAt',
            type: 'timestamp',
            default: 'CURRENT_TIMESTAMP',
            isNullable: false
          }
        ]
      }),
      true
    )
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    return queryRunner.dropTable('user')
  }
}
