import { MigrationInterface, QueryRunner, Table } from 'typeorm'

export class userAvatarTable1636978160572 implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<void> {
    return queryRunner.createTable(
      new Table({
        name: 'user-avatar',
        columns: [
          {
            name: 'id',
            type: 'uuid',
            isPrimary: true,
            isGenerated: true,
            isUnique: true,
            isNullable: false,
            generationStrategy: 'uuid',
            default: `uuid_generate_v4()`
          },
          {
            name: 'key',
            type: 'varchar',
            isNullable: false,
            isUnique: true
          },
          {
            name: 'url',
            type: 'varchar',
            isNullable: false,
            isUnique: true
          },
          {
            name: 'createdAt',
            type: 'timestamp',
            default: 'CURRENT_TIMESTAMP',
            isNullable: false
          },
          {
            name: 'updatedAt',
            type: 'timestamp',
            default: 'CURRENT_TIMESTAMP',
            isNullable: false
          }
        ]
      }),
      true
    )
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    return queryRunner.dropTable('todo')
  }
}
