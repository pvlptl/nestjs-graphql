import {
  MigrationInterface,
  QueryRunner,
  TableColumn,
  TableForeignKey
} from 'typeorm'

export class addUseridToTodo1636648082124 implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.addColumn(
      'todo',
      new TableColumn({
        name: 'userId',
        type: 'uuid',
        isNullable: true
      })
    )

    await queryRunner.createForeignKey(
      'todo',
      new TableForeignKey({
        columnNames: ['userId'],
        referencedColumnNames: ['id'],
        referencedTableName: 'user',
        onDelete: 'RESTRICT'
      })
    )
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    const table = await queryRunner.getTable('todo')

    if (table) {
      const foreignKey = table.foreignKeys.find(
        fk => fk.columnNames.indexOf('userId') !== -1
      )

      if (foreignKey) {
        await queryRunner.dropForeignKey('userId', foreignKey)
      }
    }

    await queryRunner.dropColumn('todo', 'userId')
  }
}
