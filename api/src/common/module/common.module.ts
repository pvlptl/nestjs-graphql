import { Global, Module } from '@nestjs/common'
import { CommonService } from '../service/common.service'

/*
  Making everything global is not a good design decision.
  Global modules are available to reduce the amount of necessary boilerplate.
  The imports array is generally the preferred way to make the module's API available to consumers.
  https://docs.nestjs.com/modules#global-modules
 */

@Global()
@Module({
  providers: [CommonService],
  exports: [CommonService]
})
export class CommonModule {}
