import { forwardRef, Module } from '@nestjs/common'
import { TypeOrmModule } from '@nestjs/typeorm'
import TodoRepository from '../repository/todo.repository'
import TodoResolver from '../resolver/todo.resolver'
import { TodoService } from '../service/todo.service'
import { UserModule } from '../../user/module/user.module'
import { StripeModule } from '../../stripe/module/stripe.module'

@Module({
  imports: [
    TypeOrmModule.forFeature([TodoRepository]),
    forwardRef(() => UserModule),
    StripeModule
  ],
  providers: [TodoResolver, TodoService],
  exports: [TodoService]
})
export class TodoModule {}
