import {
  Column,
  CreateDateColumn,
  Entity,
  ManyToOne,
  PrimaryGeneratedColumn,
  UpdateDateColumn
} from 'typeorm'

import { Field, ID, ObjectType } from '@nestjs/graphql'
import UserEntity from '../../user/entity/user.entity'

@ObjectType('Todo')
@Entity('todo')
export default class TodoEntity {
  @Field(() => ID)
  @PrimaryGeneratedColumn('uuid')
  id: string

  @Field()
  @Column()
  name: string

  @Field()
  @Column()
  isCompleted: boolean

  @Field()
  @CreateDateColumn()
  createdAt: Date

  @Field()
  @UpdateDateColumn()
  updatedAt: Date

  @Field(() => ID)
  @Column()
  userId: string

  @Field(() => UserEntity)
  @ManyToOne(() => UserEntity, user => user.todos)
  user: UserEntity
}
