import { IsOptional } from 'class-validator'
import { Field, InputType } from '@nestjs/graphql'

@InputType()
export default class TodoFiltersDto {
  @Field({ nullable: true })
  @IsOptional()
  readonly limit?: number

  @Field({ nullable: true })
  @IsOptional()
  readonly offset?: number
}
