import { Field, InputType } from '@nestjs/graphql'
import { IsOptional, MaxLength, MinLength } from 'class-validator'

@InputType()
export default class UpdateTodoByIdInput {
  @Field({ nullable: true })
  @IsOptional()
  @MinLength(5)
  @MaxLength(55)
  readonly name?: string

  @Field({ nullable: true })
  @IsOptional()
  readonly isCompleted?: boolean
}
