import { ObjectType } from '@nestjs/graphql'

@ObjectType()
export default class FindUserTodoByIdDto {
  readonly userId: string
  readonly todoId: string
}
