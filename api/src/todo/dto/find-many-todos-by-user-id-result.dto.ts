import { Field, ObjectType } from '@nestjs/graphql'
import TodoEntity from '../entity/todo.entity'

@ObjectType()
export default class FindManyTodosByUserIdResultDto {
  @Field(() => [TodoEntity])
  readonly rows: TodoEntity[]

  @Field()
  readonly count: number
}
