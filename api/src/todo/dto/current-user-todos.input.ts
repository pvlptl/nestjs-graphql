import { Field, InputType } from '@nestjs/graphql'
import { IsOptional } from 'class-validator'
import TodoFiltersDto from './todo-filters.dto'

@InputType()
export default class CurrentUserTodosInput {
  @Field({ nullable: true })
  @IsOptional()
  readonly filters?: TodoFiltersDto

  // we dont wanna allow user to enter user id in schema input when create, instead we get it automatically from auth headers
  readonly userId?: string
}
