import { Field, InputType } from '@nestjs/graphql'
import { IsNotEmpty, IsOptional, MaxLength, MinLength } from 'class-validator'

@InputType()
export default class CreateTodoInput {
  @Field()
  @IsNotEmpty()
  @MinLength(5)
  @MaxLength(55)
  readonly name: string

  @Field({ nullable: true, defaultValue: false })
  @IsOptional()
  readonly isCompleted?: boolean = false

  // we dont wanna allow user to enter user id in schema input when create, instead we get it automatically from auth headers
  readonly userId?: string
}
