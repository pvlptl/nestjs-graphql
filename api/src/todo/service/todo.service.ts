import { ForbiddenException, Injectable } from '@nestjs/common'
import { InjectRepository } from '@nestjs/typeorm'
import CreateTodoInput from '../dto/create-todo.input'
import TodoEntity from '../entity/todo.entity'
import TodoRepository from '../repository/todo.repository'
import CurrentUserTodosInput from '../dto/current-user-todos.input'
import UpdateTodoByIdInput from '../dto/update-todo-by-id.input'
import FindManyTodosByUserIdResultDto from '../dto/find-many-todos-by-user-id-result.dto'
import FindUserTodoByIdDto from '../dto/find-user-todo-by-id.dto'

@Injectable()
export class TodoService {
  constructor(
    @InjectRepository(TodoRepository)
    private readonly todoRepository: TodoRepository
  ) {}

  async createOne(input: CreateTodoInput): Promise<TodoEntity> {
    return this.todoRepository.createOneAndSave(input)
  }

  async deleteById(id: string): Promise<TodoEntity> {
    return this.todoRepository.deleteById(id)
  }

  async updateById(id: string, data: UpdateTodoByIdInput): Promise<TodoEntity> {
    return this.todoRepository.updateById(id, data)
  }

  async findManyByUserId(
    dto: CurrentUserTodosInput
  ): Promise<FindManyTodosByUserIdResultDto> {
    return this.todoRepository.findManyByUserId(dto)
  }

  async findUserTodoById(dto: FindUserTodoByIdDto): Promise<TodoEntity> {
    const todo = await this.todoRepository.findOne({ id: dto.todoId })

    if (dto.userId !== todo.userId) {
      throw new ForbiddenException()
    }

    return todo
  }
}
