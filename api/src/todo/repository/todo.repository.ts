import { EntityRepository, Repository } from 'typeorm'
import { ConflictException } from '@nestjs/common'
import TodoEntity from '../entity/todo.entity'
import CreateTodoInput from '../dto/create-todo.input'
import CurrentUserTodosInput from '../dto/current-user-todos.input'
import UpdateTodoByIdInput from '../dto/update-todo-by-id.input'
import FindManyTodosByUserIdResultDto from '../dto/find-many-todos-by-user-id-result.dto'

@EntityRepository(TodoEntity)
export default class TodoRepository extends Repository<TodoEntity> {
  async createOneAndSave(input: CreateTodoInput): Promise<TodoEntity> {
    const found = await this.findOne({ where: { name: input.name } })

    if (found) {
      throw new ConflictException('Todo with this name already exists')
    }

    const created = this.create(input)

    return this.save(created)
  }

  async findManyByUserId(
    dto: CurrentUserTodosInput
  ): Promise<FindManyTodosByUserIdResultDto> {
    const { userId, filters } = dto

    const limit = filters?.limit || 25
    // const page = filters?.offset || 1
    const offset = filters?.offset || 0

    const [rows, count] = await this.findAndCount({
      where: {
        userId
        // name: ILike(`%${search}%`), ...where
      },
      take: limit,
      // skip: limit * page - limit < 0 ? 0 : limit * page - limit,
      skip: offset,
      order: {
        createdAt: 'DESC'
      }
    })

    return {
      rows,
      count
    }
  }

  async deleteById(id: string): Promise<TodoEntity> {
    const found = await this.findOne({ where: { id } })

    if (!found) {
      throw new ConflictException('Todo not found')
    }

    await this.delete({
      id
    })

    return found
  }

  async updateById(id: string, data: UpdateTodoByIdInput): Promise<TodoEntity> {
    const prevData = await this.findOne({ where: { id } })

    if (!prevData) {
      throw new ConflictException('Todo not found')
    }

    return this.save({
      ...prevData, // existing fields
      ...data // updated fields
    })
  }
}
