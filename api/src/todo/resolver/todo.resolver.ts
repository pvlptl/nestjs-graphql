import {
  Args,
  Mutation,
  Parent,
  Query,
  ResolveField,
  Resolver
} from '@nestjs/graphql'
import { TodoService } from '../service/todo.service'
import TodoEntity from '../entity/todo.entity'
import CreateTodoInput from '../dto/create-todo.input'
import { NotFoundException, UseGuards } from '@nestjs/common'
import { GqlAuthGuard } from '../../auth/guard/gql/gql-auth.guard'
import UpdateTodoByIdInput from '../dto/update-todo-by-id.input'
import { UserDecorator } from '../../user/decorator/user.decorator'
import UserEntity from '../../user/entity/user.entity'
import { UserService } from '../../user/service/user.service'
import TodoFiltersDto from '../dto/todo-filters.dto'
import FindManyTodosByUserIdResultDto from '../dto/find-many-todos-by-user-id-result.dto'
import { BronzePlanGuard } from '../../stripe/guard/bronze-plan.guard'
import { SilverPlanGuard } from '../../stripe/guard/silver-plan.guard'
import { GoldPlanGuard } from '../../stripe/guard/gold-plan.guard'

@Resolver(() => TodoEntity)
export default class TodoResolver {
  constructor(
    private readonly todoService: TodoService,
    private readonly userService: UserService
  ) {}

  @UseGuards(GqlAuthGuard, BronzePlanGuard)
  @Mutation(() => TodoEntity)
  async createTodo(
    @UserDecorator() user: UserEntity,
    @Args('input') input: CreateTodoInput
  ): Promise<TodoEntity> {
    return this.todoService.createOne({
      ...input,
      userId: user.id
    })
  }

  @UseGuards(GqlAuthGuard, GoldPlanGuard)
  @Mutation(() => TodoEntity)
  async deleteTodoById(@Args('id') id: string): Promise<TodoEntity> {
    return this.todoService.deleteById(id)
  }

  @UseGuards(GqlAuthGuard, SilverPlanGuard)
  @Mutation(() => TodoEntity)
  async updateTodoById(
    @Args('id') id: string,
    @Args('data') data: UpdateTodoByIdInput
  ): Promise<TodoEntity> {
    return this.todoService.updateById(id, data)
  }

  // todo make filters argument optional
  @UseGuards(GqlAuthGuard)
  @Query(() => FindManyTodosByUserIdResultDto)
  async getCurrentUserTodos(
    @UserDecorator() user: UserEntity,
    @Args('filters') filters: TodoFiltersDto
  ): Promise<FindManyTodosByUserIdResultDto> {
    return this.todoService.findManyByUserId({ userId: user.id, filters })
  }

  @UseGuards(GqlAuthGuard)
  @Query(() => TodoEntity)
  async findCurrentUserTodoById(
    @UserDecorator() user: UserEntity,
    @Args('id') id: string
  ): Promise<TodoEntity> {
    return this.todoService.findUserTodoById({ userId: user.id, todoId: id })
  }

  @ResolveField(() => UserEntity)
  async user(@Parent() todo: TodoEntity): Promise<UserEntity> {
    const user = await this.userService.findUserById(todo.userId)

    if (!user) {
      throw new NotFoundException('User not found')
    }

    return user
  }
}
