import { Module } from '@nestjs/common'
import { TypeOrmModule } from '@nestjs/typeorm'
import UserAvatarRepository from '../repository/user-avatar.repository'
import { UserAvatarService } from '../service/user-avatar.service'
import { FileModule } from '../../file/module/file.module'

@Module({
  imports: [TypeOrmModule.forFeature([UserAvatarRepository]), FileModule],
  providers: [UserAvatarService],
  exports: [UserAvatarService]
})
export class UserAvatarModule {}
