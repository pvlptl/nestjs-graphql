import { EntityRepository, Repository } from 'typeorm'
import UserAvatarEntity from '../entity/user-avatar.entity'
import CreateUserAvatarDto from '../dto/create-user-avatar.dto'
import { ConflictException } from '@nestjs/common'

@EntityRepository(UserAvatarEntity)
export default class UserAvatarRepository extends Repository<UserAvatarEntity> {
  async createOneAndSave(dto: CreateUserAvatarDto): Promise<UserAvatarEntity> {
    const created = this.create(dto)

    await this.save(created)

    return created
  }

  async deleteById(id: string): Promise<UserAvatarEntity> {
    const found = await this.findOne({ where: { id } })

    if (!found) {
      throw new ConflictException('Avatar not found')
    }

    await this.delete({
      id
    })

    return found
  }
}
