import { ConflictException, Injectable } from '@nestjs/common'
import { InjectRepository } from '@nestjs/typeorm'
import UserAvatarEntity from '../entity/user-avatar.entity'
import UserAvatarRepository from '../repository/user-avatar.repository'
import CreateUserAvatarDto from '../dto/create-user-avatar.dto'
import { FileService } from '../../file/service/file.service'

@Injectable()
export class UserAvatarService {
  constructor(
    @InjectRepository(UserAvatarRepository)
    private readonly userAvatarRepository: UserAvatarRepository,
    private readonly fileService: FileService
  ) {}

  async createOne(dto: CreateUserAvatarDto): Promise<UserAvatarEntity> {
    const found = await this.findByUserId(dto.userId)

    if (found) {
      await this.removeByUserId(dto.userId)
    }

    return this.userAvatarRepository.createOneAndSave(dto)
  }

  async findByUserId(id: string): Promise<UserAvatarEntity | undefined> {
    return this.userAvatarRepository.findOne({ userId: id })
  }

  async removeById(id: string): Promise<UserAvatarEntity> {
    return this.userAvatarRepository.deleteById(id)
  }

  async removeByUserId(id: string): Promise<UserAvatarEntity> {
    const found = await this.findByUserId(id)

    if (!found) {
      throw new ConflictException('Avatar not found')
    }

    await this.fileService.remove(found.key)
    return await this.removeById(found.id)
  }
}
