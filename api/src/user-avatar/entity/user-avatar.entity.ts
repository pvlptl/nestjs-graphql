import {
  Column,
  CreateDateColumn,
  Entity,
  OneToOne,
  PrimaryGeneratedColumn,
  UpdateDateColumn
} from 'typeorm'

import { Field, ID, ObjectType } from '@nestjs/graphql'
import UserEntity from '../../user/entity/user.entity'

@ObjectType('UserAvatar')
@Entity('user-avatar')
export default class UserAvatarEntity {
  @Field(() => ID)
  @PrimaryGeneratedColumn('uuid')
  id: string

  @Field()
  @Column({ unique: true })
  key: string

  @Field()
  @Column({ unique: true })
  url: string

  @Field()
  @CreateDateColumn()
  createdAt: Date

  @Field()
  @UpdateDateColumn()
  updatedAt: Date

  @Field(() => ID)
  @Column()
  userId: string

  @Field(() => UserEntity)
  @OneToOne(() => UserEntity, user => user.avatar)
  user: UserEntity
}
