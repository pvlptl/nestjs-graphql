import { ObjectType } from '@nestjs/graphql'

@ObjectType()
export default class CreateUserAvatarDto {
  readonly userId: string
  readonly key: string
  readonly url: string
}
