import { TypeOrmModule } from '@nestjs/typeorm'
import { GraphQLModule } from '@nestjs/graphql'
import { Module } from '@nestjs/common'
import { ApolloDriver, ApolloDriverConfig } from '@nestjs/apollo'

import { TypeormConfigService } from './typeorm/service/typeorm.service'
import { TodoModule } from './todo/module/todo.module'
import { UserModule } from './user/module/user.module'
import { HashingModule } from './hashing/module/hashing.module'
import { JsonWebTokenModule } from './jsonWebToken/module/jsonWebToken.module'
import { AuthModule } from './auth/module/auth.module'
import { FileModule } from './file/module/file.module'
import { UserAvatarModule } from './user-avatar/module/user-avatar.module'
import { ConfigModule } from '@nestjs/config'
import { StripeModule } from './stripe/module/stripe.module'
import PuppeteerModule from './puppeteer/module/puppeteer.module'
import { CronjobModule } from './cronjob/module/cronjob.module'

@Module({
  imports: [
    ConfigModule.forRoot({
      isGlobal: true
    }),
    TypeOrmModule.forRootAsync({
      useClass: TypeormConfigService,
      inject: [TypeormConfigService]
    }),
    GraphQLModule.forRoot<ApolloDriverConfig>({
      driver: ApolloDriver,
      debug: false,
      playground: true,
      autoSchemaFile: 'schema.gql',
      subscriptions: {
        'graphql-ws': true
      }
    }),
    TodoModule,
    UserModule,
    HashingModule,
    JsonWebTokenModule,
    AuthModule,
    FileModule,
    UserAvatarModule,
    StripeModule,
    PuppeteerModule,
    CronjobModule
  ],
  controllers: [],
  providers: [TypeormConfigService]
})
export class AppModule {}
