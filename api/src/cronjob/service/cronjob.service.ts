import { Injectable } from '@nestjs/common'
import { Cron, CronExpression } from '@nestjs/schedule'
import PuppeteerService from '../../puppeteer/service/puppeteer.service'

@Injectable()
export class CronjobService {
  constructor(private readonly puppeteerService: PuppeteerService) {}

  @Cron(CronExpression.EVERY_DAY_AT_MIDNIGHT)
  async theyLoveWarCron() {
    await this.puppeteerService.theyLoveWar()
  }
}
