import { Module } from '@nestjs/common'
import { ScheduleModule } from '@nestjs/schedule'
import { CronjobService } from '../service/cronjob.service'
import PuppeteerModule from '../../puppeteer/module/puppeteer.module'

@Module({
  imports: [ScheduleModule.forRoot(), PuppeteerModule],
  providers: [CronjobService],
  controllers: [],
  exports: [CronjobService]
})
export class CronjobModule {}
