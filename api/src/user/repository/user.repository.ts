import { EntityRepository, Repository } from 'typeorm'
import { ConflictException, NotFoundException } from '@nestjs/common'
import UserEntity from '../entity/user.entity'
import CreateUserInput from '../dto/create-user.input'
import UpdateUserByIdInput from '../dto/update-user-by-id.input'

@EntityRepository(UserEntity)
export default class UserRepository extends Repository<UserEntity> {
  async createOneAndSave(input: CreateUserInput): Promise<UserEntity> {
    const found = await this.findOne({ where: { email: input.email } })

    if (found) {
      throw new ConflictException('User with this email already exists')
    }

    const created = this.create(input)

    await this.save(created)

    return created
  }

  async updateById(id: string, dto: UpdateUserByIdInput): Promise<UserEntity> {
    const found = await this.findOne({ where: { id } })

    if (!found) {
      throw new ConflictException('User not found')
    }

    return this.save({
      ...found, // existing fields
      ...dto // updated fields
    })
  }

  async findOneByEmail(email: string): Promise<UserEntity> {
    const user = await this.findOne({ email })

    if (!user) {
      throw new NotFoundException('User with this email not found')
    }

    return user
  }

  async findUserById(id: string): Promise<UserEntity> {
    const user = await this.findOne({ id })

    if (!user) {
      throw new NotFoundException('User with this email not found')
    }

    return user
  }
}
