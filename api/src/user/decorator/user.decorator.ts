import { createParamDecorator, ExecutionContext } from '@nestjs/common'
import UserEntity from '../entity/user.entity'
import { GqlExecutionContext } from '@nestjs/graphql'

export const UserDecorator = createParamDecorator(
  (data, ctx: ExecutionContext): UserEntity => {
    if (ctx.getType() === 'http') {
      const request = ctx.switchToHttp().getRequest()
      return request.user
    }

    const gqlCtx = GqlExecutionContext.create(ctx)
    return gqlCtx.getContext().req.user
  }
)
