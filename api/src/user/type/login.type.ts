import { Field, ObjectType } from '@nestjs/graphql'

@ObjectType('Login')
export default class LoginType {
  @Field()
  secret: string
}
