import { Injectable, NotFoundException } from '@nestjs/common'
import { InjectRepository } from '@nestjs/typeorm'
import { v4 as uuid } from 'uuid'

import UserEntity from '../entity/user.entity'
import CreateUserInput from '../dto/create-user.input'
import UserRepository from '../repository/user.repository'
import { HashingService } from '../../hashing/service/hashing.service'
import UpdateUserByIdInput from '../dto/update-user-by-id.input'
import { FileService } from '../../file/service/file.service'
import { StripeService } from '../../stripe/service/stripe.service'
import { UserAvatarService } from '../../user-avatar/service/user-avatar.service'
import UserAvatarEntity from '../../user-avatar/entity/user-avatar.entity'
import { ConfigService } from '@nestjs/config'

@Injectable()
export class UserService {
  constructor(
    @InjectRepository(UserRepository)
    private readonly userRepository: UserRepository,
    private hashingService: HashingService,
    private fileService: FileService,
    private userAvatarService: UserAvatarService,
    private configService: ConfigService,
    private stripeService: StripeService
  ) {}

  async createOne(input: CreateUserInput): Promise<UserEntity> {
    const stripeCustomer = await this.stripeService.createCustomer(
      `${input.firstName} ${input.lastName}`,
      input.email
    )

    const password = this.hashingService.hashSync({
      str: input.password
    })

    return this.userRepository.createOneAndSave({
      ...input,
      password,
      stripeCustomerId: stripeCustomer.id
    })
  }

  async findUserByEmail(email: string): Promise<UserEntity> {
    const user = await this.userRepository.findOneByEmail(email)

    if (!user) {
      throw new NotFoundException('User with this email not found')
    }

    return user
  }

  async findUserById(id: string): Promise<UserEntity> {
    const user = await this.userRepository.findUserById(id)

    if (!user) {
      throw new NotFoundException('User with this email not found')
    }

    return user
  }

  async updateById(id: string, dto: UpdateUserByIdInput): Promise<UserEntity> {
    return this.userRepository.updateById(id, dto)
  }

  // todo save aws result to userAvatar entity
  async uploadAvatar(
    userId: string,
    file: Express.Multer.File
  ): Promise<UserAvatarEntity> {
    const { buffer, originalname, mimetype } = file

    const folderName = this.configService.get<string>('AWS_S3_BUCKET_FOLDER')
    const ID = uuid()

    const uploadResult = await this.fileService.upload({
      mimetype,
      buffer,
      path: `${folderName}/user-avatar/${ID}___${originalname}`
    })

    return this.userAvatarService.createOne({
      userId,
      key: uploadResult.Key,
      url: uploadResult.Location
    })
  }

  async removeAvatarByUserId(id: string): Promise<UserEntity> {
    await this.userAvatarService.removeByUserId(id)
    return this.findUserById(id)
  }
}
