import {
  Column,
  CreateDateColumn,
  Entity,
  OneToMany,
  OneToOne,
  PrimaryGeneratedColumn,
  UpdateDateColumn
} from 'typeorm'

import { Field, ID, ObjectType } from '@nestjs/graphql'
import TodoEntity from '../../todo/entity/todo.entity'
import UserAvatarEntity from '../../user-avatar/entity/user-avatar.entity'

@Entity('user')
@ObjectType('User')
export default class UserEntity {
  @Field(() => ID)
  @PrimaryGeneratedColumn('uuid')
  id: string

  @Field()
  @Column()
  email: string

  @Field()
  @Column()
  firstName: string

  @Field()
  @Column()
  lastName: string

  @Column()
  password: string

  @Field()
  @CreateDateColumn()
  createdAt: Date

  @Field()
  @UpdateDateColumn()
  updatedAt: Date

  @Field(() => [TodoEntity], { nullable: true })
  @OneToMany(() => TodoEntity, todo => todo.user)
  todos?: TodoEntity[]

  @Field(() => UserAvatarEntity, { nullable: true })
  @OneToOne(() => UserAvatarEntity, avatar => avatar.user)
  avatar: UserAvatarEntity

  @Column({ unique: true, nullable: true })
  public stripeCustomerId: string
}
