import {
  Controller,
  Post,
  UnsupportedMediaTypeException,
  UploadedFile,
  UseGuards,
  UseInterceptors
} from '@nestjs/common'
import { FileInterceptor } from '@nestjs/platform-express'
import { JwtAuthGuard } from '../../auth/guard/jwt/jwt-auth.guard'
import { FileService } from '../../file/service/file.service'
import UserEntity from '../entity/user.entity'
import { UserService } from '../service/user.service'
import { UserDecorator } from '../decorator/user.decorator'

import UserAvatarEntity from '../../user-avatar/entity/user-avatar.entity'

@Controller('user')
export class UserController {
  constructor(
    private readonly fileService: FileService,
    private readonly userService: UserService
  ) {}

  @Post('/avatar')
  @UseGuards(JwtAuthGuard)
  @UseInterceptors(FileInterceptor('file'))
  async uploadAvatar(
    @UserDecorator() user: UserEntity,
    @UploadedFile() file: Express.Multer.File
  ): Promise<UserAvatarEntity> {
    if (!this.fileService.isValidImage(file)) {
      throw new UnsupportedMediaTypeException('File is not an image type')
    }

    return this.userService.uploadAvatar(user.id, file)
  }
}
