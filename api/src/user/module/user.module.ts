import { forwardRef, Module } from '@nestjs/common'
import { TypeOrmModule } from '@nestjs/typeorm'
import UserRepository from '../repository/user.repository'
import UserResolver from '../resolver/user.resolver'
import { UserService } from '../service/user.service'
import { HashingModule } from '../../hashing/module/hashing.module'
import { TodoModule } from '../../todo/module/todo.module'
import { UserController } from '../controller/user.controller'
import { FileModule } from '../../file/module/file.module'
import { UserAvatarModule } from '../../user-avatar/module/user-avatar.module'
import { StripeModule } from '../../stripe/module/stripe.module'

@Module({
  imports: [
    TypeOrmModule.forFeature([UserRepository]),
    HashingModule,
    forwardRef(() => TodoModule),
    UserAvatarModule,
    FileModule,
    StripeModule
  ],
  providers: [UserResolver, UserService],
  controllers: [UserController],
  exports: [UserService]
})
export class UserModule {}
