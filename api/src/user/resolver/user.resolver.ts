import {
  Args,
  Mutation,
  Parent,
  Query,
  ResolveField,
  Resolver,
  Subscription
} from '@nestjs/graphql'
import { FileUpload, GraphQLUpload } from 'graphql-upload'
import { createWriteStream } from 'fs'
import { PubSub } from 'graphql-subscriptions'

import CreateUserInput from '../dto/create-user.input'
import UserEntity from '../entity/user.entity'
import { UserService } from '../service/user.service'
import { NotFoundException, UseGuards } from '@nestjs/common'
import { GqlAuthGuard } from '../../auth/guard/gql/gql-auth.guard'
import { UserDecorator } from '../decorator/user.decorator'
import { TodoService } from '../../todo/service/todo.service'
import FindManyTodosByUserIdResultDto from '../../todo/dto/find-many-todos-by-user-id-result.dto'
import UpdateUserByIdInput from '../dto/update-user-by-id.input'
import UserAvatarEntity from '../..//user-avatar/entity/user-avatar.entity'
import { UserAvatarService } from '../../user-avatar/service/user-avatar.service'

const pubSub = new PubSub()

@Resolver(() => UserEntity)
export default class UserResolver {
  constructor(
    private readonly userService: UserService,
    private readonly todoService: TodoService,
    private readonly userAvatarService: UserAvatarService
  ) {}

  @Mutation(() => Boolean)
  async uploadFile(
    @Args({ name: 'file', type: () => GraphQLUpload })
    { createReadStream, filename }: FileUpload
  ): Promise<boolean> {
    return new Promise(async (resolve, reject) =>
      createReadStream()
        .pipe(createWriteStream(`./uploads/${filename}`))
        .on('finish', () => resolve(true))
        .on('error', () => reject(false))
    )
  }

  @Mutation(() => UserEntity)
  async createUser(@Args('input') input: CreateUserInput): Promise<UserEntity> {
    return this.userService.createOne(input)
  }

  // TODO MOVE TO USER AVATAR MODULE
  @UseGuards(GqlAuthGuard)
  @Mutation(() => UserEntity)
  async deleteCurrentUserAvatar(
    @UserDecorator() user: UserEntity
  ): Promise<UserEntity> {
    return this.userService.removeAvatarByUserId(user.id)
  }

  @UseGuards(GqlAuthGuard)
  @Mutation(() => UserEntity)
  async updateCurrentUser(
    @UserDecorator() user: UserEntity,
    @Args('data') data: UpdateUserByIdInput
  ): Promise<UserEntity> {
    return this.userService.updateById(user.id, data)
  }

  @UseGuards(GqlAuthGuard)
  @Query(() => UserEntity)
  async currentUser(@UserDecorator() user: UserEntity): Promise<UserEntity> {
    const result = await this.userService.findUserById(user.id)

    if (!result) {
      throw new NotFoundException('User not found')
    }

    return result
  }

  @ResolveField(() => FindManyTodosByUserIdResultDto)
  async todos(
    @Parent() user: UserEntity
  ): Promise<FindManyTodosByUserIdResultDto> {
    return this.todoService.findManyByUserId({ userId: user.id })
  }

  @ResolveField(() => UserAvatarEntity)
  async avatar(@Parent() user: UserEntity): Promise<UserAvatarEntity> {
    return this.userAvatarService.findByUserId(user.id)
  }

  @Subscription(() => String)
  commentAdded() {
    return pubSub.asyncIterator('commentAdded')
  }

  @Mutation(() => String)
  async addComment(@Args('comment', { type: () => String }) comment: string) {
    pubSub.publish('commentAdded', { commentAdded: comment })
    return comment
  }
}
