import { Field, InputType } from '@nestjs/graphql'
import {
  IsEmail,
  IsLowercase,
  IsOptional,
  MaxLength,
  MinLength
} from 'class-validator'

@InputType()
export default class UpdateUserByIdInput {
  @Field({ nullable: true })
  @IsOptional()
  @MinLength(5, {
    message:
      'Email is too short. Minimal length is $constraint1 characters, but actual is $value'
  })
  @MaxLength(320, {
    message:
      'Email is too long. Maximal length is $constraint1 characters, but actual is $value'
  })
  @IsEmail()
  @IsLowercase()
  readonly email!: string

  @Field({ nullable: true })
  @IsOptional()
  @MinLength(5, {
    message:
      'First Name is too short. Minimal length is $constraint1 characters, but actual is $value'
  })
  @MaxLength(55, {
    message:
      'First Name is too long. Maximal length is $constraint1 characters, but actual is $value'
  })
  readonly firstName: string

  @Field({ nullable: true })
  @IsOptional()
  @MinLength(5, {
    message:
      'Last Name is too short. Minimal length is $constraint1 characters, but actual is $value'
  })
  @MaxLength(55, {
    message:
      'Last Name is too long. Maximal length is $constraint1 characters, but actual is $value'
  })
  readonly lastName: string
}
