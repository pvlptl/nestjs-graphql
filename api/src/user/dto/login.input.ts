import { Field, InputType } from '@nestjs/graphql'
import {
  IsNotEmpty,
  MinLength,
  MaxLength,
  IsEmail,
  IsLowercase
} from 'class-validator'

@InputType()
export default class LoginInput {
  @Field()
  @IsNotEmpty()
  @MinLength(5, {
    message:
      'Email is too short. Minimal length is $constraint1 characters, but actual is $value'
  })
  @MaxLength(320, {
    message:
      'Email is too long. Maximal length is $constraint1 characters, but actual is $value'
  })
  @IsEmail()
  @IsLowercase()
  readonly email!: string

  @Field()
  @IsNotEmpty()
  @MinLength(1, {
    message:
      'Password is too short. Minimal length is $constraint1 characters, but actual is $value'
  })
  readonly password!: string
}
