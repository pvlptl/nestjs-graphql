import { Field, InputType } from '@nestjs/graphql'
import {
  IsNotEmpty,
  MinLength,
  MaxLength,
  IsEmail,
  IsLowercase,
  IsOptional
} from 'class-validator'

@InputType()
export default class CreateUserInput {
  @Field()
  @IsNotEmpty()
  @MinLength(5, {
    message:
      'Email is too short. Minimal length is $constraint1 characters, but actual is $value'
  })
  @MaxLength(320, {
    message:
      'Email is too long. Maximal length is $constraint1 characters, but actual is $value'
  })
  @IsEmail()
  @IsLowercase()
  readonly email!: string

  @Field()
  @MinLength(5, {
    message:
      'First Name is too short. Minimal length is $constraint1 characters, but actual is $value'
  })
  @MaxLength(55, {
    message:
      'First Name is too long. Maximal length is $constraint1 characters, but actual is $value'
  })
  readonly firstName: string

  @Field()
  @MinLength(5, {
    message:
      'Last Name is too short. Minimal length is $constraint1 characters, but actual is $value'
  })
  @MaxLength(55, {
    message:
      'Last Name is too long. Maximal length is $constraint1 characters, but actual is $value'
  })
  readonly lastName: string

  @Field()
  @IsNotEmpty()
  @MinLength(5, {
    message:
      'Password is too short. Minimal length is $constraint1 characters, but actual is $value'
  })
  readonly password!: string

  @Field({ nullable: true })
  @IsOptional()
  readonly stripeCustomerId?: string
}
