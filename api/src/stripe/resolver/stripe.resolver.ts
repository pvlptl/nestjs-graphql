import { Args, Mutation, Query, Resolver } from '@nestjs/graphql'
import { UseGuards } from '@nestjs/common'
import { GqlAuthGuard } from '../../auth/guard/gql/gql-auth.guard'
import { UserDecorator } from '../../user/decorator/user.decorator'
import UserEntity from '../../user/entity/user.entity'
import { StripeService } from '../service/stripe.service'
import { CreateChargeResponse } from '../dto/create-charge-response.dto'
import CreateChargeInput from '../dto/create-charge.input'
import { SetupIntentDto } from '../dto/setupIntent.dto'
import { PaymentMethodDto } from '../dto/payment-method.dto'
import { ProductDto } from '../dto/product.dto'
import { SubscriptionDto } from '../dto/subscription.dto'
import SubscriptionCreateInput from '../dto/subscription-create.input'
import { CustomerDto } from '../dto/customer.dto'
import { CustomersUpdateInput } from '../dto/customers-update.input'
import PaymentMethodDetachInput from '../dto/payment-method-detach.input'
import SubDelInput from '../dto/sub-del.input'
import { SubCreateResponseDto } from '../dto/sub-create-response.dto'
import { PayInvoiceResponseDto } from '../dto/pay-invoice-response.dto'
import PayInvoiceInput from '../dto/pay-invoice.input'
import { StripeCustomerDto } from '../dto/stripe-customer.dto'
import { InvoiceDto } from '../dto/invoice.dto'
import { PaymentIntentDto } from '../dto/payment-intent.dto'

@Resolver(() => UserEntity)
export default class StripeResolver {
  constructor(private readonly stripeService: StripeService) {}

  @UseGuards(GqlAuthGuard)
  @Query(() => [PaymentIntentDto])
  async paymentIntentsList(
    @UserDecorator() user: UserEntity
  ): Promise<PaymentIntentDto[]> {
    return this.stripeService.paymentIntentsList({
      customerId: user.stripeCustomerId
    })
  }

  @UseGuards(GqlAuthGuard)
  @Query(() => [InvoiceDto])
  async invoicesList(@UserDecorator() user: UserEntity): Promise<InvoiceDto[]> {
    return this.stripeService.invoicesList({
      customerId: user.stripeCustomerId
    })
  }

  @UseGuards(GqlAuthGuard)
  @Query(() => StripeCustomerDto)
  async stripeCustomer(
    @UserDecorator() user: UserEntity
  ): Promise<StripeCustomerDto> {
    return (await this.stripeService.customer(user.stripeCustomerId)) as any
  }

  @UseGuards(GqlAuthGuard)
  @Query(() => [PaymentMethodDto])
  async paymentMethodsList(
    @UserDecorator() user: UserEntity
  ): Promise<PaymentMethodDto[]> {
    return this.stripeService.paymentMethodsList(user.stripeCustomerId)
  }

  @UseGuards(GqlAuthGuard)
  @Query(() => [ProductDto])
  async productsList(): Promise<ProductDto[]> {
    return this.stripeService.productsList()
  }

  @UseGuards(GqlAuthGuard)
  @Query(() => [SubscriptionDto])
  async subsList(
    @UserDecorator() user: UserEntity
  ): Promise<SubscriptionDto[]> {
    return this.stripeService.subsList({ customerId: user.stripeCustomerId })
  }

  @UseGuards(GqlAuthGuard)
  @Mutation(() => CreateChargeResponse)
  async createCharge(
    @UserDecorator() user: UserEntity,
    @Args('input') input: CreateChargeInput
  ): Promise<CreateChargeResponse> {
    return this.stripeService.createCharge(
      input.amount,
      input.paymentMethodId,
      user.stripeCustomerId
    )
  }

  @UseGuards(GqlAuthGuard)
  @Mutation(() => SetupIntentDto)
  async createSetupIntent(
    @UserDecorator() user: UserEntity
  ): Promise<SetupIntentDto> {
    return this.stripeService.createSetupIntent(user.stripeCustomerId)
  }

  @UseGuards(GqlAuthGuard)
  @Mutation(() => SubCreateResponseDto)
  async subscriptionsCreate(
    @UserDecorator() user: UserEntity,
    @Args('input') input: SubscriptionCreateInput
  ): Promise<SubCreateResponseDto> {
    return this.stripeService.subscriptionsCreate(
      user.stripeCustomerId,
      input.priceId
    )
  }

  @UseGuards(GqlAuthGuard)
  @Mutation(() => PayInvoiceResponseDto)
  async payInvoice(
    @Args('input') input: PayInvoiceInput
  ): Promise<PayInvoiceResponseDto> {
    return this.stripeService.payInvoice(input.invoiceId)
  }

  @UseGuards(GqlAuthGuard)
  @Mutation(() => CustomerDto)
  async stripeCustomerUpdate(
    @UserDecorator() user: UserEntity,
    @Args('input') input: CustomersUpdateInput
  ): Promise<CustomerDto> {
    return this.stripeService.customerUpdate({
      customerId: user.stripeCustomerId,
      params: input
    })
  }

  @UseGuards(GqlAuthGuard)
  @Mutation(() => PaymentMethodDto)
  async paymentMethodDetach(
    @UserDecorator() user: UserEntity,
    @Args('input') input: PaymentMethodDetachInput
  ): Promise<PaymentMethodDto> {
    return this.stripeService.paymentMethodDetach({
      paymentMethodId: input.paymentMethodId
    })
  }

  @UseGuards(GqlAuthGuard)
  @Mutation(() => SubscriptionDto)
  async subDel(
    @UserDecorator() user: UserEntity,
    @Args('input') input: SubDelInput
  ): Promise<SubscriptionDto> {
    return this.stripeService.subDel({
      subId: input.subId
    })
  }
}
