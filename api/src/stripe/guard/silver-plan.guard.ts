import {
  Injectable,
  CanActivate,
  ExecutionContext,
  ForbiddenException
} from '@nestjs/common'
import { GqlExecutionContext } from '@nestjs/graphql'
import { StripeService } from '../service/stripe.service'
import { ConfigService } from '@nestjs/config'

@Injectable()
export class SilverPlanGuard implements CanActivate {
  constructor(
    private configService: ConfigService,
    private readonly stripeService: StripeService
  ) {}

  async canActivate(context: ExecutionContext): Promise<boolean> {
    let user

    if (context.getType() === 'http') {
      const request = context.switchToHttp().getRequest()
      user = request.user
    } else {
      const gqlCtx = GqlExecutionContext.create(context)
      user = gqlCtx.getContext().req.user
    }

    const subs = await this.stripeService.subsList({
      customerId: user.stripeCustomerId
    })

    const sub = subs.find(
      s =>
        s.plan.product === this.configService.get<any>('SILVER_PLAN_ID') ||
        s.plan.product === this.configService.get<any>('GOLD_PLAN_ID')
    )

    if (sub?.status !== 'active') {
      throw new ForbiddenException('Atleast Silver plan required')
    }

    return true
  }
}
