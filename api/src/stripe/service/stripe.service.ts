import { Injectable } from '@nestjs/common'
import Stripe from 'stripe'
import { ConfigService } from '@nestjs/config'
import { CreateChargeResponse } from '../dto/create-charge-response.dto'
import { SetupIntentDto } from '../dto/setupIntent.dto'
import { PaymentMethodDto } from '../dto/payment-method.dto'
import { ProductDto } from '../dto/product.dto'
import { SubscriptionDto } from '../dto/subscription.dto'
import { CustomerDto } from '../dto/customer.dto'
import { SubCreateResponseDto } from '../dto/sub-create-response.dto'
import { PayInvoiceResponseDto } from '../dto/pay-invoice-response.dto'
import { InvoiceDto } from '../dto/invoice.dto'
import { PaymentIntentDto } from '../dto/payment-intent.dto'

@Injectable()
export class StripeService {
  private stripe: Stripe

  constructor(private configService: ConfigService) {
    this.stripe = new Stripe(
      this.configService.get<string>('STRIPE_SECRET_KEY'),
      {
        apiVersion: this.configService.get<any>('STRIPE_API_VERSION')
      }
    )
  }

  public async createCustomer(
    name: string,
    email: string
  ): Promise<Stripe.Response<Stripe.Customer>> {
    return this.stripe.customers.create({
      name,
      email
    })
  }

  public async customer(
    customerId: string
  ): Promise<Stripe.Response<Stripe.Customer | Stripe.DeletedCustomer>> {
    return this.stripe.customers.retrieve(customerId)
  }

  public async createCharge(
    amount: number,
    paymentMethodId: string,
    customerId: string
  ): Promise<CreateChargeResponse> {
    const balanceTransaction =
      await this.stripe.customers.createBalanceTransaction(customerId, {
        amount,
        currency: 'usd'
      })

    console.log('balanceTransaction', balanceTransaction)

    // const response = await this.stripe.paymentIntents.create({
    //   amount,
    //   customer: customerId,
    //   payment_method: paymentMethodId,
    //   currency: 'usd',
    //   confirm: true,
    //   return_url: 'http://localhost:3002/balance'
    // })
    //
    // const { status, id, client_secret, next_action } = response
    //
    // return {
    //   id,
    //   status,
    //   client_secret,
    //   redirect_to_url: next_action?.redirect_to_url?.url || null
    // }
    return {
      id: '123',
      status: 'active' as any,
      client_secret: null,
      redirect_to_url: null
    }
  }

  public async createSetupIntent(
    stripeCustomerId: string
  ): Promise<SetupIntentDto> {
    return this.stripe.setupIntents.create({
      customer: stripeCustomerId,
      payment_method_types: ['card']
    })
  }

  public async paymentMethodsList(
    stripeCustomerId: string
  ): Promise<PaymentMethodDto[]> {
    const { data } = (await this.stripe.paymentMethods.list({
      customer: stripeCustomerId,
      type: 'card'
    })) as any

    return data
  }

  public async productsList(): Promise<ProductDto[]> {
    const response = (await this.stripe.products.list({
      active: true
    })) as any

    const temp = [...response.data] as any

    for (const i of temp) {
      const idx = temp.findIndex(x => x.id === i.id)

      const prices = await this.stripe.prices.list({
        product: i.id
      })

      temp[idx].prices = prices.data
    }

    return temp
  }

  public async subscriptionsCreate(
    stripeCustomerId: string,
    priceId: string
  ): Promise<SubCreateResponseDto> {
    const subscription = (await this.stripe.subscriptions.create({
      customer: stripeCustomerId,
      items: [{ price: priceId }]
    })) as any

    const invoice = await this.stripe.invoices.retrieve(
      subscription.latest_invoice
    )

    const paymentIntent = (await this.stripe.paymentIntents.retrieve(
      invoice.payment_intent as string
    )) as any

    return {
      subscription,
      paymentIntent
    }
  }

  public async customerUpdate({
    customerId,
    params
  }: any): Promise<CustomerDto> {
    return (await this.stripe.customers.update(customerId, params)) as any
  }

  public async paymentMethodDetach({
    paymentMethodId
  }: any): Promise<PaymentMethodDto> {
    return (await this.stripe.paymentMethods.detach(paymentMethodId)) as any
  }

  public async subsList({ customerId }: any): Promise<SubscriptionDto[]> {
    const { data } = (await this.stripe.subscriptions.list({
      customer: customerId
    })) as any

    const temp = [...data]

    for (const i of temp) {
      const idx = temp.findIndex(x => x.id === i.id)

      temp[idx].plan.product_details = await this.stripe.products.retrieve(
        temp[idx].plan.product
      )
    }

    return temp
  }

  public async subDel({ subId }: any): Promise<SubscriptionDto> {
    return (await this.stripe.subscriptions.del(subId)) as any
  }

  public async payInvoice(invoiceId: string): Promise<PayInvoiceResponseDto> {
    const invoice = (await this.stripe.invoices.retrieve(invoiceId)) as any

    const paymentIntent = (await this.stripe.paymentIntents.retrieve(
      invoice.payment_intent as string
    )) as any

    return {
      invoice,
      paymentIntent
    }
  }

  public async invoicesList({ customerId }: any): Promise<InvoiceDto[]> {
    const { data } = (await this.stripe.invoices.list({
      customer: customerId
    })) as any

    return data
  }

  public async paymentIntentsList({
    customerId
  }: any): Promise<PaymentIntentDto[]> {
    const { data } = (await this.stripe.paymentIntents.list({
      customer: customerId,
      expand: ['data.invoice']
    })) as any

    return data
  }
}
