import { Field, ObjectType } from '@nestjs/graphql'

@ObjectType()
export class CardDto {
  @Field()
  readonly brand: string

  @Field()
  readonly country: string | null

  @Field()
  readonly exp_month: number

  @Field()
  readonly exp_year: number

  @Field()
  readonly fingerprint?: string | null

  @Field()
  readonly funding: string

  @Field()
  readonly last4: string
}
