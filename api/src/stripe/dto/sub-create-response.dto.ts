import { Field, ObjectType } from '@nestjs/graphql'
import { PaymentIntentDto } from './payment-intent.dto'
import { SubscriptionDto } from './subscription.dto'

@ObjectType()
export class SubCreateResponseDto {
  @Field(() => SubscriptionDto)
  readonly subscription: SubscriptionDto

  @Field(() => PaymentIntentDto)
  readonly paymentIntent: PaymentIntentDto
}
