import { Field, InputType } from '@nestjs/graphql'
import { IsNotEmpty } from 'class-validator'

@InputType()
export default class SubscriptionCreateInput {
  @Field()
  @IsNotEmpty()
  readonly priceId: string
}
