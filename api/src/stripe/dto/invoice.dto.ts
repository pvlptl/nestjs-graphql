import { Field, ObjectType } from '@nestjs/graphql'
import Stripe from 'stripe'
import { InvoiceLinesDto } from './invoice-lines.dto'

@ObjectType()
export class InvoiceDto {
  @Field()
  readonly id: string

  @Field()
  readonly amount_due: number

  @Field()
  readonly amount_paid: number

  @Field()
  readonly amount_remaining: number

  @Field()
  readonly attempt_count: number

  @Field()
  readonly attempted: boolean

  @Field()
  readonly currency: string

  @Field({ nullable: true })
  readonly next_payment_attempt: number | null

  @Field()
  readonly period_start: number

  @Field()
  readonly period_end: number

  @Field()
  readonly created: number

  @Field()
  readonly paid: boolean

  @Field({ nullable: true })
  readonly payment_intent: string | null

  @Field({ nullable: true })
  readonly status: Stripe.Invoice.Status | null

  @Field({ nullable: true })
  readonly invoice_pdf: string | null

  @Field({ nullable: true })
  readonly hosted_invoice_url: string | null

  @Field({ nullable: true })
  readonly billing_reason: Stripe.Invoice.BillingReason | null

  @Field()
  readonly collection_method: Stripe.Invoice.CollectionMethod

  @Field(() => InvoiceLinesDto)
  readonly lines: InvoiceLinesDto
}
