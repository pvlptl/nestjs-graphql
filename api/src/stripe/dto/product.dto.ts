import { Field, ObjectType } from '@nestjs/graphql'
import { PriceDto } from './price.dto'

@ObjectType()
export class ProductDto {
  @Field()
  readonly id: string

  @Field()
  readonly active: boolean

  @Field({ nullable: true })
  readonly description: string | null

  @Field(() => [String])
  readonly images: string[]

  @Field()
  readonly name: string

  @Field(() => [PriceDto])
  readonly prices: PriceDto[]
}
