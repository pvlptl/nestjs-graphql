import { Field, InputType } from '@nestjs/graphql'
import { IsNotEmpty } from 'class-validator'

@InputType()
export default class CreateChargeInput {
  @Field()
  @IsNotEmpty()
  readonly amount: number

  @Field()
  @IsNotEmpty()
  readonly paymentMethodId: string
}
