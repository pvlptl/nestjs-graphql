import { Field, ObjectType } from '@nestjs/graphql'

@ObjectType()
export class UseStripeSdkDto {
  @Field({ nullable: true })
  readonly return_url: string | null

  @Field({ nullable: true })
  readonly url: string | null
}

@ObjectType()
export class RedirectToUrlDto {
  @Field({ nullable: true })
  readonly return_url: string | null

  @Field({ nullable: true })
  readonly url: string | null
}

@ObjectType()
export class NextActionDto {
  @Field()
  readonly type: string

  @Field(() => RedirectToUrlDto, { nullable: true, defaultValue: null })
  readonly redirect_to_url: RedirectToUrlDto

  // todo use UseStripeSdkDto
  @Field()
  readonly use_stripe_sdk: string
}
