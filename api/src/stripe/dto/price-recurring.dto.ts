import { Field, ObjectType } from '@nestjs/graphql'
import Stripe from 'stripe'

@ObjectType()
export class PriceRecurringDto {
  @Field({ nullable: true })
  readonly aggregate_usage: Stripe.Price.Recurring.AggregateUsage | null

  @Field()
  readonly interval: Stripe.Price.Recurring.Interval

  @Field()
  readonly interval_count: number

  @Field({ nullable: true })
  readonly trial_period_days: number | null

  @Field()
  readonly usage_type: Stripe.Price.Recurring.UsageType
}
