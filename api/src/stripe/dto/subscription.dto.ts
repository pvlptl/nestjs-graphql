import { Field, ObjectType } from '@nestjs/graphql'
import Stripe from 'stripe'
import { PlanDto } from './plan.dto'

@ObjectType()
export class SubscriptionDto {
  @Field()
  readonly id: string

  @Field({ nullable: true })
  readonly cancel_at: number | null

  @Field()
  readonly cancel_at_period_end: boolean

  @Field({ nullable: true })
  readonly canceled_at: number | null

  @Field()
  readonly current_period_end: number

  @Field()
  readonly current_period_start: number

  @Field({ nullable: true })
  readonly days_until_due: number | null

  @Field({ nullable: true })
  readonly ended_at: number | null

  @Field()
  readonly start_date: number

  @Field()
  readonly status: Stripe.Subscription.Status

  @Field(() => PlanDto)
  readonly plan: PlanDto

  @Field({ nullable: true })
  readonly latest_invoice: string | null
}
