import { Field, ObjectType } from '@nestjs/graphql'
import Stripe from 'stripe'
import { NextActionDto } from './nextAction.dto'
import { InvoiceDto } from './invoice.dto'

@ObjectType()
export class PaymentIntentDto {
  @Field()
  readonly id: string

  @Field()
  readonly amount: number

  @Field()
  readonly amount_capturable: number

  @Field()
  readonly amount_received: number

  @Field({ nullable: true })
  readonly canceled_at: number | null

  @Field({ nullable: true })
  readonly cancellation_reason: Stripe.PaymentIntent.CancellationReason | null

  @Field()
  readonly capture_method: Stripe.PaymentIntent.CaptureMethod

  @Field({ nullable: true })
  readonly client_secret: string | null

  @Field()
  readonly confirmation_method: Stripe.PaymentIntent.ConfirmationMethod

  @Field()
  readonly created: number

  @Field()
  readonly currency: string

  @Field({ nullable: true })
  readonly description: string | null

  @Field(() => InvoiceDto, { nullable: true })
  readonly invoice: InvoiceDto | null

  @Field({ nullable: true })
  readonly payment_method: string | null

  @Field(() => NextActionDto, { nullable: true })
  readonly next_action: NextActionDto | null

  @Field()
  readonly status: Stripe.PaymentIntent.Status
}
