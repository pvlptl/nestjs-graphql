import { Field, ObjectType } from '@nestjs/graphql'
import { AddressDto } from './address.dto'

@ObjectType()
export class StripeCustomerDto {
  @Field()
  readonly id: string

  @Field(() => AddressDto, { nullable: true })
  readonly address: AddressDto | null

  @Field()
  readonly balance: number

  @Field({ nullable: true })
  readonly currency: string | null

  @Field({ nullable: true })
  readonly delinquent: boolean | null

  @Field({ nullable: true })
  readonly description: string | null

  @Field({ nullable: true })
  readonly email: string | null

  @Field({ nullable: true })
  readonly invoice_prefix: string | null

  @Field({ nullable: true })
  readonly name: string | null

  @Field({ nullable: true })
  readonly phone: string | null
}
