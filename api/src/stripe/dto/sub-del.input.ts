import { Field, InputType } from '@nestjs/graphql'
import { IsNotEmpty } from 'class-validator'

@InputType()
export default class SubDelInput {
  @Field()
  @IsNotEmpty()
  readonly subId: string
}
