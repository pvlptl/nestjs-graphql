import { Field, ObjectType } from '@nestjs/graphql'
import { PaymentIntentDto } from './payment-intent.dto'
import { InvoiceDto } from './invoice.dto'

@ObjectType()
export class PayInvoiceResponseDto {
  @Field(() => InvoiceDto)
  readonly invoice: InvoiceDto

  @Field(() => PaymentIntentDto)
  readonly paymentIntent: PaymentIntentDto
}
