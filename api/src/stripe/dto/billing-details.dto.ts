import { Field, ObjectType } from '@nestjs/graphql'
import { AddressDto } from './address.dto'

@ObjectType()
export class BillingDetailsDto {
  @Field(() => AddressDto)
  readonly address: AddressDto | null

  @Field({ nullable: true })
  readonly email: string | null

  @Field({ nullable: true })
  readonly name: string | null

  @Field({ nullable: true })
  readonly phone: string | null
}
