import { Field, ObjectType } from '@nestjs/graphql'
import { InvoiceLineItemDto } from './invoice-line-item.dto'

@ObjectType()
export class InvoiceLinesDto {
  @Field(() => [InvoiceLineItemDto])
  readonly data: InvoiceLineItemDto[]

  @Field()
  readonly has_more: boolean

  @Field()
  readonly url: string
}
