import { Field, ObjectType } from '@nestjs/graphql'
import Stripe from 'stripe'

@ObjectType()
export class SetupIntentDto {
  @Field()
  readonly id: string

  @Field({ nullable: true })
  readonly client_secret: string | null

  @Field()
  readonly status: Stripe.SetupIntent.Status
}
