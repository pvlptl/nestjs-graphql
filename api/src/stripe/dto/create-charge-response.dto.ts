import Stripe from 'stripe'
import { Field, ObjectType } from '@nestjs/graphql'

@ObjectType()
export class CreateChargeResponse {
  @Field()
  readonly id: string

  @Field()
  readonly status: Stripe.PaymentIntent.Status

  @Field({ nullable: true })
  readonly client_secret: string | null

  @Field({ nullable: true })
  readonly redirect_to_url: string | null
}
