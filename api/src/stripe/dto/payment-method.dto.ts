import { Field, ObjectType } from '@nestjs/graphql'
import { BillingDetailsDto } from './billing-details.dto'
import { CardDto } from './card.dto'

@ObjectType()
export class PaymentMethodDto {
  @Field()
  readonly id: string

  @Field()
  readonly billing_details: BillingDetailsDto

  @Field()
  readonly card: CardDto
}
