import { Field, ObjectType } from '@nestjs/graphql'
import Stripe from 'stripe'
import { PriceRecurringDto } from './price-recurring.dto'

@ObjectType()
export class PriceDto {
  @Field()
  readonly id: string

  @Field()
  readonly active: boolean

  @Field()
  readonly currency: string

  @Field()
  readonly billing_scheme: Stripe.Price.BillingScheme

  @Field(() => PriceRecurringDto)
  readonly recurring: PriceRecurringDto

  @Field()
  readonly type: Stripe.Price.Type

  @Field()
  readonly unit_amount: number | null

  @Field()
  readonly unit_amount_decimal: string | null
}
