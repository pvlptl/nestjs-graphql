import { Field, ObjectType } from '@nestjs/graphql'
import Stripe from 'stripe'
import { PlanDto } from './plan.dto'

@ObjectType()
export class InvoiceLineItemDto {
  @Field()
  readonly id: string

  @Field()
  readonly amount: number

  @Field()
  readonly currency: string

  @Field({ nullable: true })
  readonly description: string | null

  @Field({ nullable: true, defaultValue: null })
  readonly invoice_item?: string | undefined | null

  @Field(() => PlanDto, { nullable: true })
  readonly plan: PlanDto | null

  @Field()
  readonly proration: boolean

  @Field({ nullable: true })
  readonly quantity: number | null

  @Field({ nullable: true })
  readonly subscription: string | null

  @Field()
  readonly subscription_item: string

  @Field()
  readonly type: Stripe.InvoiceLineItem.Type
}
