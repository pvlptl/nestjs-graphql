import { Field, InputType } from '@nestjs/graphql'
import { IsNotEmpty } from 'class-validator'

@InputType()
export default class PayInvoiceInput {
  @Field()
  @IsNotEmpty()
  readonly invoiceId: string
}
