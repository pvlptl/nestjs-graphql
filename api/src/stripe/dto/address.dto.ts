import { Field, ObjectType } from '@nestjs/graphql'

@ObjectType()
export class AddressDto {
  @Field({ nullable: true })
  readonly city: string | null

  @Field({ nullable: true })
  readonly country: string | null

  @Field({ nullable: true })
  readonly line1: string | null

  @Field({ nullable: true })
  readonly line2: string | null

  @Field({ nullable: true })
  readonly postal_code: string | null

  @Field({ nullable: true })
  readonly state: string | null
}
