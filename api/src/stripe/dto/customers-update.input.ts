import { Field, InputType } from '@nestjs/graphql'
import { IsOptional } from 'class-validator'

@InputType()
export class InvoiceSettingsDto {
  @Field({ nullable: true })
  @IsOptional()
  readonly default_payment_method?: string
}

@InputType()
export class CustomersUpdateInput {
  @Field(() => InvoiceSettingsDto)
  @IsOptional()
  readonly invoice_settings?: InvoiceSettingsDto
}
