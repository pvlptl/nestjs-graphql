import { Field, ObjectType } from '@nestjs/graphql'
import Stripe from 'stripe'
import { ProductDto } from './product.dto'

@ObjectType()
export class PlanDto {
  @Field()
  readonly id: string

  @Field()
  readonly active: boolean

  @Field({ nullable: true })
  readonly amount: number

  @Field({ nullable: true })
  readonly amount_decimal: string

  @Field()
  readonly created: number

  @Field()
  readonly currency: string

  @Field()
  readonly interval: Stripe.Plan.Interval

  @Field()
  readonly interval_count: number

  @Field()
  readonly product: string

  @Field(() => ProductDto, { nullable: true, defaultValue: null })
  readonly product_details?: ProductDto | null | undefined
}
