import { Field, InputType } from '@nestjs/graphql'
import { IsNotEmpty } from 'class-validator'

@InputType()
export default class AttachPaymentMethodInput {
  @Field()
  @IsNotEmpty()
  readonly paymentMethodId: string
}
