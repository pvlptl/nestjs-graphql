import { Module } from '@nestjs/common'
import { StripeService } from '../service/stripe.service'
import StripeResolver from '../resolver/stripe.resolver'

@Module({
  imports: [],
  providers: [StripeResolver, StripeService],
  controllers: [],
  exports: [StripeService]
})
export class StripeModule {}
