import React, { useEffect } from 'react'
import { Routes, Route, Link } from 'react-router-dom'
import Box from '@mui/material/Box'
import Login from './routes/Login'
import Home from './routes/Home'
import RouteNotFound from './routes/RouteNotFound'
import { Grid, Theme } from '@mui/material'
import Signup from './routes/Signup'
import Logout from './routes/Logout'
import useCurrentUser from './hooks/useCurrentUser'
import TodosRoute from './routes/TodosRoute'
import SettingsRoute from './routes/SettingsRoute'
import Avatar from './components/Avatar'
import { useRecoilValue } from 'recoil'
import authTokenSelector from './recoil/selectors/authTokenSelector'
import axios from 'axios'
import TodoRoute from './routes/TodoRoute'
import CardRoute from './routes/CardRoute'
import PlansRoute from './routes/PlansRoute'
import SubsRoute from './routes/SubsRoute'
import MapsRoute from './routes/MapsRoute'
import InvoicesRoute from './routes/InvoicesRoute'
import PaymentsRoute from './routes/PaymentsRoute'
import TheyLoveWarRoute from './routes/TheyLoveWarRoute'
import WebsocketsRoute from './routes/WebsocketsRoute'

function App() {
  const currentUser = useCurrentUser()
  const { authToken } = useRecoilValue(authTokenSelector)

  useEffect(() => {
    axios.defaults.headers.common['Authorization'] = authToken
      ? `Bearer ${authToken}`
      : ''
  }, [authToken])

  const getTitle = () => {
    if (currentUser.user) {
      return (
        <Grid
          container
          sx={{
            alignItems: 'center',
            gap: theme => theme.spacing(1)
          }}
        >
          {currentUser.user?.avatar && (
            <Avatar
              size={32}
              alt="User Avatar"
              src={currentUser.user?.avatar?.url}
            />
          )}
          <h2>
            Hello, {currentUser.user.firstName} {currentUser.user.lastName}
          </h2>
        </Grid>
      )
    }

    return <h2>Hello, Guest</h2>
  }

  if (currentUser.isFetching) {
    return <div />
  }

  return (
    <Box
      sx={{
        p: (theme: Theme) => theme.spacing(3),
        pt: 0
      }}
    >
      <main>{getTitle()}</main>
      <div>
        <nav style={{ gap: 8, display: 'flex', marginBottom: 24 }}>
          <Link to="/">Home</Link>
          {!currentUser.user && (
            <>
              <Link to="login">Login</Link>
              <Link to="signup">Signup</Link>
            </>
          )}
          {currentUser.user && (
            <>
              <Link to="todos">Todos</Link>
              <Link to="settings">Settings</Link>
              <Link to="payment-methods">Payment Methods</Link>
              <Link to="subscriptions">Subscriptions</Link>
              <Link to="payments">Payments</Link>
              <Link to="invoices">Invoices</Link>
              <Link to="plans">All Plans</Link>
              <Link to="maps">Google Maps</Link>
              <Link to="theylovewar">TheyLoveWar</Link>
              <Link to="websockets">Websockets</Link>
              {/*<Link to="balance">Balance</Link>*/}
              <Link to="logout">Logout</Link>
            </>
          )}
        </nav>

        <Routes>
          <Route path="/" element={<Home />} />
          <Route path="login" element={<Login />} />
          <Route path="signup" element={<Signup />} />
          <Route path="todos" element={<TodosRoute />} />
          <Route path="todos/:id" element={<TodoRoute />} />
          <Route path="settings" element={<SettingsRoute />} />
          <Route path="payment-methods" element={<CardRoute />} />
          <Route path="plans" element={<PlansRoute />} />
          <Route path="subscriptions" element={<SubsRoute />} />
          <Route path="payments" element={<PaymentsRoute />} />
          <Route path="invoices" element={<InvoicesRoute />} />
          {/*<Route path="balance" element={<BalanceRoute />} />*/}
          <Route path="maps" element={<MapsRoute />} />
          <Route path="theylovewar" element={<TheyLoveWarRoute />} />
          <Route path="websockets" element={<WebsocketsRoute />} />
          <Route path="logout" element={<Logout />} />
          <Route path="*" element={<RouteNotFound />} />
        </Routes>
      </div>
    </Box>
  )
}

export default App
