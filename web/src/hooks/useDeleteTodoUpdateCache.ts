import getCurrentUserTodosQuery from '../graphql/query/getCurrentUserTodos'
import { GetCurrentUserTodosQueryVariables, Todo } from '../graphql/generated'
import { useApolloClient } from '@apollo/client'

interface Props {
  variables: GetCurrentUserTodosQueryVariables
}

function useDeleteTodoUpdateTodosCache({ variables }: Props) {
  const client = useApolloClient()

  const cb = (todoId: string) => {
    const oldQueryData = client.readQuery({
      query: getCurrentUserTodosQuery,
      variables
    })

    client.writeQuery({
      query: getCurrentUserTodosQuery,
      variables,
      data: {
        ...oldQueryData,
        getCurrentUserTodos: {
          ...oldQueryData.getCurrentUserTodos,
          count: oldQueryData.getCurrentUserTodos.count - 1,
          rows: oldQueryData.getCurrentUserTodos.rows.filter(
            (row: Todo) => row.id !== todoId
          )
        }
      }
    })
  }

  return {
    cb
  }
}

export default useDeleteTodoUpdateTodosCache
