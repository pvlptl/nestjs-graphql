import { useMutation } from '@apollo/client'
import updateCurrentUserMutation from '../graphql/mutation/updateCurrentUser'
import { useSnackbar } from 'notistack'
import { UpdateUserByIdInput } from '../graphql/generated'
import useCurrentUser from './useCurrentUser'

function useSaveUserSettings() {
  const currentUser = useCurrentUser()
  const [mutate] = useMutation(updateCurrentUserMutation)
  const { enqueueSnackbar } = useSnackbar()

  const handleSubmit = async (data: UpdateUserByIdInput) => {
    try {
      mutate({
        variables: {
          data
        },
        optimisticResponse: () => {
          return {
            updateCurrentUser: {
              ...currentUser.user,
              ...data
            }
          }
        }
      })
    } catch (error: any) {
      enqueueSnackbar(error.message, { variant: 'error' })
      currentUser.onRefetch()
    }
  }

  return {
    onSubmit: handleSubmit
  }
}

export default useSaveUserSettings
