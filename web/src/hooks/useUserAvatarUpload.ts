import { useState } from 'react'

function useUserAvatarUpload() {
  const [src, setSrc] = useState<undefined | string>(undefined)
  const [file, setFile] = useState<File | undefined | any>(undefined)

  const handleReset = () => {
    setSrc(undefined)
    setFile(undefined)
  }

  const handleUpload = (e: any) => {
    const reader = new FileReader()
    const tempFile = e.target.files[0]
    setFile(tempFile)

    reader.addEventListener(
      'load',
      () => {
        setSrc(reader.result as string)
      },
      false
    )

    if (tempFile) {
      reader.readAsDataURL(tempFile)
    }
  }

  return {
    src,
    file,
    onUpload: handleUpload,
    onReset: handleReset
  }
}

export default useUserAvatarUpload
