import { useSetRecoilState } from 'recoil'
import { useApolloClient } from '@apollo/client'
import authTokenAtom from '../recoil/atoms/authTokenAtom'

function useLogout() {
  const client = useApolloClient()
  const setAuthToken = useSetRecoilState(authTokenAtom)

  return () => {
    localStorage.removeItem('auth_token')
    setAuthToken(null)
    client.resetStore()
  }
}
export default useLogout
