import { useApolloClient, useMutation } from '@apollo/client'
import createTodoMutation from '../graphql/mutation/createTodo'
import {
  CreateTodoInput,
  GetCurrentUserTodosQueryVariables
} from '../graphql/generated'
import faker from 'faker'
import getCurrentUserTodosQuery from '../graphql/query/getCurrentUserTodos'
import { useSnackbar } from 'notistack'

interface Props {
  getTodosVariables: GetCurrentUserTodosQueryVariables
  onRefetch: () => void
}

function useCreateTodo({
  getTodosVariables: GET_TODOS_VARIABLES,
  onRefetch
}: Props) {
  const client = useApolloClient()
  const { enqueueSnackbar } = useSnackbar()
  const [mutate, { loading }] = useMutation(createTodoMutation)

  const handleCreateTodoHelper = async (input: CreateTodoInput) => {
    try {
      const mockedTodo = {
        ...input,
        id: faker.datatype.uuid(),
        createdAt: faker.datatype.datetime(),
        __typename: 'Todo'
      }

      const oldQueryData = await client.readQuery({
        query: getCurrentUserTodosQuery,
        variables: GET_TODOS_VARIABLES
      })

      client.writeQuery({
        query: getCurrentUserTodosQuery,
        variables: GET_TODOS_VARIABLES,
        data: {
          ...oldQueryData,
          getCurrentUserTodos: {
            ...oldQueryData.getCurrentUserTodos,
            rows: [mockedTodo, ...oldQueryData.getCurrentUserTodos.rows],
            count: oldQueryData.getCurrentUserTodos.count + 1
          }
        }
      })

      const response = await mutate({
        variables: {
          input
        }
      })

      client.writeQuery({
        query: getCurrentUserTodosQuery,
        variables: GET_TODOS_VARIABLES,
        data: {
          ...oldQueryData,
          getCurrentUserTodos: {
            ...oldQueryData.getCurrentUserTodos,
            rows: [
              response.data.createTodo,
              ...oldQueryData.getCurrentUserTodos.rows
            ].filter(i => i.id !== mockedTodo.id),
            count: oldQueryData.getCurrentUserTodos.count + 1
          }
        }
      })
    } catch (error: any) {
      onRefetch()
      enqueueSnackbar(error.message, { variant: 'error' })
    }
  }

  const handleCreate = async (name: string) => {
    handleCreateTodoHelper({
      name,
      isCompleted: false
    })
  }

  const handleRandomCreate = async () => {
    handleCreateTodoHelper({
      name: faker.name.title(),
      isCompleted: faker.datatype.boolean()
    })
  }

  return {
    loading,
    onCreate: handleCreate,
    onRandomCreate: handleRandomCreate
  }
}

export default useCreateTodo
