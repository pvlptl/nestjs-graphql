import authTokenSelector from '../recoil/selectors/authTokenSelector'
import { useRecoilValue } from 'recoil'
import {
  ApolloClient,
  createHttpLink,
  InMemoryCache,
  split
} from '@apollo/client'
import { setContext } from '@apollo/client/link/context'
// import { createUploadLink } from 'apollo-upload-client'

import { getMainDefinition } from '@apollo/client/utilities'
import { GraphQLWsLink } from '@apollo/client/link/subscriptions'
import { createClient } from 'graphql-ws'

const useGraphqlClient = () => {
  const { authToken } = useRecoilValue(authTokenSelector)

  const httpLink = createHttpLink({
    uri: process.env.REACT_APP_GRAPHQL_API_URL
  })

  // const httpLink = createUploadLink({
  //   uri: process.env.REACT_APP_GRAPHQL_API_URL
  // })

  const wsLink = new GraphQLWsLink(
    createClient({
      url: process.env.REACT_APP_GRAPHQL_API_WEBSOCKETS_URL as string
      // connectionParams: {
      //   authToken: user.authToken,
      // },
    })
  )

  const authLink = setContext((_, { headers }) => {
    return {
      headers: {
        ...headers,
        authorization: authToken ? `Bearer ${authToken}` : ''
      }
    }
  })

  const splitLink = split(
    ({ query }) => {
      const definition = getMainDefinition(query)
      return (
        definition.kind === 'OperationDefinition' &&
        definition.operation === 'subscription'
      )
    },
    wsLink,
    httpLink
  )

  return new ApolloClient({
    uri: process.env.REACT_APP_GRAPHQL_API_URL,
    cache: new InMemoryCache(),
    link: authLink.concat(splitLink),
    connectToDevTools: true
  })
}

export default useGraphqlClient
