import uploadCurrentUserAvatar from '../rest/uploadCurrentUserAvatar'
import { useSnackbar } from 'notistack'

interface Props {
  file: File
}

function useUploadUserAvatar() {
  const { enqueueSnackbar } = useSnackbar()

  const cb = async ({ file }: Props) => {
    try {
      const formData = new FormData()
      formData.append('file', file)
      return uploadCurrentUserAvatar(formData)
    } catch (e: any) {
      enqueueSnackbar(e.message, { variant: 'error' })
    }
  }

  return {
    cb
  }
}

export default useUploadUserAvatar
