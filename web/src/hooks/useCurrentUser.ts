import { useRecoilValue } from 'recoil'
import authTokenSelector from '../recoil/selectors/authTokenSelector'
import currentUserQuery from '../graphql/query/currentUser'
import { useQuery } from '@apollo/client'
import { User } from '../graphql/generated'

function useCurrentUser() {
  const { authToken } = useRecoilValue(authTokenSelector)

  const { loading, data, refetch, error } = useQuery(currentUserQuery, {
    skip: !authToken
  })

  const handleRefetch = async () => {
    const res = await refetch()
    console.log('handleRefetch', res)
  }

  return {
    error,
    user: data?.currentUser as User,
    isFetching: loading,
    onRefetch: handleRefetch
  }
}

export default useCurrentUser
