import { Todo } from '../graphql/generated'
import { useMutation } from '@apollo/client'
import deleteTodoByIdMutation from '../graphql/mutation/deleteTodoById'
import { useSnackbar } from 'notistack'

interface Props {
  onSuccess?: (todoId: string) => void
  onRefetch?: () => void
}

function useDeleteTodo(props: Props) {
  const { onSuccess, onRefetch } = props
  const [mutate] = useMutation(deleteTodoByIdMutation)
  const { enqueueSnackbar } = useSnackbar()

  const handleDelete = async (todo: Todo) => {
    try {
      const { id } = todo
      if (onSuccess) {
        onSuccess(id)
      }

      await mutate({
        variables: {
          id
        }
      })
    } catch (error: any) {
      if (onRefetch) {
        onRefetch()
      }
      enqueueSnackbar(error.message, { variant: 'error' })
    }
  }

  return {
    onDelete: handleDelete
  }
}

export default useDeleteTodo
