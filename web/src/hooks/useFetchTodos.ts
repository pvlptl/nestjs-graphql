import { useState } from 'react'
import { useQuery } from '@apollo/client'
import getCurrentUserTodosQuery from '../graphql/query/getCurrentUserTodos'
import { GetCurrentUserTodosQueryVariables } from '../graphql/generated'

interface Props {
  variables: GetCurrentUserTodosQueryVariables
}

function useFetchTodos({ variables }: Props) {
  const [isFetchingMore, setIsFetchingMore] = useState(false)

  const { refetch, data, fetchMore, updateQuery } = useQuery(
    getCurrentUserTodosQuery,
    {
      variables
    }
  )

  const hasMore =
    data?.getCurrentUserTodos?.rows.length < data?.getCurrentUserTodos?.count

  const handleLoadMore = async () => {
    if (hasMore && !isFetchingMore) {
      try {
        setIsFetchingMore(true)

        const result: any = await fetchMore({
          variables: {
            ...variables,
            filters: {
              ...variables.filters,
              offset: data?.getCurrentUserTodos?.rows.length
            }
          }
        })

        updateQuery(previousQueryResult => {
          return {
            ...previousQueryResult,
            getCurrentUserTodos: {
              ...previousQueryResult.getCurrentUserTodos,
              rows: [
                ...previousQueryResult.getCurrentUserTodos.rows,
                ...result.data.getCurrentUserTodos.rows
              ]
            }
          }
        })
      } finally {
        setIsFetchingMore(false)
      }
    }
  }

  return {
    isFetchingMore,
    onLoadMore: handleLoadMore,
    onRefetch: () => refetch(),
    hasMore,
    rows: data?.getCurrentUserTodos?.rows || [],
    count: data?.getCurrentUserTodos?.count
  }
}

export default useFetchTodos
