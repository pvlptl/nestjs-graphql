import { useMutation } from '@apollo/client'
import updateTodoByIdMutation from '../graphql/mutation/updateTodoById'
import { Todo } from '../graphql/generated'
import { useSnackbar } from 'notistack'

function useToggleTodo() {
  const [mutate] = useMutation(updateTodoByIdMutation)
  const { enqueueSnackbar } = useSnackbar()

  const handleToggle = async (todo: Todo) => {
    try {
      await mutate({
        variables: {
          id: todo.id,
          data: {
            isCompleted: !todo.isCompleted
          }
        },
        optimisticResponse: () => {
          return {
            updateTodoById: {
              ...todo,
              isCompleted: !todo.isCompleted
            }
          }
        }
      })
    } catch (error: any) {
      enqueueSnackbar(error.message, { variant: 'error' })
    }
  }

  return {
    onToggle: handleToggle
  }
}

export default useToggleTodo
