import { useApolloClient } from '@apollo/client'
import currentUserQuery from '../graphql/query/currentUser'
import faker from 'faker'

interface Props {
  url: string
}

function useUploadUserAvatarUpdateCache() {
  const client = useApolloClient()

  const cb = ({ url }: Props) => {
    const oldQueryData = client.readQuery({
      query: currentUserQuery
    })

    client.writeQuery({
      query: currentUserQuery,
      data: {
        ...oldQueryData,
        currentUser: {
          ...oldQueryData.currentUser,
          avatar: {
            ...oldQueryData.currentUser.avatar,
            id: oldQueryData.currentUser.avatar?.id || faker.datatype.uuid(),
            url
          }
        }
      }
    })
  }

  return {
    cb
  }
}

export default useUploadUserAvatarUpdateCache
