import React from 'react'
import SingleCardUi from '../SingleCardUi'
import { PaymentMethodDto } from '../../graphql/generated'

function PaymentMethodsList({ nodes = [], onDelete }: any) {
  return (
    <div
      style={{
        display: 'flex',
        flexDirection: 'column',
        gap: 8
      }}
    >
      {nodes.map((i: PaymentMethodDto) => (
        <SingleCardUi key={i.id} data={i} onDelete={onDelete} />
      ))}
    </div>
  )
}

export default PaymentMethodsList
