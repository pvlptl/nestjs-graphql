import React from 'react'
import BalanceForm from '../BalanceForm'
import Alert from '@mui/material/Alert'

function Balance({ onSubmit, submitLoading, hasPaymentMethods }: any) {
  return (
    <div
      style={{
        maxWidth: 400,
        border: '1px solid lightblue',
        padding: 24,
        borderRadius: 8
      }}
    >
      {!hasPaymentMethods && (
        <Alert style={{ marginBottom: 8 }} severity="warning">
          Please add payment method!
        </Alert>
      )}
      <BalanceForm
        onSubmit={onSubmit}
        submitLoading={submitLoading}
        disabled={!hasPaymentMethods}
      />
    </div>
  )
}

export default Balance
