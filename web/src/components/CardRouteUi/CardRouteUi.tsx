import React from 'react'
import BalanceForm from '../BalanceForm'
import PaymentMethodsList from '../PaymentMethodsList'

function CardRouteUi({
  onSubmit,
  submitLoading,
  paymentMethods,
  onDelete
}: any) {
  return (
    <div>
      <h2>Add New Payment Method</h2>
      <div
        style={{
          maxWidth: 400,
          border: '1px solid lightblue',
          padding: 24,
          borderRadius: 8
        }}
      >
        <BalanceForm
          submitLabel="Save Card"
          onSubmit={onSubmit}
          submitLoading={submitLoading}
        />
      </div>

      <h2>Payment Methods List</h2>
      <PaymentMethodsList nodes={paymentMethods} onDelete={onDelete} />
    </div>
  )
}

export default CardRouteUi
