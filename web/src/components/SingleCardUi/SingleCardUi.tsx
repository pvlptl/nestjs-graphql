import React from 'react'
import { PaymentMethodDto } from '../../graphql/generated'
import DeleteIcon from '@mui/icons-material/Delete'
import IconButton from '@mui/material/IconButton'

interface Props {
  data: PaymentMethodDto
  onDelete: (props: any) => void
}

function SingleCardUi({ data, onDelete }: Props) {
  return (
    <div
      style={{
        backgroundColor: 'lightblue',
        width: 'fit-content',
        borderRadius: 8,
        padding: 8,
        minHeight: 150,
        minWidth: 240,
        display: 'flex',
        flexDirection: 'column',
        justifyContent: 'space-around'
      }}
    >
      <span>{data.card.brand}</span>
      <span>**** **** **** {data.card.last4}</span>
      <span>
        {data.card.exp_month} / {data.card.exp_year}
      </span>
      <span>{data.billing_details.name}</span>
      <IconButton
        onClick={() => onDelete({ data })}
        style={{ width: 'fit-content' }}
      >
        <DeleteIcon />
      </IconButton>
    </div>
  )
}

export default SingleCardUi
