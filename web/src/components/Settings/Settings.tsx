import React from 'react'
import { Props } from './types'
import TextField from '@mui/material/TextField'
import Button from '@mui/material/Button'
import DeleteIcon from '@mui/icons-material/DeleteForever'
import { Grid, IconButton } from '@mui/material'
import { useFormik } from 'formik'
import Avatar from '../Avatar'
import UploadFile from '../UploadFile'

function Settings(props: Props) {
  const {
    onSubmit,
    initialValues,
    onUpload,
    avatarSrc,
    onDelete,
    isDeletable
  } = props

  const formik = useFormik({
    initialValues,
    onSubmit
  })

  return (
    <Grid
      container
      sx={{
        maxWidth: 400,
        flexDirection: 'column',
        gap: theme => theme.spacing(2)
      }}
    >
      {avatarSrc && <Avatar alt="User Avatar" src={avatarSrc} />}
      <Grid
        container
        sx={{
          alignItems: 'center'
        }}
      >
        <UploadFile onChange={onUpload} />
        {isDeletable && (
          <IconButton
            onClick={onDelete}
            color="error"
            aria-label="delete picture"
            component="span"
          >
            <DeleteIcon />
          </IconButton>
        )}
      </Grid>
      <TextField
        label="First Name"
        variant="outlined"
        placeholder="Enter First Name"
        name="firstName"
        onChange={formik.handleChange}
        value={formik.values.firstName}
      />
      <TextField
        label="Last Name"
        variant="outlined"
        placeholder="Enter Last Name"
        name="lastName"
        onChange={formik.handleChange}
        value={formik.values.lastName}
      />
      <TextField
        label="Email"
        variant="outlined"
        placeholder="Email"
        name="email"
        onChange={formik.handleChange}
        value={formik.values.email}
      />
      <Button onClick={() => formik.handleSubmit()} variant="contained">
        Submit
      </Button>
    </Grid>
  )
}

export default Settings
