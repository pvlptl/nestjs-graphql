import { UpdateUserByIdInput } from '../../graphql/generated'

export interface Props {
  onSubmit: (data: UpdateUserByIdInput) => void
  onUpload: (e: any) => void
  onDelete: () => void
  initialValues: UpdateUserByIdInput
  avatarSrc?: string
  isDeletable: boolean
}
