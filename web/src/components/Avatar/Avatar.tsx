import React from 'react'
import MuiAvatar from '@mui/material/Avatar'
import { Props } from './types'

export default function Avatar(props: Props) {
  const { alt, src, size = 150 } = props
  return <MuiAvatar alt={alt} src={src} sx={{ width: size, height: size }} />
}
