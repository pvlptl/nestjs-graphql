export interface Props {
  src: string
  alt: string
  size?: number
}
