import React, { useState } from 'react'
import TextField from '@mui/material/TextField'
import Button from '@mui/material/Button'
import Modal from '@mui/material/Modal'
import Box from '@mui/material/Box'
import Typography from '@mui/material/Typography'
import ListItem from '@mui/material/ListItem'
import ListItemButton from '@mui/material/ListItemButton'
import ListItemText from '@mui/material/ListItemText'
import ListItemAvatar from '@mui/material/ListItemAvatar'
import Checkbox from '@mui/material/Checkbox'
import IconButton from '@mui/material/IconButton'
import ExtensionIcon from '@mui/icons-material/Extension'
import DeleteIcon from '@mui/icons-material/Delete'
import { Props } from './types'
import { useSnackbar } from 'notistack'
import { InfiniteLoader, List } from 'react-virtualized'
import { Paper } from '@mui/material'
import { useResizeDetector } from 'react-resize-detector'
import { Link } from 'react-router-dom'

function Todos({
  rows,
  limit,
  // isFetchingMore,
  totalRowsCount,
  onLoadMoreTodos,
  onChangeLimit,
  onAddRandomTodo,
  onDeleteTodo,
  // hasMoreTodos,
  onToggle,
  onRefetch,
  createTodoLoading,
  onCreateTodo
}: Props) {
  const [todoName, setTodoName] = useState('')
  const [open, setOpen] = useState(false)
  const { enqueueSnackbar } = useSnackbar()
  const { width = 0, ref } = useResizeDetector()

  const handleOpen = () => setOpen(true)
  const handleClose = () => setOpen(false)

  const handleCreateTodo = () => {
    try {
      onCreateTodo(todoName)
      handleClose()
      setTodoName('')
    } catch (error: any) {
      enqueueSnackbar(error.message, { variant: 'error' })
    }
  }

  function isRowLoaded({ index }: any) {
    return !!rows[index]
  }

  const rowRenderer = ({ index, key, style }: any) => {
    if (!isRowLoaded({ index })) {
      return (
        <ListItem
          style={style}
          key={key}
          secondaryAction={
            <>
              <Checkbox edge="end" checked={false} />
              <IconButton>
                <DeleteIcon />
              </IconButton>
            </>
          }
          disablePadding
        >
          <ListItemButton>
            <ListItemAvatar>
              <ExtensionIcon />
            </ListItemAvatar>
            <ListItemText primary="Loading..." />
          </ListItemButton>
        </ListItem>
      )
    }

    const todo = rows[index]

    return (
      <Link style={style} key={key} to={`/todos/${todo.id}`}>
        <ListItem
          secondaryAction={
            <>
              <Checkbox
                onClick={e => {
                  e.preventDefault()
                  onToggle(todo)
                }}
                edge="end"
                // onChange={() => onToggle(todo)}
                checked={todo.isCompleted}
              />
              <IconButton
                onClick={e => {
                  e.preventDefault()
                  onDeleteTodo(todo)
                }}
              >
                <DeleteIcon />
              </IconButton>
            </>
          }
          disablePadding
        >
          <ListItemButton sx={{ color: 'black' }}>
            <ListItemAvatar>
              <ExtensionIcon />
            </ListItemAvatar>
            <ListItemText
              sx={{ color: 'black' }}
              id={todo.id}
              primary={todo.name}
            />
          </ListItemButton>
        </ListItem>
      </Link>
    )
  }

  return (
    <div>
      <TextField
        label="Todos Limit"
        variant="outlined"
        onChange={onChangeLimit}
        type="number"
        placeholder="Set todos limit"
        value={limit}
      />

      <br />
      <br />

      <div
        style={{
          display: 'flex',
          alignItems: 'center',
          gap: 24,
          marginBottom: 24
        }}
      >
        <Button onClick={handleOpen} variant="contained">
          Add Todo
        </Button>

        <Button onClick={onAddRandomTodo} variant="contained">
          Fast Add Random Todo
        </Button>

        <Button onClick={onRefetch} variant="contained">
          Refetch todos
        </Button>
      </div>

      <Paper ref={ref}>
        <InfiniteLoader
          isRowLoaded={isRowLoaded}
          loadMoreRows={onLoadMoreTodos as any}
          rowCount={totalRowsCount}
        >
          {({ onRowsRendered, registerChild }) => (
            <List
              height={500}
              onRowsRendered={onRowsRendered}
              ref={registerChild}
              rowCount={totalRowsCount}
              rowHeight={48}
              rowRenderer={rowRenderer}
              width={width}
            />
          )}
        </InfiniteLoader>
      </Paper>

      <Modal
        open={open}
        onClose={handleClose}
        aria-labelledby="modal-modal-title"
        aria-describedby="modal-modal-description"
      >
        <Box
          sx={{
            position: 'absolute',
            top: '50%',
            left: '50%',
            transform: 'translate(-50%, -50%)',
            width: 400,
            bgcolor: 'background.paper',
            border: '2px solid #000',
            boxShadow: 24,
            p: 4
          }}
        >
          <Typography id="modal-modal-title" variant="h6" component="h2">
            Add Todo
          </Typography>
          <Box sx={{ mt: 2 }}>
            <TextField
              label="Todo Name"
              variant="outlined"
              placeholder="Enter Todo Name"
              name="name"
              value={todoName}
              onChange={e => setTodoName(e.target.value)}
            />

            <br />
            <br />

            <Button
              disabled={createTodoLoading}
              onClick={handleCreateTodo}
              variant="contained"
            >
              Submit
            </Button>
          </Box>
        </Box>
      </Modal>
    </div>
  )
}

export default Todos
