import { Todo } from '../../graphql/generated'

export interface Props {
  rows: Todo[]
  limit: number
  totalRowsCount: number
  onChangeLimit: (e: any) => void
  onToggle: (todo: Todo) => void
  onDeleteTodo: (todo: Todo) => void
  onAddRandomTodo: () => void
  onRefetch: () => void
  onLoadMoreTodos: () => void
  hasMoreTodos: boolean
  isFetchingMore: boolean
  createTodoLoading: boolean
  onCreateTodo: (name: string) => void
}
