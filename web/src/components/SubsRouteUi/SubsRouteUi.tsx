import React from 'react'
import moment from 'moment'
import { SubscriptionDto } from '../../graphql/generated'
import parseStripeUnitAmount from '../../utils/parseStripeUnitAmount'
import Button from '@mui/material/Button'
import Alert from '@mui/material/Alert'

function SubsRouteUi({ nodes = [], onDelete, onRetryPay }: any) {
  return (
    <div
      style={{
        maxWidth: 500,
        display: 'flex',
        flexDirection: 'column',
        gap: 24
      }}
    >
      {nodes.length === 0 && (
        <Alert severity="warning">You dont have subscriptions</Alert>
      )}
      {nodes.map((i: SubscriptionDto) => (
        <div
          key={i.id}
          style={{
            display: 'flex',
            flexDirection: 'column',
            gap: 8,
            border: '1px solid lightblue',
            borderRadius: 8,
            padding: 8
          }}
        >
          <img
            style={{ maxWidth: 200 }}
            src={i.plan?.product_details?.images[0]}
            alt={i.plan?.product_details?.name}
          />
          <span>
            Subscription plan: <b>{i.plan?.product_details?.name}</b>
          </span>
          <span>
            Status: <b>{i.status}</b>
          </span>
          <span>
            {parseStripeUnitAmount(i.plan.amount as number)} {i.plan.currency} /{' '}
            {i.plan.interval}
          </span>
          <span>
            Next invoice on:{' '}
            <b>
              {moment(i.current_period_end * 1000).format('DD MMM YYYY HH:mm')}
            </b>
          </span>
          <Button
            onClick={() => onDelete({ id: i.id })}
            style={{ width: 'fit-content' }}
            variant="contained"
          >
            Cancel subscription
          </Button>
          {i.status === 'incomplete' && (
            <Button
              onClick={() =>
                onRetryPay({ id: i.id, latest_invoice: i.latest_invoice })
              }
              style={{ width: 'fit-content' }}
              variant="outlined"
            >
              Retry payment
            </Button>
          )}
        </div>
      ))}
    </div>
  )
}

export default SubsRouteUi
