import React from 'react'
import SinglePlanSection from '../SinglePlanSection'
import { ProductDto } from '../../graphql/generated'
import Alert from '@mui/material/Alert'

function PlansRouteUi({
  nodes = [],
  selectedPlan,
  submitLoading,
  onSubmit,
  hasPaymentMethods
}: any) {
  return (
    <div style={{ display: 'flex', flexDirection: 'column', gap: 8 }}>
      {!hasPaymentMethods && (
        <Alert severity="warning">Please add payment method!</Alert>
      )}

      {nodes.map((i: ProductDto) => (
        <SinglePlanSection
          key={i.id}
          name={i.name}
          description={i.description}
          images={i.images}
          prices={i.prices}
          onSubmit={onSubmit}
          submitLoading={submitLoading && selectedPlan === i.id}
          planId={i.id}
          hasPaymentMethods={hasPaymentMethods}
        />
      ))}
    </div>
  )
}

export default PlansRouteUi
