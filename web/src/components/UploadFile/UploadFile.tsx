import React from 'react'
import { styled } from '@mui/material/styles'
import IconButton from '@mui/material/IconButton'
import PhotoCamera from '@mui/icons-material/PhotoCamera'
import { Props } from './types'

const Input = styled('input')({
  display: 'none'
})

export default function UploadFile({ onChange }: Props) {
  return (
    <label htmlFor="icon-button-file">
      <Input
        onChange={onChange}
        accept="image/*"
        id="icon-button-file"
        type="file"
      />
      <IconButton color="primary" aria-label="upload picture" component="span">
        <PhotoCamera />
      </IconButton>
    </label>
  )
}
