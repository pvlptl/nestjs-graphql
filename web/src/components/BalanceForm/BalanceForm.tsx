import { useStripe, CardElement } from '@stripe/react-stripe-js'
import Button from '@mui/material/Button'

const BalanceForm = ({ onSubmit, submitLoading, submitLabel = 'Pay' }: any) => {
  const stripe = useStripe()

  return (
    <form onSubmit={onSubmit}>
      <CardElement options={{ hidePostalCode: true }} />
      <br />
      <Button
        type="submit"
        variant="contained"
        disabled={!stripe || submitLoading}
      >
        {submitLabel}
      </Button>
    </form>
  )
}

export default BalanceForm
