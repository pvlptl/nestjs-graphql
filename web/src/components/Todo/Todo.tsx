import React from 'react'
import { Props } from './types'
import Button from '@mui/material/Button'
import TextField from '@mui/material/TextField'
import Checkbox from '@mui/material/Checkbox'
import { Grid, Typography } from '@mui/material'
import { useFormik } from 'formik'

function Todo(props: Props) {
  const { initialValues, onSubmit, onDelete } = props

  const formik = useFormik({
    initialValues,
    onSubmit
  })

  const handleToggle = () => {
    formik.setFieldValue('isCompleted', !formik.values.isCompleted)
  }

  const handleSave = () => {
    formik.handleSubmit()
  }

  return (
    <form onSubmit={formik.handleSubmit}>
      <Grid
        container
        sx={{
          alignItems: 'start',
          flexDirection: 'column',
          gap: theme => theme.spacing(3)
        }}
      >
        <TextField
          label="Todo Name"
          variant="outlined"
          placeholder="Enter Todo Name"
          name="name"
          onChange={formik.handleChange}
          value={formik.values.name}
        />

        <Grid container alignItems="center">
          <Typography>Completed:</Typography>
          <Checkbox
            edge="end"
            onChange={handleToggle}
            checked={formik.values.isCompleted}
          />
        </Grid>

        <Button onClick={onDelete} color="error" variant="contained">
          Delete
        </Button>
        <Button onClick={handleSave} variant="contained">
          Save
        </Button>
      </Grid>
    </form>
  )
}

export default Todo
