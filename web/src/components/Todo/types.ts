export interface InitialValues {
  name: string
  isCompleted: boolean
}

export interface Props {
  initialValues: InitialValues
  onSubmit: (values: InitialValues) => void
  onDelete: () => void
}
