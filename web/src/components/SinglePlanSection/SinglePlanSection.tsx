import Button from '@mui/material/Button'
import React from 'react'
import parseStripeUnitAmount from '../../utils/parseStripeUnitAmount'

function SinglePlanSection({
  name,
  description,
  images = [],
  prices = [],
  onSubmit,
  submitLoading,
  planId,
  hasPaymentMethods
}: any) {
  const price = prices[0]

  const handleSubmit = () => {
    if (onSubmit) {
      onSubmit(planId, price.id)
    }
  }

  return (
    <div
      style={{
        alignItems: 'start',
        border: '1px solid lightblue',
        borderRadius: 8,
        padding: 8,
        display: 'flex',
        flexDirection: 'column',
        gap: 8
      }}
    >
      <img style={{ maxWidth: 200 }} src={images} alt={name} />
      <b>{name}</b>
      <p>{description}</p>
      <p>
        {parseStripeUnitAmount(price.unit_amount)} {price.currency} /{' '}
        {price.recurring.interval}
      </p>
      <Button
        disabled={submitLoading || !hasPaymentMethods}
        onClick={handleSubmit}
        type="submit"
        variant="contained"
      >
        Subscribe
      </Button>
    </div>
  )
}

export default SinglePlanSection
