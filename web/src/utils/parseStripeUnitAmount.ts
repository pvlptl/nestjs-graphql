function parseStripeUnitAmount(amount: number) {
  return amount / 100
}

export default parseStripeUnitAmount
