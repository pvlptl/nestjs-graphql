import getOr from 'lodash/fp/getOr'

function getAxiosError(error: any) {
  return getOr(error, ['response', 'data'], error)
}

export default getAxiosError
