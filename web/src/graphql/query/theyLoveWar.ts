import { gql } from '@apollo/client'

const theyLoveWarQuery = gql`
  query TheyLoveWar {
    theyLoveWar {
      id
      name
      comment
      country_code
      main_image
      social_links {
        id
        url
        type
      }
      images {
        id
        src
      }
    }
  }
`

export default theyLoveWarQuery
