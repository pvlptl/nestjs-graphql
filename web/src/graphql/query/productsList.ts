import { gql } from '@apollo/client'

const productsListQuery = gql`
  query ProductsList {
    productsList {
      id
      active
      description
      name
      images
      prices {
        id
        active
        currency
        billing_scheme
        type
        unit_amount
        unit_amount_decimal
        recurring {
          aggregate_usage
          interval
          interval_count
          trial_period_days
          usage_type
        }
      }
    }
  }
`

export default productsListQuery
