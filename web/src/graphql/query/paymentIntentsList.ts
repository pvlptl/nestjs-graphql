import { gql } from '@apollo/client'

const paymentIntentsListQuery = gql`
  query PaymentIntentsList {
    paymentIntentsList {
      id
      client_secret
      description
      amount
      currency
      status
      invoice {
        id
        paid
        status
        invoice_pdf
        hosted_invoice_url
        lines {
          has_more
          url
          data {
            id
            amount
            currency
            description
            invoice_item
            plan {
              id
              active
              amount
              amount_decimal
              created
              currency
              interval
              interval_count
              product
              product_details {
                id
                active
                description
                images
                name
              }
            }
            proration
            quantity
            subscription
            subscription_item
            type
          }
        }
      }
    }
  }
`

export default paymentIntentsListQuery
