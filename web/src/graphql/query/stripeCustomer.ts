import { gql } from '@apollo/client'

const stripeCustomerQuery = gql`
  query StripeCustomer {
    stripeCustomer {
      id
      balance
      currency
      delinquent
      description
      email
      invoice_prefix
      name
      phone
      address {
        city
        country
        line1
        line2
        postal_code
        state
      }
    }
  }
`

export default stripeCustomerQuery
