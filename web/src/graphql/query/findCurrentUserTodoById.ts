import { gql } from '@apollo/client'

const findCurrentUserTodoByIdMutation = gql`
  query FindCurrentUserTodoById($id: String!) {
    findCurrentUserTodoById(id: $id) {
      id
      isCompleted
      name
      createdAt
    }
  }
`

export default findCurrentUserTodoByIdMutation
