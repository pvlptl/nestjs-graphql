import { gql } from '@apollo/client'

const getCurrentUserTodosQuery = gql`
  query GetCurrentUserTodos($filters: TodoFiltersDto!) {
    getCurrentUserTodos(filters: $filters) {
      rows {
        id
        isCompleted
        name
        createdAt
      }
      count
    }
  }
`

export default getCurrentUserTodosQuery
