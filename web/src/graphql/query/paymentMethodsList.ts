import { gql } from '@apollo/client'

const paymentMethodsListQuery = gql`
  query PaymentMethodsList {
    paymentMethodsList {
      id
      card {
        brand
        country
        exp_year
        exp_month
        fingerprint
        funding
        last4
      }
      billing_details {
        name
      }
    }
  }
`

export default paymentMethodsListQuery
