import { gql } from '@apollo/client'

const subsListQuery = gql`
  query SubsList {
    subsList {
      id
      cancel_at
      cancel_at_period_end
      canceled_at
      current_period_end
      current_period_start
      days_until_due
      ended_at
      start_date
      status
      latest_invoice
      plan {
        id
        active
        amount
        amount_decimal
        created
        currency
        interval
        interval_count
        product
        product_details {
          id
          active
          description
          images
          name
        }
      }
    }
  }
`

export default subsListQuery
