import { gql } from '@apollo/client'

const currentUserQuery = gql`
  query CurrentUser {
    currentUser {
      id
      email
      firstName
      lastName
      avatar {
        id
        url
      }
    }
  }
`

export default currentUserQuery
