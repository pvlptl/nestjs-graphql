import { gql } from '@apollo/client'

const invoicesListQuery = gql`
  query InvoicesList {
    invoicesList {
      id
      amount_due
      amount_paid
      amount_remaining
      attempt_count
      attempted
      currency
      next_payment_attempt
      period_start
      period_end
      created
      paid
      payment_intent
      status
      invoice_pdf
      hosted_invoice_url
      billing_reason
      collection_method
      lines {
        has_more
        url
        data {
          id
          amount
          currency
          description
          invoice_item
          plan {
            id
            active
            amount
            amount_decimal
            created
            currency
            interval
            interval_count
            product
            product_details {
              id
              active
              description
              images
              name
            }
          }
          proration
          quantity
          subscription
          subscription_item
          type
        }
      }
    }
  }
`

export default invoicesListQuery
