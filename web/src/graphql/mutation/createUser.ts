import { gql } from '@apollo/client'

const createUserMutation = gql`
  mutation CreateUser($input: CreateUserInput!) {
    createUser(input: $input) {
      id
      email
      firstName
      lastName
      createdAt
    }
  }
`

export default createUserMutation
