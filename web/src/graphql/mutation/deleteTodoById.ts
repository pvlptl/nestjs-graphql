import { gql } from '@apollo/client'

const deleteTodoByIdMutation = gql`
  mutation DeleteTodoById($id: String!) {
    deleteTodoById(id: $id) {
      id
      isCompleted
      name
      createdAt
    }
  }
`

export default deleteTodoByIdMutation
