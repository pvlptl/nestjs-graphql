import { gql } from '@apollo/client'

const paymentMethodDetachMutation = gql`
  mutation PaymentMethodDetach($input: PaymentMethodDetachInput!) {
    paymentMethodDetach(input: $input) {
      id
      card {
        brand
        country
        exp_year
        exp_month
        fingerprint
        funding
        last4
      }
      billing_details {
        name
      }
    }
  }
`

export default paymentMethodDetachMutation
