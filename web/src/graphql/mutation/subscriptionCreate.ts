import { gql } from '@apollo/client'

const subscriptionCreateMutation = gql`
  mutation SubscriptionCreate($input: SubscriptionCreateInput!) {
    subscriptionsCreate(input: $input) {
      subscription {
        id
        cancel_at
        cancel_at_period_end
        canceled_at
        current_period_end
        current_period_start
        days_until_due
        ended_at
        start_date
        status
        plan {
          currency
          amount
        }
      }
      paymentIntent {
        id
        client_secret
        status
      }
    }
  }
`

export default subscriptionCreateMutation
