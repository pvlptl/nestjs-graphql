import { gql } from '@apollo/client'

const stripeCustomerUpdateMutation = gql`
  mutation StripeCustomerUpdate($input: CustomersUpdateInput!) {
    stripeCustomerUpdate(input: $input) {
      id
      address {
        city
        country
        line1
        line2
        postal_code
        state
      }
      balance
      created
      currency
      delinquent
      description
      email
      invoice_prefix
      name
      next_invoice_sequence
      phone
    }
  }
`

export default stripeCustomerUpdateMutation
