import { gql } from '@apollo/client'

const subDelMutation = gql`
  mutation SubDel($input: SubDelInput!) {
    subDel(input: $input) {
      id
    }
  }
`

export default subDelMutation
