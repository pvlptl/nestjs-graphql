import { gql } from '@apollo/client'

const updateTodoByIdMutation = gql`
  mutation UpdateTodoById($id: String!, $data: UpdateTodoByIdInput!) {
    updateTodoById(id: $id, data: $data) {
      id
      isCompleted
      name
      createdAt
    }
  }
`

export default updateTodoByIdMutation
