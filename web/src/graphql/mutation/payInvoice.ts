import { gql } from '@apollo/client'

const payInvoiceMutation = gql`
  mutation PayInvoice($input: PayInvoiceInput!) {
    payInvoice(input: $input) {
      paymentIntent {
        id
        client_secret
        status
      }
      invoice {
        id
        paid
        payment_intent
        status
      }
    }
  }
`

export default payInvoiceMutation
