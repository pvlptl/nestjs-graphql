import { gql } from '@apollo/client'

const deleteCurrentUserAvatarMutation = gql`
  mutation DeleteCurrentUserAvatar {
    deleteCurrentUserAvatar {
      id
      email
      firstName
      lastName
      avatar {
        id
        url
      }
    }
  }
`

export default deleteCurrentUserAvatarMutation
