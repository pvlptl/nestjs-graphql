import { gql } from '@apollo/client'

const createChargeMutation = gql`
  mutation CreateCharge($input: CreateChargeInput!) {
    createCharge(input: $input) {
      id
      status
      client_secret
      redirect_to_url
    }
  }
`

export default createChargeMutation
