import { gql } from '@apollo/client'

const updateCurrentUserMutation = gql`
  mutation UpdateCurrentUser($data: UpdateUserByIdInput!) {
    updateCurrentUser(data: $data) {
      id
      email
      firstName
      lastName
    }
  }
`

export default updateCurrentUserMutation
