import { gql } from '@apollo/client'

const createSetupIntentMutation = gql`
  mutation CreateSetupIntent {
    createSetupIntent {
      id
      client_secret
      status
    }
  }
`

export default createSetupIntentMutation
