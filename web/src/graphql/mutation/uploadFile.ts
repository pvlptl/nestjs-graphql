import { gql } from '@apollo/client'

const UploadFileMutation = gql`
  mutation UploadFile($file: Upload!) {
    uploadFile(file: $file)
  }
`

export default UploadFileMutation
