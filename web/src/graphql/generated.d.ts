import { gql } from '@apollo/client';
import * as Apollo from '@apollo/client';
import * as ApolloReactHooks from '@apollo/client';
import gql from 'graphql-tag';
export type Maybe<T> = T | null;
export type Exact<T extends { [key: string]: unknown }> = { [K in keyof T]: T[K] };
export type MakeOptional<T, K extends keyof T> = Omit<T, K> & { [SubKey in K]?: Maybe<T[SubKey]> };
export type MakeMaybe<T, K extends keyof T> = Omit<T, K> & { [SubKey in K]: Maybe<T[SubKey]> };
const defaultOptions =  {}
/** All built-in and custom scalars, mapped to their actual values */
export type Scalars = {
  ID: string;
  String: string;
  Boolean: boolean;
  Int: number;
  Float: number;
  /** A date-time string at UTC, such as 2019-12-03T09:54:33Z, compliant with the date-time format. */
  DateTime: any;
  /** The `Upload` scalar type represents a file upload. */
  Upload: any;
};

export type AddressDto = {
  __typename?: 'AddressDto';
  city?: Maybe<Scalars['String']>;
  country?: Maybe<Scalars['String']>;
  line1?: Maybe<Scalars['String']>;
  line2?: Maybe<Scalars['String']>;
  postal_code?: Maybe<Scalars['String']>;
  state?: Maybe<Scalars['String']>;
};

export type BillingDetailsDto = {
  __typename?: 'BillingDetailsDto';
  address: AddressDto;
  email?: Maybe<Scalars['String']>;
  name?: Maybe<Scalars['String']>;
  phone?: Maybe<Scalars['String']>;
};

export type CardDto = {
  __typename?: 'CardDto';
  brand: Scalars['String'];
  country: Scalars['String'];
  exp_month: Scalars['Float'];
  exp_year: Scalars['Float'];
  fingerprint: Scalars['String'];
  funding: Scalars['String'];
  last4: Scalars['String'];
};

export type CreateChargeInput = {
  amount: Scalars['Float'];
  paymentMethodId: Scalars['String'];
};

export type CreateChargeResponse = {
  __typename?: 'CreateChargeResponse';
  client_secret?: Maybe<Scalars['String']>;
  id: Scalars['String'];
  redirect_to_url?: Maybe<Scalars['String']>;
  status: Scalars['String'];
};

export type CreateTodoInput = {
  isCompleted?: Maybe<Scalars['Boolean']>;
  name: Scalars['String'];
};

export type CreateUserInput = {
  email: Scalars['String'];
  firstName: Scalars['String'];
  lastName: Scalars['String'];
  password: Scalars['String'];
  stripeCustomerId?: Maybe<Scalars['String']>;
};

export type CustomerDto = {
  __typename?: 'CustomerDto';
  address?: Maybe<AddressDto>;
  balance: Scalars['Float'];
  created: Scalars['Float'];
  currency?: Maybe<Scalars['String']>;
  delinquent?: Maybe<Scalars['Boolean']>;
  description?: Maybe<Scalars['String']>;
  email?: Maybe<Scalars['String']>;
  id: Scalars['String'];
  invoice_prefix?: Maybe<Scalars['String']>;
  name?: Maybe<Scalars['String']>;
  next_invoice_sequence: Scalars['Float'];
  phone?: Maybe<Scalars['String']>;
};

export type CustomersUpdateInput = {
  invoice_settings: InvoiceSettingsDto;
};

export type FindManyTodosByUserIdResultDto = {
  __typename?: 'FindManyTodosByUserIdResultDto';
  count: Scalars['Float'];
  rows: Array<Todo>;
};

export type InvoiceDto = {
  __typename?: 'InvoiceDto';
  amount_due: Scalars['Float'];
  amount_paid: Scalars['Float'];
  amount_remaining: Scalars['Float'];
  attempt_count: Scalars['Float'];
  attempted: Scalars['Boolean'];
  billing_reason?: Maybe<Scalars['String']>;
  collection_method: Scalars['String'];
  created: Scalars['Float'];
  currency: Scalars['String'];
  hosted_invoice_url?: Maybe<Scalars['String']>;
  id: Scalars['String'];
  invoice_pdf?: Maybe<Scalars['String']>;
  lines: InvoiceLinesDto;
  next_payment_attempt?: Maybe<Scalars['Float']>;
  paid: Scalars['Boolean'];
  payment_intent?: Maybe<Scalars['String']>;
  period_end: Scalars['Float'];
  period_start: Scalars['Float'];
  status?: Maybe<Scalars['String']>;
};

export type InvoiceLineItemDto = {
  __typename?: 'InvoiceLineItemDto';
  amount: Scalars['Float'];
  currency: Scalars['String'];
  description?: Maybe<Scalars['String']>;
  id: Scalars['String'];
  invoice_item?: Maybe<Scalars['String']>;
  plan?: Maybe<PlanDto>;
  proration: Scalars['Boolean'];
  quantity?: Maybe<Scalars['Float']>;
  subscription?: Maybe<Scalars['String']>;
  subscription_item: Scalars['String'];
  type: Scalars['String'];
};

export type InvoiceLinesDto = {
  __typename?: 'InvoiceLinesDto';
  data: Array<InvoiceLineItemDto>;
  has_more: Scalars['Boolean'];
  url: Scalars['String'];
};

export type InvoiceSettingsDto = {
  default_payment_method?: Maybe<Scalars['String']>;
};

export type Mutation = {
  __typename?: 'Mutation';
  addComment: Scalars['String'];
  createCharge: CreateChargeResponse;
  createSetupIntent: SetupIntentDto;
  createTodo: Todo;
  createUser: User;
  deleteCurrentUserAvatar: User;
  deleteTodoById: Todo;
  payInvoice: PayInvoiceResponseDto;
  paymentMethodDetach: PaymentMethodDto;
  stripeCustomerUpdate: CustomerDto;
  subDel: SubscriptionDto;
  subscriptionsCreate: SubCreateResponseDto;
  updateCurrentUser: User;
  updateTodoById: Todo;
  uploadFile: Scalars['Boolean'];
};


export type MutationAddCommentArgs = {
  comment: Scalars['String'];
};


export type MutationCreateChargeArgs = {
  input: CreateChargeInput;
};


export type MutationCreateTodoArgs = {
  input: CreateTodoInput;
};


export type MutationCreateUserArgs = {
  input: CreateUserInput;
};


export type MutationDeleteTodoByIdArgs = {
  id: Scalars['String'];
};


export type MutationPayInvoiceArgs = {
  input: PayInvoiceInput;
};


export type MutationPaymentMethodDetachArgs = {
  input: PaymentMethodDetachInput;
};


export type MutationStripeCustomerUpdateArgs = {
  input: CustomersUpdateInput;
};


export type MutationSubDelArgs = {
  input: SubDelInput;
};


export type MutationSubscriptionsCreateArgs = {
  input: SubscriptionCreateInput;
};


export type MutationUpdateCurrentUserArgs = {
  data: UpdateUserByIdInput;
};


export type MutationUpdateTodoByIdArgs = {
  data: UpdateTodoByIdInput;
  id: Scalars['String'];
};


export type MutationUploadFileArgs = {
  file: Scalars['Upload'];
};

export type NextActionDto = {
  __typename?: 'NextActionDto';
  redirect_to_url?: Maybe<RedirectToUrlDto>;
  type: Scalars['String'];
  use_stripe_sdk: Scalars['String'];
};

export type PayInvoiceInput = {
  invoiceId: Scalars['String'];
};

export type PayInvoiceResponseDto = {
  __typename?: 'PayInvoiceResponseDto';
  invoice: InvoiceDto;
  paymentIntent: PaymentIntentDto;
};

export type PaymentIntentDto = {
  __typename?: 'PaymentIntentDto';
  amount: Scalars['Float'];
  amount_capturable: Scalars['Float'];
  amount_received: Scalars['Float'];
  canceled_at?: Maybe<Scalars['Float']>;
  cancellation_reason?: Maybe<Scalars['String']>;
  capture_method: Scalars['String'];
  client_secret?: Maybe<Scalars['String']>;
  confirmation_method: Scalars['String'];
  created: Scalars['Float'];
  currency: Scalars['String'];
  description?: Maybe<Scalars['String']>;
  id: Scalars['String'];
  invoice?: Maybe<InvoiceDto>;
  next_action?: Maybe<NextActionDto>;
  payment_method?: Maybe<Scalars['String']>;
  status: Scalars['String'];
};

export type PaymentMethodDetachInput = {
  paymentMethodId: Scalars['String'];
};

export type PaymentMethodDto = {
  __typename?: 'PaymentMethodDto';
  billing_details: BillingDetailsDto;
  card: CardDto;
  id: Scalars['String'];
};

export type PlanDto = {
  __typename?: 'PlanDto';
  active: Scalars['Boolean'];
  amount?: Maybe<Scalars['Float']>;
  amount_decimal?: Maybe<Scalars['String']>;
  created: Scalars['Float'];
  currency: Scalars['String'];
  id: Scalars['String'];
  interval: Scalars['String'];
  interval_count: Scalars['Float'];
  product: Scalars['String'];
  product_details?: Maybe<ProductDto>;
};

export type PriceDto = {
  __typename?: 'PriceDto';
  active: Scalars['Boolean'];
  billing_scheme: Scalars['String'];
  currency: Scalars['String'];
  id: Scalars['String'];
  recurring: PriceRecurringDto;
  type: Scalars['String'];
  unit_amount: Scalars['Float'];
  unit_amount_decimal: Scalars['String'];
};

export type PriceRecurringDto = {
  __typename?: 'PriceRecurringDto';
  aggregate_usage?: Maybe<Scalars['String']>;
  interval: Scalars['String'];
  interval_count: Scalars['Float'];
  trial_period_days?: Maybe<Scalars['Float']>;
  usage_type: Scalars['String'];
};

export type ProductDto = {
  __typename?: 'ProductDto';
  active: Scalars['Boolean'];
  description?: Maybe<Scalars['String']>;
  id: Scalars['String'];
  images: Array<Scalars['String']>;
  name: Scalars['String'];
  prices: Array<PriceDto>;
};

export type Query = {
  __typename?: 'Query';
  currentUser: User;
  findCurrentUserTodoById: Todo;
  getCurrentUserTodos: FindManyTodosByUserIdResultDto;
  invoicesList: Array<InvoiceDto>;
  paymentIntentsList: Array<PaymentIntentDto>;
  paymentMethodsList: Array<PaymentMethodDto>;
  productsList: Array<ProductDto>;
  stripeCustomer: StripeCustomerDto;
  subsList: Array<SubscriptionDto>;
  theyLoveWar: Array<TheyLoveWarNodeDto>;
};


export type QueryFindCurrentUserTodoByIdArgs = {
  id: Scalars['String'];
};


export type QueryGetCurrentUserTodosArgs = {
  filters: TodoFiltersDto;
};

export type RedirectToUrlDto = {
  __typename?: 'RedirectToUrlDto';
  return_url?: Maybe<Scalars['String']>;
  url?: Maybe<Scalars['String']>;
};

export type SetupIntentDto = {
  __typename?: 'SetupIntentDto';
  client_secret?: Maybe<Scalars['String']>;
  id: Scalars['String'];
  status: Scalars['String'];
};

export type StripeCustomerDto = {
  __typename?: 'StripeCustomerDto';
  address?: Maybe<AddressDto>;
  balance: Scalars['Float'];
  currency?: Maybe<Scalars['String']>;
  delinquent?: Maybe<Scalars['Boolean']>;
  description?: Maybe<Scalars['String']>;
  email?: Maybe<Scalars['String']>;
  id: Scalars['String'];
  invoice_prefix?: Maybe<Scalars['String']>;
  name?: Maybe<Scalars['String']>;
  phone?: Maybe<Scalars['String']>;
};

export type SubCreateResponseDto = {
  __typename?: 'SubCreateResponseDto';
  paymentIntent: PaymentIntentDto;
  subscription: SubscriptionDto;
};

export type SubDelInput = {
  subId: Scalars['String'];
};

export type Subscription = {
  __typename?: 'Subscription';
  commentAdded: Scalars['String'];
};

export type SubscriptionCreateInput = {
  priceId: Scalars['String'];
};

export type SubscriptionDto = {
  __typename?: 'SubscriptionDto';
  cancel_at?: Maybe<Scalars['Float']>;
  cancel_at_period_end: Scalars['Boolean'];
  canceled_at?: Maybe<Scalars['Float']>;
  current_period_end: Scalars['Float'];
  current_period_start: Scalars['Float'];
  days_until_due?: Maybe<Scalars['Float']>;
  ended_at?: Maybe<Scalars['Float']>;
  id: Scalars['String'];
  latest_invoice?: Maybe<Scalars['String']>;
  plan: PlanDto;
  start_date: Scalars['Float'];
  status: Scalars['String'];
};

export type TheyLoveWarImageDto = {
  __typename?: 'TheyLoveWarImageDto';
  id: Scalars['String'];
  src: Scalars['String'];
};

export type TheyLoveWarNodeDto = {
  __typename?: 'TheyLoveWarNodeDto';
  comment: Scalars['String'];
  country_code: Scalars['String'];
  id: Scalars['String'];
  images: Array<TheyLoveWarImageDto>;
  main_image: Scalars['String'];
  name: Scalars['String'];
  social_links: Array<TheyLoveWarSocialLinkDto>;
};

export type TheyLoveWarSocialLinkDto = {
  __typename?: 'TheyLoveWarSocialLinkDto';
  id: Scalars['String'];
  type: Scalars['String'];
  url: Scalars['String'];
};

export type Todo = {
  __typename?: 'Todo';
  createdAt: Scalars['DateTime'];
  id: Scalars['ID'];
  isCompleted: Scalars['Boolean'];
  name: Scalars['String'];
  updatedAt: Scalars['DateTime'];
  user: User;
  userId: Scalars['ID'];
};

export type TodoFiltersDto = {
  limit?: Maybe<Scalars['Float']>;
  offset?: Maybe<Scalars['Float']>;
};

export type UpdateTodoByIdInput = {
  isCompleted?: Maybe<Scalars['Boolean']>;
  name?: Maybe<Scalars['String']>;
};

export type UpdateUserByIdInput = {
  email?: Maybe<Scalars['String']>;
  firstName?: Maybe<Scalars['String']>;
  lastName?: Maybe<Scalars['String']>;
};

export type User = {
  __typename?: 'User';
  avatar?: Maybe<UserAvatar>;
  createdAt: Scalars['DateTime'];
  email: Scalars['String'];
  firstName: Scalars['String'];
  id: Scalars['ID'];
  lastName: Scalars['String'];
  todos?: Maybe<Array<Todo>>;
  updatedAt: Scalars['DateTime'];
};

export type UserAvatar = {
  __typename?: 'UserAvatar';
  createdAt: Scalars['DateTime'];
  id: Scalars['ID'];
  key: Scalars['String'];
  updatedAt: Scalars['DateTime'];
  url: Scalars['String'];
  user: User;
  userId: Scalars['ID'];
};

export type CreateChargeMutationVariables = Exact<{
  input: CreateChargeInput;
}>;


export type CreateChargeMutation = { __typename?: 'Mutation', createCharge: { __typename?: 'CreateChargeResponse', id: string, status: string, client_secret?: string | null | undefined, redirect_to_url?: string | null | undefined } };

export type CreateSetupIntentMutationVariables = Exact<{ [key: string]: never; }>;


export type CreateSetupIntentMutation = { __typename?: 'Mutation', createSetupIntent: { __typename?: 'SetupIntentDto', id: string, client_secret?: string | null | undefined, status: string } };

export type CreateTodoMutationVariables = Exact<{
  input: CreateTodoInput;
}>;


export type CreateTodoMutation = { __typename?: 'Mutation', createTodo: { __typename?: 'Todo', id: string, isCompleted: boolean, name: string, createdAt: any } };

export type CreateUserMutationVariables = Exact<{
  input: CreateUserInput;
}>;


export type CreateUserMutation = { __typename?: 'Mutation', createUser: { __typename?: 'User', id: string, email: string, firstName: string, lastName: string, createdAt: any } };

export type DeleteCurrentUserAvatarMutationVariables = Exact<{ [key: string]: never; }>;


export type DeleteCurrentUserAvatarMutation = { __typename?: 'Mutation', deleteCurrentUserAvatar: { __typename?: 'User', id: string, email: string, firstName: string, lastName: string, avatar?: { __typename?: 'UserAvatar', id: string, url: string } | null | undefined } };

export type DeleteTodoByIdMutationVariables = Exact<{
  id: Scalars['String'];
}>;


export type DeleteTodoByIdMutation = { __typename?: 'Mutation', deleteTodoById: { __typename?: 'Todo', id: string, isCompleted: boolean, name: string, createdAt: any } };

export type PayInvoiceMutationVariables = Exact<{
  input: PayInvoiceInput;
}>;


export type PayInvoiceMutation = { __typename?: 'Mutation', payInvoice: { __typename?: 'PayInvoiceResponseDto', paymentIntent: { __typename?: 'PaymentIntentDto', id: string, client_secret?: string | null | undefined, status: string }, invoice: { __typename?: 'InvoiceDto', id: string, paid: boolean, payment_intent?: string | null | undefined, status?: string | null | undefined } } };

export type PaymentMethodDetachMutationVariables = Exact<{
  input: PaymentMethodDetachInput;
}>;


export type PaymentMethodDetachMutation = { __typename?: 'Mutation', paymentMethodDetach: { __typename?: 'PaymentMethodDto', id: string, card: { __typename?: 'CardDto', brand: string, country: string, exp_year: number, exp_month: number, fingerprint: string, funding: string, last4: string }, billing_details: { __typename?: 'BillingDetailsDto', name?: string | null | undefined } } };

export type StripeCustomerUpdateMutationVariables = Exact<{
  input: CustomersUpdateInput;
}>;


export type StripeCustomerUpdateMutation = { __typename?: 'Mutation', stripeCustomerUpdate: { __typename?: 'CustomerDto', id: string, balance: number, created: number, currency?: string | null | undefined, delinquent?: boolean | null | undefined, description?: string | null | undefined, email?: string | null | undefined, invoice_prefix?: string | null | undefined, name?: string | null | undefined, next_invoice_sequence: number, phone?: string | null | undefined, address?: { __typename?: 'AddressDto', city?: string | null | undefined, country?: string | null | undefined, line1?: string | null | undefined, line2?: string | null | undefined, postal_code?: string | null | undefined, state?: string | null | undefined } | null | undefined } };

export type SubDelMutationVariables = Exact<{
  input: SubDelInput;
}>;


export type SubDelMutation = { __typename?: 'Mutation', subDel: { __typename?: 'SubscriptionDto', id: string } };

export type SubscriptionCreateMutationVariables = Exact<{
  input: SubscriptionCreateInput;
}>;


export type SubscriptionCreateMutation = { __typename?: 'Mutation', subscriptionsCreate: { __typename?: 'SubCreateResponseDto', subscription: { __typename?: 'SubscriptionDto', id: string, cancel_at?: number | null | undefined, cancel_at_period_end: boolean, canceled_at?: number | null | undefined, current_period_end: number, current_period_start: number, days_until_due?: number | null | undefined, ended_at?: number | null | undefined, start_date: number, status: string, plan: { __typename?: 'PlanDto', currency: string, amount?: number | null | undefined } }, paymentIntent: { __typename?: 'PaymentIntentDto', id: string, client_secret?: string | null | undefined, status: string } } };

export type UpdateCurrentUserMutationVariables = Exact<{
  data: UpdateUserByIdInput;
}>;


export type UpdateCurrentUserMutation = { __typename?: 'Mutation', updateCurrentUser: { __typename?: 'User', id: string, email: string, firstName: string, lastName: string } };

export type UpdateTodoByIdMutationVariables = Exact<{
  id: Scalars['String'];
  data: UpdateTodoByIdInput;
}>;


export type UpdateTodoByIdMutation = { __typename?: 'Mutation', updateTodoById: { __typename?: 'Todo', id: string, isCompleted: boolean, name: string, createdAt: any } };

export type UploadFileMutationVariables = Exact<{
  file: Scalars['Upload'];
}>;


export type UploadFileMutation = { __typename?: 'Mutation', uploadFile: boolean };

export type CurrentUserQueryVariables = Exact<{ [key: string]: never; }>;


export type CurrentUserQuery = { __typename?: 'Query', currentUser: { __typename?: 'User', id: string, email: string, firstName: string, lastName: string, avatar?: { __typename?: 'UserAvatar', id: string, url: string } | null | undefined } };

export type FindCurrentUserTodoByIdQueryVariables = Exact<{
  id: Scalars['String'];
}>;


export type FindCurrentUserTodoByIdQuery = { __typename?: 'Query', findCurrentUserTodoById: { __typename?: 'Todo', id: string, isCompleted: boolean, name: string, createdAt: any } };

export type GetCurrentUserTodosQueryVariables = Exact<{
  filters: TodoFiltersDto;
}>;


export type GetCurrentUserTodosQuery = { __typename?: 'Query', getCurrentUserTodos: { __typename?: 'FindManyTodosByUserIdResultDto', count: number, rows: Array<{ __typename?: 'Todo', id: string, isCompleted: boolean, name: string, createdAt: any }> } };

export type InvoicesListQueryVariables = Exact<{ [key: string]: never; }>;


export type InvoicesListQuery = { __typename?: 'Query', invoicesList: Array<{ __typename?: 'InvoiceDto', id: string, amount_due: number, amount_paid: number, amount_remaining: number, attempt_count: number, attempted: boolean, currency: string, next_payment_attempt?: number | null | undefined, period_start: number, period_end: number, created: number, paid: boolean, payment_intent?: string | null | undefined, status?: string | null | undefined, invoice_pdf?: string | null | undefined, hosted_invoice_url?: string | null | undefined, billing_reason?: string | null | undefined, collection_method: string, lines: { __typename?: 'InvoiceLinesDto', has_more: boolean, url: string, data: Array<{ __typename?: 'InvoiceLineItemDto', id: string, amount: number, currency: string, description?: string | null | undefined, invoice_item?: string | null | undefined, proration: boolean, quantity?: number | null | undefined, subscription?: string | null | undefined, subscription_item: string, type: string, plan?: { __typename?: 'PlanDto', id: string, active: boolean, amount?: number | null | undefined, amount_decimal?: string | null | undefined, created: number, currency: string, interval: string, interval_count: number, product: string, product_details?: { __typename?: 'ProductDto', id: string, active: boolean, description?: string | null | undefined, images: Array<string>, name: string } | null | undefined } | null | undefined }> } }> };

export type PaymentIntentsListQueryVariables = Exact<{ [key: string]: never; }>;


export type PaymentIntentsListQuery = { __typename?: 'Query', paymentIntentsList: Array<{ __typename?: 'PaymentIntentDto', id: string, client_secret?: string | null | undefined, description?: string | null | undefined, amount: number, currency: string, status: string, invoice?: { __typename?: 'InvoiceDto', id: string, paid: boolean, status?: string | null | undefined, invoice_pdf?: string | null | undefined, hosted_invoice_url?: string | null | undefined, lines: { __typename?: 'InvoiceLinesDto', has_more: boolean, url: string, data: Array<{ __typename?: 'InvoiceLineItemDto', id: string, amount: number, currency: string, description?: string | null | undefined, invoice_item?: string | null | undefined, proration: boolean, quantity?: number | null | undefined, subscription?: string | null | undefined, subscription_item: string, type: string, plan?: { __typename?: 'PlanDto', id: string, active: boolean, amount?: number | null | undefined, amount_decimal?: string | null | undefined, created: number, currency: string, interval: string, interval_count: number, product: string, product_details?: { __typename?: 'ProductDto', id: string, active: boolean, description?: string | null | undefined, images: Array<string>, name: string } | null | undefined } | null | undefined }> } } | null | undefined }> };

export type PaymentMethodsListQueryVariables = Exact<{ [key: string]: never; }>;


export type PaymentMethodsListQuery = { __typename?: 'Query', paymentMethodsList: Array<{ __typename?: 'PaymentMethodDto', id: string, card: { __typename?: 'CardDto', brand: string, country: string, exp_year: number, exp_month: number, fingerprint: string, funding: string, last4: string }, billing_details: { __typename?: 'BillingDetailsDto', name?: string | null | undefined } }> };

export type ProductsListQueryVariables = Exact<{ [key: string]: never; }>;


export type ProductsListQuery = { __typename?: 'Query', productsList: Array<{ __typename?: 'ProductDto', id: string, active: boolean, description?: string | null | undefined, name: string, images: Array<string>, prices: Array<{ __typename?: 'PriceDto', id: string, active: boolean, currency: string, billing_scheme: string, type: string, unit_amount: number, unit_amount_decimal: string, recurring: { __typename?: 'PriceRecurringDto', aggregate_usage?: string | null | undefined, interval: string, interval_count: number, trial_period_days?: number | null | undefined, usage_type: string } }> }> };

export type StripeCustomerQueryVariables = Exact<{ [key: string]: never; }>;


export type StripeCustomerQuery = { __typename?: 'Query', stripeCustomer: { __typename?: 'StripeCustomerDto', id: string, balance: number, currency?: string | null | undefined, delinquent?: boolean | null | undefined, description?: string | null | undefined, email?: string | null | undefined, invoice_prefix?: string | null | undefined, name?: string | null | undefined, phone?: string | null | undefined, address?: { __typename?: 'AddressDto', city?: string | null | undefined, country?: string | null | undefined, line1?: string | null | undefined, line2?: string | null | undefined, postal_code?: string | null | undefined, state?: string | null | undefined } | null | undefined } };

export type SubsListQueryVariables = Exact<{ [key: string]: never; }>;


export type SubsListQuery = { __typename?: 'Query', subsList: Array<{ __typename?: 'SubscriptionDto', id: string, cancel_at?: number | null | undefined, cancel_at_period_end: boolean, canceled_at?: number | null | undefined, current_period_end: number, current_period_start: number, days_until_due?: number | null | undefined, ended_at?: number | null | undefined, start_date: number, status: string, latest_invoice?: string | null | undefined, plan: { __typename?: 'PlanDto', id: string, active: boolean, amount?: number | null | undefined, amount_decimal?: string | null | undefined, created: number, currency: string, interval: string, interval_count: number, product: string, product_details?: { __typename?: 'ProductDto', id: string, active: boolean, description?: string | null | undefined, images: Array<string>, name: string } | null | undefined } }> };

export type TheyLoveWarQueryVariables = Exact<{ [key: string]: never; }>;


export type TheyLoveWarQuery = { __typename?: 'Query', theyLoveWar: Array<{ __typename?: 'TheyLoveWarNodeDto', id: string, name: string, comment: string, country_code: string, main_image: string, social_links: Array<{ __typename?: 'TheyLoveWarSocialLinkDto', id: string, url: string, type: string }>, images: Array<{ __typename?: 'TheyLoveWarImageDto', id: string, src: string }> }> };

export type OnCommentAddedSubscriptionVariables = Exact<{ [key: string]: never; }>;


export type OnCommentAddedSubscription = { __typename?: 'Subscription', commentAdded: string };


export const CreateChargeDocument = gql`
    mutation CreateCharge($input: CreateChargeInput!) {
  createCharge(input: $input) {
    id
    status
    client_secret
    redirect_to_url
  }
}
    `;
export type CreateChargeMutationFn = Apollo.MutationFunction<CreateChargeMutation, CreateChargeMutationVariables>;

/**
 * __useCreateChargeMutation__
 *
 * To run a mutation, you first call `useCreateChargeMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useCreateChargeMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [createChargeMutation, { data, loading, error }] = useCreateChargeMutation({
 *   variables: {
 *      input: // value for 'input'
 *   },
 * });
 */
export function useCreateChargeMutation(baseOptions?: ApolloReactHooks.MutationHookOptions<CreateChargeMutation, CreateChargeMutationVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return ApolloReactHooks.useMutation<CreateChargeMutation, CreateChargeMutationVariables>(CreateChargeDocument, options);
      }
export type CreateChargeMutationHookResult = ReturnType<typeof useCreateChargeMutation>;
export type CreateChargeMutationResult = Apollo.MutationResult<CreateChargeMutation>;
export type CreateChargeMutationOptions = Apollo.BaseMutationOptions<CreateChargeMutation, CreateChargeMutationVariables>;
export const CreateSetupIntentDocument = gql`
    mutation CreateSetupIntent {
  createSetupIntent {
    id
    client_secret
    status
  }
}
    `;
export type CreateSetupIntentMutationFn = Apollo.MutationFunction<CreateSetupIntentMutation, CreateSetupIntentMutationVariables>;

/**
 * __useCreateSetupIntentMutation__
 *
 * To run a mutation, you first call `useCreateSetupIntentMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useCreateSetupIntentMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [createSetupIntentMutation, { data, loading, error }] = useCreateSetupIntentMutation({
 *   variables: {
 *   },
 * });
 */
export function useCreateSetupIntentMutation(baseOptions?: ApolloReactHooks.MutationHookOptions<CreateSetupIntentMutation, CreateSetupIntentMutationVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return ApolloReactHooks.useMutation<CreateSetupIntentMutation, CreateSetupIntentMutationVariables>(CreateSetupIntentDocument, options);
      }
export type CreateSetupIntentMutationHookResult = ReturnType<typeof useCreateSetupIntentMutation>;
export type CreateSetupIntentMutationResult = Apollo.MutationResult<CreateSetupIntentMutation>;
export type CreateSetupIntentMutationOptions = Apollo.BaseMutationOptions<CreateSetupIntentMutation, CreateSetupIntentMutationVariables>;
export const CreateTodoDocument = gql`
    mutation CreateTodo($input: CreateTodoInput!) {
  createTodo(input: $input) {
    id
    isCompleted
    name
    createdAt
  }
}
    `;
export type CreateTodoMutationFn = Apollo.MutationFunction<CreateTodoMutation, CreateTodoMutationVariables>;

/**
 * __useCreateTodoMutation__
 *
 * To run a mutation, you first call `useCreateTodoMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useCreateTodoMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [createTodoMutation, { data, loading, error }] = useCreateTodoMutation({
 *   variables: {
 *      input: // value for 'input'
 *   },
 * });
 */
export function useCreateTodoMutation(baseOptions?: ApolloReactHooks.MutationHookOptions<CreateTodoMutation, CreateTodoMutationVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return ApolloReactHooks.useMutation<CreateTodoMutation, CreateTodoMutationVariables>(CreateTodoDocument, options);
      }
export type CreateTodoMutationHookResult = ReturnType<typeof useCreateTodoMutation>;
export type CreateTodoMutationResult = Apollo.MutationResult<CreateTodoMutation>;
export type CreateTodoMutationOptions = Apollo.BaseMutationOptions<CreateTodoMutation, CreateTodoMutationVariables>;
export const CreateUserDocument = gql`
    mutation CreateUser($input: CreateUserInput!) {
  createUser(input: $input) {
    id
    email
    firstName
    lastName
    createdAt
  }
}
    `;
export type CreateUserMutationFn = Apollo.MutationFunction<CreateUserMutation, CreateUserMutationVariables>;

/**
 * __useCreateUserMutation__
 *
 * To run a mutation, you first call `useCreateUserMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useCreateUserMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [createUserMutation, { data, loading, error }] = useCreateUserMutation({
 *   variables: {
 *      input: // value for 'input'
 *   },
 * });
 */
export function useCreateUserMutation(baseOptions?: ApolloReactHooks.MutationHookOptions<CreateUserMutation, CreateUserMutationVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return ApolloReactHooks.useMutation<CreateUserMutation, CreateUserMutationVariables>(CreateUserDocument, options);
      }
export type CreateUserMutationHookResult = ReturnType<typeof useCreateUserMutation>;
export type CreateUserMutationResult = Apollo.MutationResult<CreateUserMutation>;
export type CreateUserMutationOptions = Apollo.BaseMutationOptions<CreateUserMutation, CreateUserMutationVariables>;
export const DeleteCurrentUserAvatarDocument = gql`
    mutation DeleteCurrentUserAvatar {
  deleteCurrentUserAvatar {
    id
    email
    firstName
    lastName
    avatar {
      id
      url
    }
  }
}
    `;
export type DeleteCurrentUserAvatarMutationFn = Apollo.MutationFunction<DeleteCurrentUserAvatarMutation, DeleteCurrentUserAvatarMutationVariables>;

/**
 * __useDeleteCurrentUserAvatarMutation__
 *
 * To run a mutation, you first call `useDeleteCurrentUserAvatarMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useDeleteCurrentUserAvatarMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [deleteCurrentUserAvatarMutation, { data, loading, error }] = useDeleteCurrentUserAvatarMutation({
 *   variables: {
 *   },
 * });
 */
export function useDeleteCurrentUserAvatarMutation(baseOptions?: ApolloReactHooks.MutationHookOptions<DeleteCurrentUserAvatarMutation, DeleteCurrentUserAvatarMutationVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return ApolloReactHooks.useMutation<DeleteCurrentUserAvatarMutation, DeleteCurrentUserAvatarMutationVariables>(DeleteCurrentUserAvatarDocument, options);
      }
export type DeleteCurrentUserAvatarMutationHookResult = ReturnType<typeof useDeleteCurrentUserAvatarMutation>;
export type DeleteCurrentUserAvatarMutationResult = Apollo.MutationResult<DeleteCurrentUserAvatarMutation>;
export type DeleteCurrentUserAvatarMutationOptions = Apollo.BaseMutationOptions<DeleteCurrentUserAvatarMutation, DeleteCurrentUserAvatarMutationVariables>;
export const DeleteTodoByIdDocument = gql`
    mutation DeleteTodoById($id: String!) {
  deleteTodoById(id: $id) {
    id
    isCompleted
    name
    createdAt
  }
}
    `;
export type DeleteTodoByIdMutationFn = Apollo.MutationFunction<DeleteTodoByIdMutation, DeleteTodoByIdMutationVariables>;

/**
 * __useDeleteTodoByIdMutation__
 *
 * To run a mutation, you first call `useDeleteTodoByIdMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useDeleteTodoByIdMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [deleteTodoByIdMutation, { data, loading, error }] = useDeleteTodoByIdMutation({
 *   variables: {
 *      id: // value for 'id'
 *   },
 * });
 */
export function useDeleteTodoByIdMutation(baseOptions?: ApolloReactHooks.MutationHookOptions<DeleteTodoByIdMutation, DeleteTodoByIdMutationVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return ApolloReactHooks.useMutation<DeleteTodoByIdMutation, DeleteTodoByIdMutationVariables>(DeleteTodoByIdDocument, options);
      }
export type DeleteTodoByIdMutationHookResult = ReturnType<typeof useDeleteTodoByIdMutation>;
export type DeleteTodoByIdMutationResult = Apollo.MutationResult<DeleteTodoByIdMutation>;
export type DeleteTodoByIdMutationOptions = Apollo.BaseMutationOptions<DeleteTodoByIdMutation, DeleteTodoByIdMutationVariables>;
export const PayInvoiceDocument = gql`
    mutation PayInvoice($input: PayInvoiceInput!) {
  payInvoice(input: $input) {
    paymentIntent {
      id
      client_secret
      status
    }
    invoice {
      id
      paid
      payment_intent
      status
    }
  }
}
    `;
export type PayInvoiceMutationFn = Apollo.MutationFunction<PayInvoiceMutation, PayInvoiceMutationVariables>;

/**
 * __usePayInvoiceMutation__
 *
 * To run a mutation, you first call `usePayInvoiceMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `usePayInvoiceMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [payInvoiceMutation, { data, loading, error }] = usePayInvoiceMutation({
 *   variables: {
 *      input: // value for 'input'
 *   },
 * });
 */
export function usePayInvoiceMutation(baseOptions?: ApolloReactHooks.MutationHookOptions<PayInvoiceMutation, PayInvoiceMutationVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return ApolloReactHooks.useMutation<PayInvoiceMutation, PayInvoiceMutationVariables>(PayInvoiceDocument, options);
      }
export type PayInvoiceMutationHookResult = ReturnType<typeof usePayInvoiceMutation>;
export type PayInvoiceMutationResult = Apollo.MutationResult<PayInvoiceMutation>;
export type PayInvoiceMutationOptions = Apollo.BaseMutationOptions<PayInvoiceMutation, PayInvoiceMutationVariables>;
export const PaymentMethodDetachDocument = gql`
    mutation PaymentMethodDetach($input: PaymentMethodDetachInput!) {
  paymentMethodDetach(input: $input) {
    id
    card {
      brand
      country
      exp_year
      exp_month
      fingerprint
      funding
      last4
    }
    billing_details {
      name
    }
  }
}
    `;
export type PaymentMethodDetachMutationFn = Apollo.MutationFunction<PaymentMethodDetachMutation, PaymentMethodDetachMutationVariables>;

/**
 * __usePaymentMethodDetachMutation__
 *
 * To run a mutation, you first call `usePaymentMethodDetachMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `usePaymentMethodDetachMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [paymentMethodDetachMutation, { data, loading, error }] = usePaymentMethodDetachMutation({
 *   variables: {
 *      input: // value for 'input'
 *   },
 * });
 */
export function usePaymentMethodDetachMutation(baseOptions?: ApolloReactHooks.MutationHookOptions<PaymentMethodDetachMutation, PaymentMethodDetachMutationVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return ApolloReactHooks.useMutation<PaymentMethodDetachMutation, PaymentMethodDetachMutationVariables>(PaymentMethodDetachDocument, options);
      }
export type PaymentMethodDetachMutationHookResult = ReturnType<typeof usePaymentMethodDetachMutation>;
export type PaymentMethodDetachMutationResult = Apollo.MutationResult<PaymentMethodDetachMutation>;
export type PaymentMethodDetachMutationOptions = Apollo.BaseMutationOptions<PaymentMethodDetachMutation, PaymentMethodDetachMutationVariables>;
export const StripeCustomerUpdateDocument = gql`
    mutation StripeCustomerUpdate($input: CustomersUpdateInput!) {
  stripeCustomerUpdate(input: $input) {
    id
    address {
      city
      country
      line1
      line2
      postal_code
      state
    }
    balance
    created
    currency
    delinquent
    description
    email
    invoice_prefix
    name
    next_invoice_sequence
    phone
  }
}
    `;
export type StripeCustomerUpdateMutationFn = Apollo.MutationFunction<StripeCustomerUpdateMutation, StripeCustomerUpdateMutationVariables>;

/**
 * __useStripeCustomerUpdateMutation__
 *
 * To run a mutation, you first call `useStripeCustomerUpdateMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useStripeCustomerUpdateMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [stripeCustomerUpdateMutation, { data, loading, error }] = useStripeCustomerUpdateMutation({
 *   variables: {
 *      input: // value for 'input'
 *   },
 * });
 */
export function useStripeCustomerUpdateMutation(baseOptions?: ApolloReactHooks.MutationHookOptions<StripeCustomerUpdateMutation, StripeCustomerUpdateMutationVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return ApolloReactHooks.useMutation<StripeCustomerUpdateMutation, StripeCustomerUpdateMutationVariables>(StripeCustomerUpdateDocument, options);
      }
export type StripeCustomerUpdateMutationHookResult = ReturnType<typeof useStripeCustomerUpdateMutation>;
export type StripeCustomerUpdateMutationResult = Apollo.MutationResult<StripeCustomerUpdateMutation>;
export type StripeCustomerUpdateMutationOptions = Apollo.BaseMutationOptions<StripeCustomerUpdateMutation, StripeCustomerUpdateMutationVariables>;
export const SubDelDocument = gql`
    mutation SubDel($input: SubDelInput!) {
  subDel(input: $input) {
    id
  }
}
    `;
export type SubDelMutationFn = Apollo.MutationFunction<SubDelMutation, SubDelMutationVariables>;

/**
 * __useSubDelMutation__
 *
 * To run a mutation, you first call `useSubDelMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useSubDelMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [subDelMutation, { data, loading, error }] = useSubDelMutation({
 *   variables: {
 *      input: // value for 'input'
 *   },
 * });
 */
export function useSubDelMutation(baseOptions?: ApolloReactHooks.MutationHookOptions<SubDelMutation, SubDelMutationVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return ApolloReactHooks.useMutation<SubDelMutation, SubDelMutationVariables>(SubDelDocument, options);
      }
export type SubDelMutationHookResult = ReturnType<typeof useSubDelMutation>;
export type SubDelMutationResult = Apollo.MutationResult<SubDelMutation>;
export type SubDelMutationOptions = Apollo.BaseMutationOptions<SubDelMutation, SubDelMutationVariables>;
export const SubscriptionCreateDocument = gql`
    mutation SubscriptionCreate($input: SubscriptionCreateInput!) {
  subscriptionsCreate(input: $input) {
    subscription {
      id
      cancel_at
      cancel_at_period_end
      canceled_at
      current_period_end
      current_period_start
      days_until_due
      ended_at
      start_date
      status
      plan {
        currency
        amount
      }
    }
    paymentIntent {
      id
      client_secret
      status
    }
  }
}
    `;
export type SubscriptionCreateMutationFn = Apollo.MutationFunction<SubscriptionCreateMutation, SubscriptionCreateMutationVariables>;

/**
 * __useSubscriptionCreateMutation__
 *
 * To run a mutation, you first call `useSubscriptionCreateMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useSubscriptionCreateMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [subscriptionCreateMutation, { data, loading, error }] = useSubscriptionCreateMutation({
 *   variables: {
 *      input: // value for 'input'
 *   },
 * });
 */
export function useSubscriptionCreateMutation(baseOptions?: ApolloReactHooks.MutationHookOptions<SubscriptionCreateMutation, SubscriptionCreateMutationVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return ApolloReactHooks.useMutation<SubscriptionCreateMutation, SubscriptionCreateMutationVariables>(SubscriptionCreateDocument, options);
      }
export type SubscriptionCreateMutationHookResult = ReturnType<typeof useSubscriptionCreateMutation>;
export type SubscriptionCreateMutationResult = Apollo.MutationResult<SubscriptionCreateMutation>;
export type SubscriptionCreateMutationOptions = Apollo.BaseMutationOptions<SubscriptionCreateMutation, SubscriptionCreateMutationVariables>;
export const UpdateCurrentUserDocument = gql`
    mutation UpdateCurrentUser($data: UpdateUserByIdInput!) {
  updateCurrentUser(data: $data) {
    id
    email
    firstName
    lastName
  }
}
    `;
export type UpdateCurrentUserMutationFn = Apollo.MutationFunction<UpdateCurrentUserMutation, UpdateCurrentUserMutationVariables>;

/**
 * __useUpdateCurrentUserMutation__
 *
 * To run a mutation, you first call `useUpdateCurrentUserMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useUpdateCurrentUserMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [updateCurrentUserMutation, { data, loading, error }] = useUpdateCurrentUserMutation({
 *   variables: {
 *      data: // value for 'data'
 *   },
 * });
 */
export function useUpdateCurrentUserMutation(baseOptions?: ApolloReactHooks.MutationHookOptions<UpdateCurrentUserMutation, UpdateCurrentUserMutationVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return ApolloReactHooks.useMutation<UpdateCurrentUserMutation, UpdateCurrentUserMutationVariables>(UpdateCurrentUserDocument, options);
      }
export type UpdateCurrentUserMutationHookResult = ReturnType<typeof useUpdateCurrentUserMutation>;
export type UpdateCurrentUserMutationResult = Apollo.MutationResult<UpdateCurrentUserMutation>;
export type UpdateCurrentUserMutationOptions = Apollo.BaseMutationOptions<UpdateCurrentUserMutation, UpdateCurrentUserMutationVariables>;
export const UpdateTodoByIdDocument = gql`
    mutation UpdateTodoById($id: String!, $data: UpdateTodoByIdInput!) {
  updateTodoById(id: $id, data: $data) {
    id
    isCompleted
    name
    createdAt
  }
}
    `;
export type UpdateTodoByIdMutationFn = Apollo.MutationFunction<UpdateTodoByIdMutation, UpdateTodoByIdMutationVariables>;

/**
 * __useUpdateTodoByIdMutation__
 *
 * To run a mutation, you first call `useUpdateTodoByIdMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useUpdateTodoByIdMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [updateTodoByIdMutation, { data, loading, error }] = useUpdateTodoByIdMutation({
 *   variables: {
 *      id: // value for 'id'
 *      data: // value for 'data'
 *   },
 * });
 */
export function useUpdateTodoByIdMutation(baseOptions?: ApolloReactHooks.MutationHookOptions<UpdateTodoByIdMutation, UpdateTodoByIdMutationVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return ApolloReactHooks.useMutation<UpdateTodoByIdMutation, UpdateTodoByIdMutationVariables>(UpdateTodoByIdDocument, options);
      }
export type UpdateTodoByIdMutationHookResult = ReturnType<typeof useUpdateTodoByIdMutation>;
export type UpdateTodoByIdMutationResult = Apollo.MutationResult<UpdateTodoByIdMutation>;
export type UpdateTodoByIdMutationOptions = Apollo.BaseMutationOptions<UpdateTodoByIdMutation, UpdateTodoByIdMutationVariables>;
export const UploadFileDocument = gql`
    mutation UploadFile($file: Upload!) {
  uploadFile(file: $file)
}
    `;
export type UploadFileMutationFn = Apollo.MutationFunction<UploadFileMutation, UploadFileMutationVariables>;

/**
 * __useUploadFileMutation__
 *
 * To run a mutation, you first call `useUploadFileMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useUploadFileMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [uploadFileMutation, { data, loading, error }] = useUploadFileMutation({
 *   variables: {
 *      file: // value for 'file'
 *   },
 * });
 */
export function useUploadFileMutation(baseOptions?: ApolloReactHooks.MutationHookOptions<UploadFileMutation, UploadFileMutationVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return ApolloReactHooks.useMutation<UploadFileMutation, UploadFileMutationVariables>(UploadFileDocument, options);
      }
export type UploadFileMutationHookResult = ReturnType<typeof useUploadFileMutation>;
export type UploadFileMutationResult = Apollo.MutationResult<UploadFileMutation>;
export type UploadFileMutationOptions = Apollo.BaseMutationOptions<UploadFileMutation, UploadFileMutationVariables>;
export const CurrentUserDocument = gql`
    query CurrentUser {
  currentUser {
    id
    email
    firstName
    lastName
    avatar {
      id
      url
    }
  }
}
    `;

/**
 * __useCurrentUserQuery__
 *
 * To run a query within a React component, call `useCurrentUserQuery` and pass it any options that fit your needs.
 * When your component renders, `useCurrentUserQuery` returns an object from Apollo Client that contains loading, error, and data properties
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the query, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = useCurrentUserQuery({
 *   variables: {
 *   },
 * });
 */
export function useCurrentUserQuery(baseOptions?: ApolloReactHooks.QueryHookOptions<CurrentUserQuery, CurrentUserQueryVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return ApolloReactHooks.useQuery<CurrentUserQuery, CurrentUserQueryVariables>(CurrentUserDocument, options);
      }
export function useCurrentUserLazyQuery(baseOptions?: ApolloReactHooks.LazyQueryHookOptions<CurrentUserQuery, CurrentUserQueryVariables>) {
          const options = {...defaultOptions, ...baseOptions}
          return ApolloReactHooks.useLazyQuery<CurrentUserQuery, CurrentUserQueryVariables>(CurrentUserDocument, options);
        }
export type CurrentUserQueryHookResult = ReturnType<typeof useCurrentUserQuery>;
export type CurrentUserLazyQueryHookResult = ReturnType<typeof useCurrentUserLazyQuery>;
export type CurrentUserQueryResult = Apollo.QueryResult<CurrentUserQuery, CurrentUserQueryVariables>;
export function refetchCurrentUserQuery(variables?: CurrentUserQueryVariables) {
      return { query: CurrentUserDocument, variables: variables }
    }
export const FindCurrentUserTodoByIdDocument = gql`
    query FindCurrentUserTodoById($id: String!) {
  findCurrentUserTodoById(id: $id) {
    id
    isCompleted
    name
    createdAt
  }
}
    `;

/**
 * __useFindCurrentUserTodoByIdQuery__
 *
 * To run a query within a React component, call `useFindCurrentUserTodoByIdQuery` and pass it any options that fit your needs.
 * When your component renders, `useFindCurrentUserTodoByIdQuery` returns an object from Apollo Client that contains loading, error, and data properties
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the query, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = useFindCurrentUserTodoByIdQuery({
 *   variables: {
 *      id: // value for 'id'
 *   },
 * });
 */
export function useFindCurrentUserTodoByIdQuery(baseOptions: ApolloReactHooks.QueryHookOptions<FindCurrentUserTodoByIdQuery, FindCurrentUserTodoByIdQueryVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return ApolloReactHooks.useQuery<FindCurrentUserTodoByIdQuery, FindCurrentUserTodoByIdQueryVariables>(FindCurrentUserTodoByIdDocument, options);
      }
export function useFindCurrentUserTodoByIdLazyQuery(baseOptions?: ApolloReactHooks.LazyQueryHookOptions<FindCurrentUserTodoByIdQuery, FindCurrentUserTodoByIdQueryVariables>) {
          const options = {...defaultOptions, ...baseOptions}
          return ApolloReactHooks.useLazyQuery<FindCurrentUserTodoByIdQuery, FindCurrentUserTodoByIdQueryVariables>(FindCurrentUserTodoByIdDocument, options);
        }
export type FindCurrentUserTodoByIdQueryHookResult = ReturnType<typeof useFindCurrentUserTodoByIdQuery>;
export type FindCurrentUserTodoByIdLazyQueryHookResult = ReturnType<typeof useFindCurrentUserTodoByIdLazyQuery>;
export type FindCurrentUserTodoByIdQueryResult = Apollo.QueryResult<FindCurrentUserTodoByIdQuery, FindCurrentUserTodoByIdQueryVariables>;
export function refetchFindCurrentUserTodoByIdQuery(variables: FindCurrentUserTodoByIdQueryVariables) {
      return { query: FindCurrentUserTodoByIdDocument, variables: variables }
    }
export const GetCurrentUserTodosDocument = gql`
    query GetCurrentUserTodos($filters: TodoFiltersDto!) {
  getCurrentUserTodos(filters: $filters) {
    rows {
      id
      isCompleted
      name
      createdAt
    }
    count
  }
}
    `;

/**
 * __useGetCurrentUserTodosQuery__
 *
 * To run a query within a React component, call `useGetCurrentUserTodosQuery` and pass it any options that fit your needs.
 * When your component renders, `useGetCurrentUserTodosQuery` returns an object from Apollo Client that contains loading, error, and data properties
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the query, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = useGetCurrentUserTodosQuery({
 *   variables: {
 *      filters: // value for 'filters'
 *   },
 * });
 */
export function useGetCurrentUserTodosQuery(baseOptions: ApolloReactHooks.QueryHookOptions<GetCurrentUserTodosQuery, GetCurrentUserTodosQueryVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return ApolloReactHooks.useQuery<GetCurrentUserTodosQuery, GetCurrentUserTodosQueryVariables>(GetCurrentUserTodosDocument, options);
      }
export function useGetCurrentUserTodosLazyQuery(baseOptions?: ApolloReactHooks.LazyQueryHookOptions<GetCurrentUserTodosQuery, GetCurrentUserTodosQueryVariables>) {
          const options = {...defaultOptions, ...baseOptions}
          return ApolloReactHooks.useLazyQuery<GetCurrentUserTodosQuery, GetCurrentUserTodosQueryVariables>(GetCurrentUserTodosDocument, options);
        }
export type GetCurrentUserTodosQueryHookResult = ReturnType<typeof useGetCurrentUserTodosQuery>;
export type GetCurrentUserTodosLazyQueryHookResult = ReturnType<typeof useGetCurrentUserTodosLazyQuery>;
export type GetCurrentUserTodosQueryResult = Apollo.QueryResult<GetCurrentUserTodosQuery, GetCurrentUserTodosQueryVariables>;
export function refetchGetCurrentUserTodosQuery(variables: GetCurrentUserTodosQueryVariables) {
      return { query: GetCurrentUserTodosDocument, variables: variables }
    }
export const InvoicesListDocument = gql`
    query InvoicesList {
  invoicesList {
    id
    amount_due
    amount_paid
    amount_remaining
    attempt_count
    attempted
    currency
    next_payment_attempt
    period_start
    period_end
    created
    paid
    payment_intent
    status
    invoice_pdf
    hosted_invoice_url
    billing_reason
    collection_method
    lines {
      has_more
      url
      data {
        id
        amount
        currency
        description
        invoice_item
        plan {
          id
          active
          amount
          amount_decimal
          created
          currency
          interval
          interval_count
          product
          product_details {
            id
            active
            description
            images
            name
          }
        }
        proration
        quantity
        subscription
        subscription_item
        type
      }
    }
  }
}
    `;

/**
 * __useInvoicesListQuery__
 *
 * To run a query within a React component, call `useInvoicesListQuery` and pass it any options that fit your needs.
 * When your component renders, `useInvoicesListQuery` returns an object from Apollo Client that contains loading, error, and data properties
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the query, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = useInvoicesListQuery({
 *   variables: {
 *   },
 * });
 */
export function useInvoicesListQuery(baseOptions?: ApolloReactHooks.QueryHookOptions<InvoicesListQuery, InvoicesListQueryVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return ApolloReactHooks.useQuery<InvoicesListQuery, InvoicesListQueryVariables>(InvoicesListDocument, options);
      }
export function useInvoicesListLazyQuery(baseOptions?: ApolloReactHooks.LazyQueryHookOptions<InvoicesListQuery, InvoicesListQueryVariables>) {
          const options = {...defaultOptions, ...baseOptions}
          return ApolloReactHooks.useLazyQuery<InvoicesListQuery, InvoicesListQueryVariables>(InvoicesListDocument, options);
        }
export type InvoicesListQueryHookResult = ReturnType<typeof useInvoicesListQuery>;
export type InvoicesListLazyQueryHookResult = ReturnType<typeof useInvoicesListLazyQuery>;
export type InvoicesListQueryResult = Apollo.QueryResult<InvoicesListQuery, InvoicesListQueryVariables>;
export function refetchInvoicesListQuery(variables?: InvoicesListQueryVariables) {
      return { query: InvoicesListDocument, variables: variables }
    }
export const PaymentIntentsListDocument = gql`
    query PaymentIntentsList {
  paymentIntentsList {
    id
    client_secret
    description
    amount
    currency
    status
    invoice {
      id
      paid
      status
      invoice_pdf
      hosted_invoice_url
      lines {
        has_more
        url
        data {
          id
          amount
          currency
          description
          invoice_item
          plan {
            id
            active
            amount
            amount_decimal
            created
            currency
            interval
            interval_count
            product
            product_details {
              id
              active
              description
              images
              name
            }
          }
          proration
          quantity
          subscription
          subscription_item
          type
        }
      }
    }
  }
}
    `;

/**
 * __usePaymentIntentsListQuery__
 *
 * To run a query within a React component, call `usePaymentIntentsListQuery` and pass it any options that fit your needs.
 * When your component renders, `usePaymentIntentsListQuery` returns an object from Apollo Client that contains loading, error, and data properties
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the query, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = usePaymentIntentsListQuery({
 *   variables: {
 *   },
 * });
 */
export function usePaymentIntentsListQuery(baseOptions?: ApolloReactHooks.QueryHookOptions<PaymentIntentsListQuery, PaymentIntentsListQueryVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return ApolloReactHooks.useQuery<PaymentIntentsListQuery, PaymentIntentsListQueryVariables>(PaymentIntentsListDocument, options);
      }
export function usePaymentIntentsListLazyQuery(baseOptions?: ApolloReactHooks.LazyQueryHookOptions<PaymentIntentsListQuery, PaymentIntentsListQueryVariables>) {
          const options = {...defaultOptions, ...baseOptions}
          return ApolloReactHooks.useLazyQuery<PaymentIntentsListQuery, PaymentIntentsListQueryVariables>(PaymentIntentsListDocument, options);
        }
export type PaymentIntentsListQueryHookResult = ReturnType<typeof usePaymentIntentsListQuery>;
export type PaymentIntentsListLazyQueryHookResult = ReturnType<typeof usePaymentIntentsListLazyQuery>;
export type PaymentIntentsListQueryResult = Apollo.QueryResult<PaymentIntentsListQuery, PaymentIntentsListQueryVariables>;
export function refetchPaymentIntentsListQuery(variables?: PaymentIntentsListQueryVariables) {
      return { query: PaymentIntentsListDocument, variables: variables }
    }
export const PaymentMethodsListDocument = gql`
    query PaymentMethodsList {
  paymentMethodsList {
    id
    card {
      brand
      country
      exp_year
      exp_month
      fingerprint
      funding
      last4
    }
    billing_details {
      name
    }
  }
}
    `;

/**
 * __usePaymentMethodsListQuery__
 *
 * To run a query within a React component, call `usePaymentMethodsListQuery` and pass it any options that fit your needs.
 * When your component renders, `usePaymentMethodsListQuery` returns an object from Apollo Client that contains loading, error, and data properties
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the query, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = usePaymentMethodsListQuery({
 *   variables: {
 *   },
 * });
 */
export function usePaymentMethodsListQuery(baseOptions?: ApolloReactHooks.QueryHookOptions<PaymentMethodsListQuery, PaymentMethodsListQueryVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return ApolloReactHooks.useQuery<PaymentMethodsListQuery, PaymentMethodsListQueryVariables>(PaymentMethodsListDocument, options);
      }
export function usePaymentMethodsListLazyQuery(baseOptions?: ApolloReactHooks.LazyQueryHookOptions<PaymentMethodsListQuery, PaymentMethodsListQueryVariables>) {
          const options = {...defaultOptions, ...baseOptions}
          return ApolloReactHooks.useLazyQuery<PaymentMethodsListQuery, PaymentMethodsListQueryVariables>(PaymentMethodsListDocument, options);
        }
export type PaymentMethodsListQueryHookResult = ReturnType<typeof usePaymentMethodsListQuery>;
export type PaymentMethodsListLazyQueryHookResult = ReturnType<typeof usePaymentMethodsListLazyQuery>;
export type PaymentMethodsListQueryResult = Apollo.QueryResult<PaymentMethodsListQuery, PaymentMethodsListQueryVariables>;
export function refetchPaymentMethodsListQuery(variables?: PaymentMethodsListQueryVariables) {
      return { query: PaymentMethodsListDocument, variables: variables }
    }
export const ProductsListDocument = gql`
    query ProductsList {
  productsList {
    id
    active
    description
    name
    images
    prices {
      id
      active
      currency
      billing_scheme
      type
      unit_amount
      unit_amount_decimal
      recurring {
        aggregate_usage
        interval
        interval_count
        trial_period_days
        usage_type
      }
    }
  }
}
    `;

/**
 * __useProductsListQuery__
 *
 * To run a query within a React component, call `useProductsListQuery` and pass it any options that fit your needs.
 * When your component renders, `useProductsListQuery` returns an object from Apollo Client that contains loading, error, and data properties
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the query, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = useProductsListQuery({
 *   variables: {
 *   },
 * });
 */
export function useProductsListQuery(baseOptions?: ApolloReactHooks.QueryHookOptions<ProductsListQuery, ProductsListQueryVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return ApolloReactHooks.useQuery<ProductsListQuery, ProductsListQueryVariables>(ProductsListDocument, options);
      }
export function useProductsListLazyQuery(baseOptions?: ApolloReactHooks.LazyQueryHookOptions<ProductsListQuery, ProductsListQueryVariables>) {
          const options = {...defaultOptions, ...baseOptions}
          return ApolloReactHooks.useLazyQuery<ProductsListQuery, ProductsListQueryVariables>(ProductsListDocument, options);
        }
export type ProductsListQueryHookResult = ReturnType<typeof useProductsListQuery>;
export type ProductsListLazyQueryHookResult = ReturnType<typeof useProductsListLazyQuery>;
export type ProductsListQueryResult = Apollo.QueryResult<ProductsListQuery, ProductsListQueryVariables>;
export function refetchProductsListQuery(variables?: ProductsListQueryVariables) {
      return { query: ProductsListDocument, variables: variables }
    }
export const StripeCustomerDocument = gql`
    query StripeCustomer {
  stripeCustomer {
    id
    balance
    currency
    delinquent
    description
    email
    invoice_prefix
    name
    phone
    address {
      city
      country
      line1
      line2
      postal_code
      state
    }
  }
}
    `;

/**
 * __useStripeCustomerQuery__
 *
 * To run a query within a React component, call `useStripeCustomerQuery` and pass it any options that fit your needs.
 * When your component renders, `useStripeCustomerQuery` returns an object from Apollo Client that contains loading, error, and data properties
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the query, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = useStripeCustomerQuery({
 *   variables: {
 *   },
 * });
 */
export function useStripeCustomerQuery(baseOptions?: ApolloReactHooks.QueryHookOptions<StripeCustomerQuery, StripeCustomerQueryVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return ApolloReactHooks.useQuery<StripeCustomerQuery, StripeCustomerQueryVariables>(StripeCustomerDocument, options);
      }
export function useStripeCustomerLazyQuery(baseOptions?: ApolloReactHooks.LazyQueryHookOptions<StripeCustomerQuery, StripeCustomerQueryVariables>) {
          const options = {...defaultOptions, ...baseOptions}
          return ApolloReactHooks.useLazyQuery<StripeCustomerQuery, StripeCustomerQueryVariables>(StripeCustomerDocument, options);
        }
export type StripeCustomerQueryHookResult = ReturnType<typeof useStripeCustomerQuery>;
export type StripeCustomerLazyQueryHookResult = ReturnType<typeof useStripeCustomerLazyQuery>;
export type StripeCustomerQueryResult = Apollo.QueryResult<StripeCustomerQuery, StripeCustomerQueryVariables>;
export function refetchStripeCustomerQuery(variables?: StripeCustomerQueryVariables) {
      return { query: StripeCustomerDocument, variables: variables }
    }
export const SubsListDocument = gql`
    query SubsList {
  subsList {
    id
    cancel_at
    cancel_at_period_end
    canceled_at
    current_period_end
    current_period_start
    days_until_due
    ended_at
    start_date
    status
    latest_invoice
    plan {
      id
      active
      amount
      amount_decimal
      created
      currency
      interval
      interval_count
      product
      product_details {
        id
        active
        description
        images
        name
      }
    }
  }
}
    `;

/**
 * __useSubsListQuery__
 *
 * To run a query within a React component, call `useSubsListQuery` and pass it any options that fit your needs.
 * When your component renders, `useSubsListQuery` returns an object from Apollo Client that contains loading, error, and data properties
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the query, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = useSubsListQuery({
 *   variables: {
 *   },
 * });
 */
export function useSubsListQuery(baseOptions?: ApolloReactHooks.QueryHookOptions<SubsListQuery, SubsListQueryVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return ApolloReactHooks.useQuery<SubsListQuery, SubsListQueryVariables>(SubsListDocument, options);
      }
export function useSubsListLazyQuery(baseOptions?: ApolloReactHooks.LazyQueryHookOptions<SubsListQuery, SubsListQueryVariables>) {
          const options = {...defaultOptions, ...baseOptions}
          return ApolloReactHooks.useLazyQuery<SubsListQuery, SubsListQueryVariables>(SubsListDocument, options);
        }
export type SubsListQueryHookResult = ReturnType<typeof useSubsListQuery>;
export type SubsListLazyQueryHookResult = ReturnType<typeof useSubsListLazyQuery>;
export type SubsListQueryResult = Apollo.QueryResult<SubsListQuery, SubsListQueryVariables>;
export function refetchSubsListQuery(variables?: SubsListQueryVariables) {
      return { query: SubsListDocument, variables: variables }
    }
export const TheyLoveWarDocument = gql`
    query TheyLoveWar {
  theyLoveWar {
    id
    name
    comment
    country_code
    main_image
    social_links {
      id
      url
      type
    }
    images {
      id
      src
    }
  }
}
    `;

/**
 * __useTheyLoveWarQuery__
 *
 * To run a query within a React component, call `useTheyLoveWarQuery` and pass it any options that fit your needs.
 * When your component renders, `useTheyLoveWarQuery` returns an object from Apollo Client that contains loading, error, and data properties
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the query, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = useTheyLoveWarQuery({
 *   variables: {
 *   },
 * });
 */
export function useTheyLoveWarQuery(baseOptions?: ApolloReactHooks.QueryHookOptions<TheyLoveWarQuery, TheyLoveWarQueryVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return ApolloReactHooks.useQuery<TheyLoveWarQuery, TheyLoveWarQueryVariables>(TheyLoveWarDocument, options);
      }
export function useTheyLoveWarLazyQuery(baseOptions?: ApolloReactHooks.LazyQueryHookOptions<TheyLoveWarQuery, TheyLoveWarQueryVariables>) {
          const options = {...defaultOptions, ...baseOptions}
          return ApolloReactHooks.useLazyQuery<TheyLoveWarQuery, TheyLoveWarQueryVariables>(TheyLoveWarDocument, options);
        }
export type TheyLoveWarQueryHookResult = ReturnType<typeof useTheyLoveWarQuery>;
export type TheyLoveWarLazyQueryHookResult = ReturnType<typeof useTheyLoveWarLazyQuery>;
export type TheyLoveWarQueryResult = Apollo.QueryResult<TheyLoveWarQuery, TheyLoveWarQueryVariables>;
export function refetchTheyLoveWarQuery(variables?: TheyLoveWarQueryVariables) {
      return { query: TheyLoveWarDocument, variables: variables }
    }
export const OnCommentAddedDocument = gql`
    subscription OnCommentAdded {
  commentAdded
}
    `;

/**
 * __useOnCommentAddedSubscription__
 *
 * To run a query within a React component, call `useOnCommentAddedSubscription` and pass it any options that fit your needs.
 * When your component renders, `useOnCommentAddedSubscription` returns an object from Apollo Client that contains loading, error, and data properties
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the subscription, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = useOnCommentAddedSubscription({
 *   variables: {
 *   },
 * });
 */
export function useOnCommentAddedSubscription(baseOptions?: ApolloReactHooks.SubscriptionHookOptions<OnCommentAddedSubscription, OnCommentAddedSubscriptionVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return ApolloReactHooks.useSubscription<OnCommentAddedSubscription, OnCommentAddedSubscriptionVariables>(OnCommentAddedDocument, options);
      }
export type OnCommentAddedSubscriptionHookResult = ReturnType<typeof useOnCommentAddedSubscription>;
export type OnCommentAddedSubscriptionResult = Apollo.SubscriptionResult<OnCommentAddedSubscription>;

declare module '*/createCharge.ts' {
  import { DocumentNode } from 'graphql';
  const defaultDocument: DocumentNode;
  export const CreateCharge: DocumentNode;

  export default defaultDocument;
}
    

declare module '*/createSetupIntent.ts' {
  import { DocumentNode } from 'graphql';
  const defaultDocument: DocumentNode;
  export const CreateSetupIntent: DocumentNode;

  export default defaultDocument;
}
    

declare module '*/createTodo.ts' {
  import { DocumentNode } from 'graphql';
  const defaultDocument: DocumentNode;
  export const CreateTodo: DocumentNode;

  export default defaultDocument;
}
    

declare module '*/createUser.ts' {
  import { DocumentNode } from 'graphql';
  const defaultDocument: DocumentNode;
  export const CreateUser: DocumentNode;

  export default defaultDocument;
}
    

declare module '*/deleteCurrentUserAvatar.ts' {
  import { DocumentNode } from 'graphql';
  const defaultDocument: DocumentNode;
  export const DeleteCurrentUserAvatar: DocumentNode;

  export default defaultDocument;
}
    

declare module '*/deleteTodoById.ts' {
  import { DocumentNode } from 'graphql';
  const defaultDocument: DocumentNode;
  export const DeleteTodoById: DocumentNode;

  export default defaultDocument;
}
    

declare module '*/payInvoice.ts' {
  import { DocumentNode } from 'graphql';
  const defaultDocument: DocumentNode;
  export const PayInvoice: DocumentNode;

  export default defaultDocument;
}
    

declare module '*/paymentMethodDetach.ts' {
  import { DocumentNode } from 'graphql';
  const defaultDocument: DocumentNode;
  export const PaymentMethodDetach: DocumentNode;

  export default defaultDocument;
}
    

declare module '*/stripeCustomerUpdate.ts' {
  import { DocumentNode } from 'graphql';
  const defaultDocument: DocumentNode;
  export const StripeCustomerUpdate: DocumentNode;

  export default defaultDocument;
}
    

declare module '*/subDel.ts' {
  import { DocumentNode } from 'graphql';
  const defaultDocument: DocumentNode;
  export const SubDel: DocumentNode;

  export default defaultDocument;
}
    

declare module '*/subscriptionCreate.ts' {
  import { DocumentNode } from 'graphql';
  const defaultDocument: DocumentNode;
  export const SubscriptionCreate: DocumentNode;

  export default defaultDocument;
}
    

declare module '*/updateCurrentUser.ts' {
  import { DocumentNode } from 'graphql';
  const defaultDocument: DocumentNode;
  export const UpdateCurrentUser: DocumentNode;

  export default defaultDocument;
}
    

declare module '*/updateTodoById.ts' {
  import { DocumentNode } from 'graphql';
  const defaultDocument: DocumentNode;
  export const UpdateTodoById: DocumentNode;

  export default defaultDocument;
}
    

declare module '*/uploadFile.ts' {
  import { DocumentNode } from 'graphql';
  const defaultDocument: DocumentNode;
  export const UploadFile: DocumentNode;

  export default defaultDocument;
}
    

declare module '*/currentUser.ts' {
  import { DocumentNode } from 'graphql';
  const defaultDocument: DocumentNode;
  export const CurrentUser: DocumentNode;

  export default defaultDocument;
}
    

declare module '*/findCurrentUserTodoById.ts' {
  import { DocumentNode } from 'graphql';
  const defaultDocument: DocumentNode;
  export const FindCurrentUserTodoById: DocumentNode;

  export default defaultDocument;
}
    

declare module '*/getCurrentUserTodos.ts' {
  import { DocumentNode } from 'graphql';
  const defaultDocument: DocumentNode;
  export const GetCurrentUserTodos: DocumentNode;

  export default defaultDocument;
}
    

declare module '*/invoicesList.ts' {
  import { DocumentNode } from 'graphql';
  const defaultDocument: DocumentNode;
  export const InvoicesList: DocumentNode;

  export default defaultDocument;
}
    

declare module '*/paymentIntentsList.ts' {
  import { DocumentNode } from 'graphql';
  const defaultDocument: DocumentNode;
  export const PaymentIntentsList: DocumentNode;

  export default defaultDocument;
}
    

declare module '*/paymentMethodsList.ts' {
  import { DocumentNode } from 'graphql';
  const defaultDocument: DocumentNode;
  export const PaymentMethodsList: DocumentNode;

  export default defaultDocument;
}
    

declare module '*/productsList.ts' {
  import { DocumentNode } from 'graphql';
  const defaultDocument: DocumentNode;
  export const ProductsList: DocumentNode;

  export default defaultDocument;
}
    

declare module '*/stripeCustomer.ts' {
  import { DocumentNode } from 'graphql';
  const defaultDocument: DocumentNode;
  export const StripeCustomer: DocumentNode;

  export default defaultDocument;
}
    

declare module '*/subsList.ts' {
  import { DocumentNode } from 'graphql';
  const defaultDocument: DocumentNode;
  export const SubsList: DocumentNode;

  export default defaultDocument;
}
    

declare module '*/theyLoveWar.ts' {
  import { DocumentNode } from 'graphql';
  const defaultDocument: DocumentNode;
  export const TheyLoveWar: DocumentNode;

  export default defaultDocument;
}
    

declare module '*/commentAddedSubscription.ts' {
  import { DocumentNode } from 'graphql';
  const defaultDocument: DocumentNode;
  export const OnCommentAdded: DocumentNode;

  export default defaultDocument;
}
    

export const CreateCharge = gql`
    mutation CreateCharge($input: CreateChargeInput!) {
  createCharge(input: $input) {
    id
    status
    client_secret
    redirect_to_url
  }
}
    `;
export const CreateSetupIntent = gql`
    mutation CreateSetupIntent {
  createSetupIntent {
    id
    client_secret
    status
  }
}
    `;
export const CreateTodo = gql`
    mutation CreateTodo($input: CreateTodoInput!) {
  createTodo(input: $input) {
    id
    isCompleted
    name
    createdAt
  }
}
    `;
export const CreateUser = gql`
    mutation CreateUser($input: CreateUserInput!) {
  createUser(input: $input) {
    id
    email
    firstName
    lastName
    createdAt
  }
}
    `;
export const DeleteCurrentUserAvatar = gql`
    mutation DeleteCurrentUserAvatar {
  deleteCurrentUserAvatar {
    id
    email
    firstName
    lastName
    avatar {
      id
      url
    }
  }
}
    `;
export const DeleteTodoById = gql`
    mutation DeleteTodoById($id: String!) {
  deleteTodoById(id: $id) {
    id
    isCompleted
    name
    createdAt
  }
}
    `;
export const PayInvoice = gql`
    mutation PayInvoice($input: PayInvoiceInput!) {
  payInvoice(input: $input) {
    paymentIntent {
      id
      client_secret
      status
    }
    invoice {
      id
      paid
      payment_intent
      status
    }
  }
}
    `;
export const PaymentMethodDetach = gql`
    mutation PaymentMethodDetach($input: PaymentMethodDetachInput!) {
  paymentMethodDetach(input: $input) {
    id
    card {
      brand
      country
      exp_year
      exp_month
      fingerprint
      funding
      last4
    }
    billing_details {
      name
    }
  }
}
    `;
export const StripeCustomerUpdate = gql`
    mutation StripeCustomerUpdate($input: CustomersUpdateInput!) {
  stripeCustomerUpdate(input: $input) {
    id
    address {
      city
      country
      line1
      line2
      postal_code
      state
    }
    balance
    created
    currency
    delinquent
    description
    email
    invoice_prefix
    name
    next_invoice_sequence
    phone
  }
}
    `;
export const SubDel = gql`
    mutation SubDel($input: SubDelInput!) {
  subDel(input: $input) {
    id
  }
}
    `;
export const SubscriptionCreate = gql`
    mutation SubscriptionCreate($input: SubscriptionCreateInput!) {
  subscriptionsCreate(input: $input) {
    subscription {
      id
      cancel_at
      cancel_at_period_end
      canceled_at
      current_period_end
      current_period_start
      days_until_due
      ended_at
      start_date
      status
      plan {
        currency
        amount
      }
    }
    paymentIntent {
      id
      client_secret
      status
    }
  }
}
    `;
export const UpdateCurrentUser = gql`
    mutation UpdateCurrentUser($data: UpdateUserByIdInput!) {
  updateCurrentUser(data: $data) {
    id
    email
    firstName
    lastName
  }
}
    `;
export const UpdateTodoById = gql`
    mutation UpdateTodoById($id: String!, $data: UpdateTodoByIdInput!) {
  updateTodoById(id: $id, data: $data) {
    id
    isCompleted
    name
    createdAt
  }
}
    `;
export const UploadFile = gql`
    mutation UploadFile($file: Upload!) {
  uploadFile(file: $file)
}
    `;
export const CurrentUser = gql`
    query CurrentUser {
  currentUser {
    id
    email
    firstName
    lastName
    avatar {
      id
      url
    }
  }
}
    `;
export const FindCurrentUserTodoById = gql`
    query FindCurrentUserTodoById($id: String!) {
  findCurrentUserTodoById(id: $id) {
    id
    isCompleted
    name
    createdAt
  }
}
    `;
export const GetCurrentUserTodos = gql`
    query GetCurrentUserTodos($filters: TodoFiltersDto!) {
  getCurrentUserTodos(filters: $filters) {
    rows {
      id
      isCompleted
      name
      createdAt
    }
    count
  }
}
    `;
export const InvoicesList = gql`
    query InvoicesList {
  invoicesList {
    id
    amount_due
    amount_paid
    amount_remaining
    attempt_count
    attempted
    currency
    next_payment_attempt
    period_start
    period_end
    created
    paid
    payment_intent
    status
    invoice_pdf
    hosted_invoice_url
    billing_reason
    collection_method
    lines {
      has_more
      url
      data {
        id
        amount
        currency
        description
        invoice_item
        plan {
          id
          active
          amount
          amount_decimal
          created
          currency
          interval
          interval_count
          product
          product_details {
            id
            active
            description
            images
            name
          }
        }
        proration
        quantity
        subscription
        subscription_item
        type
      }
    }
  }
}
    `;
export const PaymentIntentsList = gql`
    query PaymentIntentsList {
  paymentIntentsList {
    id
    client_secret
    description
    amount
    currency
    status
    invoice {
      id
      paid
      status
      invoice_pdf
      hosted_invoice_url
      lines {
        has_more
        url
        data {
          id
          amount
          currency
          description
          invoice_item
          plan {
            id
            active
            amount
            amount_decimal
            created
            currency
            interval
            interval_count
            product
            product_details {
              id
              active
              description
              images
              name
            }
          }
          proration
          quantity
          subscription
          subscription_item
          type
        }
      }
    }
  }
}
    `;
export const PaymentMethodsList = gql`
    query PaymentMethodsList {
  paymentMethodsList {
    id
    card {
      brand
      country
      exp_year
      exp_month
      fingerprint
      funding
      last4
    }
    billing_details {
      name
    }
  }
}
    `;
export const ProductsList = gql`
    query ProductsList {
  productsList {
    id
    active
    description
    name
    images
    prices {
      id
      active
      currency
      billing_scheme
      type
      unit_amount
      unit_amount_decimal
      recurring {
        aggregate_usage
        interval
        interval_count
        trial_period_days
        usage_type
      }
    }
  }
}
    `;
export const StripeCustomer = gql`
    query StripeCustomer {
  stripeCustomer {
    id
    balance
    currency
    delinquent
    description
    email
    invoice_prefix
    name
    phone
    address {
      city
      country
      line1
      line2
      postal_code
      state
    }
  }
}
    `;
export const SubsList = gql`
    query SubsList {
  subsList {
    id
    cancel_at
    cancel_at_period_end
    canceled_at
    current_period_end
    current_period_start
    days_until_due
    ended_at
    start_date
    status
    latest_invoice
    plan {
      id
      active
      amount
      amount_decimal
      created
      currency
      interval
      interval_count
      product
      product_details {
        id
        active
        description
        images
        name
      }
    }
  }
}
    `;
export const TheyLoveWar = gql`
    query TheyLoveWar {
  theyLoveWar {
    id
    name
    comment
    country_code
    main_image
    social_links {
      id
      url
      type
    }
    images {
      id
      src
    }
  }
}
    `;
export const OnCommentAdded = gql`
    subscription OnCommentAdded {
  commentAdded
}
    `;

      export interface PossibleTypesResultData {
        possibleTypes: {
          [key: string]: string[]
        }
      }
      const result: PossibleTypesResultData = {
  "possibleTypes": {}
};
      export default result;
    