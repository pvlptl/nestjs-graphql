import { gql } from '@apollo/client'

const COMMENT_ADDED_SUBSCRIPTION = gql`
  subscription OnCommentAdded {
    commentAdded
  }
`

export default COMMENT_ADDED_SUBSCRIPTION
