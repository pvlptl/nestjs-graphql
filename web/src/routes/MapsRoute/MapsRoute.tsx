import React from 'react'
import GoogleMapReact from 'google-map-react'

const defaultProps = {
  center: {
    lat: 48.9226,
    lng: 24.7111
  },
  zoom: 11
}

const Marker = ({ text, lat, lng }: any) => {
  console.log(lat)
  console.log(lng)
  return (
    <div
      style={{
        width: 'fit-content',
        padding: 8,
        backgroundColor: 'red',
        borderRadius: 24,
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'center'
      }}
    >
      <b style={{ color: 'white' }}>{text}</b>
    </div>
  )
}

function MapsRoute() {
  return (
    <div style={{ height: '80vh', width: '95vw' }}>
      <GoogleMapReact
        bootstrapURLKeys={{
          key: process.env.REACT_APP_GOOGLE_MAPS_KEY as string
        }}
        defaultCenter={defaultProps.center}
        defaultZoom={defaultProps.zoom}
      >
        <Marker lat={49.8397} lng={24.0297} text="" />
        <Marker lat={48.0159} lng={37.8028} text="" />
      </GoogleMapReact>
    </div>
  )
}

export default MapsRoute
