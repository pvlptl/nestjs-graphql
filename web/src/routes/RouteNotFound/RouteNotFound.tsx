import React from 'react'
import { Link } from 'react-router-dom'

function RouteNotFound() {
  return (
    <div>
      <h2>Route Not Found!</h2>
      <p>
        <Link to="/">Go to the home page</Link>
      </p>
    </div>
  )
}

export default RouteNotFound
