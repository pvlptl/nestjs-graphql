import React from 'react'
import { useParams } from 'react-router-dom'
import TodoContainer from '../../containers/TodoContainer'

function TodoRoute() {
  const { id } = useParams()

  if (!id) {
    return <div>No todo id!</div>
  }

  return <TodoContainer id={id} />
}

export default TodoRoute
