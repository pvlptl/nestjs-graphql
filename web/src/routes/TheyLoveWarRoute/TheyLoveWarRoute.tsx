import React from 'react'
import { useQuery } from '@apollo/client'
import theyLoveWarQuery from '../../graphql/query/theyLoveWar'
import { TheyLoveWarNodeDto } from '../../graphql/generated'
import getUnicodeFlagIcon from 'country-flag-icons/unicode'
import { hasFlag } from 'country-flag-icons'

function TheyLoveWarRoute() {
  const { data } = useQuery(theyLoveWarQuery)

  const nodes: TheyLoveWarNodeDto[] = data?.theyLoveWar || []

  return (
    <div
      style={{
        maxWidth: '90vw',
        display: 'flex',
        flexWrap: 'wrap',
        gap: 24
      }}
    >
      {nodes.map(i => (
        <div
          style={{
            width: 300,
            display: 'flex',
            flexDirection: 'column',
            gap: 8,
            borderRadius: 8,
            padding: 8,
            border: '1px solid lightblue'
          }}
          key={i.id}
        >
          <img
            style={{ width: '100%', borderRadius: 8 }}
            src={i.images[0]?.src}
            alt={i.name}
          />
          <b>
            {i.name}{' '}
            {i.country_code &&
              i.country_code.length > 0 &&
              hasFlag(i.country_code.toUpperCase()) &&
              getUnicodeFlagIcon(i.country_code.toUpperCase())}
          </b>
          <b>{i.comment}</b>
          <ul>
            {i.social_links.map(sc => (
              <li key={sc.id}>
                <a target="_blank" href={sc.url}>
                  {sc.type}
                </a>
              </li>
            ))}
          </ul>
        </div>
      ))}
    </div>
  )
}

export default TheyLoveWarRoute
