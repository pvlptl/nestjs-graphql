import React from 'react'
import PlansRouteConnected from '../../containers/PlansRouteConnected'
import StripeElementsContainer from '../../containers/StripeElementsContainer'

function PlansRoute() {
  return (
    <StripeElementsContainer>
      <PlansRouteConnected />
    </StripeElementsContainer>
  )
}

export default PlansRoute
