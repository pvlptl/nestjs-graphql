import React from 'react'
import SubsRouteConnected from '../../containers/SubsRouteConnected'
import StripeElementsContainer from '../../containers/StripeElementsContainer'

function SubsRoute() {
  return (
    <StripeElementsContainer>
      <SubsRouteConnected />
    </StripeElementsContainer>
  )
}

export default SubsRoute
