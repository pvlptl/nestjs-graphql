import React from 'react'
import { useQuery } from '@apollo/client'
import invoicesListQuery from '../../graphql/query/invoicesList'
import { InvoiceDto } from '../../graphql/generated'
import parseStripeUnitAmount from '../../utils/parseStripeUnitAmount'
import moment from 'moment'

function InvoicesRoute() {
  const ilq = useQuery(invoicesListQuery)

  const nodes = ilq?.data?.invoicesList || []

  console.log(nodes)

  return (
    <div
      style={{
        maxWidth: 500,
        display: 'flex',
        flexDirection: 'column',
        gap: 8
      }}
    >
      {nodes.map((i: InvoiceDto) => (
        <div
          style={{
            border: '1px solid lightblue',
            padding: 8,
            borderRadius: 8,
            display: 'flex',
            flexDirection: 'column',
            gap: 8
          }}
          key={i.id}
        >
          <div>
            {i.lines.data.map(x => (
              <span key={x.id}>
                <b>description</b> {x.description}
              </span>
            ))}
          </div>
          <span>
            <b>status</b>: {i.status}
          </span>
          <span>
            <b>amount_due</b>: {parseStripeUnitAmount(i.amount_due)}
          </span>
          <span>
            <b>amount_paid</b>: {parseStripeUnitAmount(i.amount_paid)}
          </span>
          <span>
            <b>amount_remaining</b>: {parseStripeUnitAmount(i.amount_remaining)}
          </span>
          <span>
            <b>attempt_count</b> {i.attempt_count}
          </span>
          <span>
            <b>attempted</b> {i.attempted ? 'Yes' : 'No'}
          </span>
          <span>
            <b>billing_reason</b> {i.billing_reason}
          </span>
          <span>
            <b>collection_method</b> {i.collection_method}
          </span>
          <span>
            <b>currency</b> {i.currency}
          </span>
          <span>
            <b>next_payment_attempt</b> {i.next_payment_attempt}
          </span>
          <span>
            <b>paid</b> {i.paid ? 'Yes' : 'No'}
          </span>
          <span>
            <b>payment_intent</b> {i.payment_intent}
          </span>
          <span>
            <b>created</b>{' '}
            {moment(i.created * 1000).format('DD MMM YYYY HH:mm')}
          </span>
          <span>
            <b>period_start</b>{' '}
            {moment(i.period_start * 1000).format('DD MMM YYYY HH:mm')}
          </span>
          <span>
            <b>period_end</b>{' '}
            {moment(i.period_end * 1000).format('DD MMM YYYY HH:mm')}
          </span>
          <a target="_blank" href={i.hosted_invoice_url as string}>
            hosted_invoice_url
          </a>
          <a target="_blank" href={i.invoice_pdf as string}>
            invoice_pdf
          </a>
        </div>
      ))}
    </div>
  )
}

export default InvoicesRoute
