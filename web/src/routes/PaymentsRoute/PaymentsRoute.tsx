import React from 'react'
import { useQuery } from '@apollo/client'
import paymentIntentsListQuery from '../../graphql/query/paymentIntentsList'
import { PaymentIntentDto } from '../../graphql/generated'
import parseStripeUnitAmount from '../../utils/parseStripeUnitAmount'

// todo each payment have client_secret - add pay again logic when status not paid

function PaymentsRoute() {
  const pilq = useQuery(paymentIntentsListQuery)
  const nodes = pilq?.data?.paymentIntentsList || ([] as any)
  return (
    <div
      style={{
        display: 'flex',
        flexDirection: 'column',
        gap: 8,
        maxWidth: 800
      }}
    >
      {nodes.map((i: PaymentIntentDto) => (
        <div
          style={{
            border: '1px solid lightblue',
            padding: 8,
            borderRadius: 8,
            display: 'flex',
            flexDirection: 'column',
            gap: 8
          }}
          key={i.id}
        >
          <span>{i.description}</span>
          <span>
            {parseStripeUnitAmount(i.amount)} {i.currency}
          </span>
          <span>{i.status}</span>

          <b>Invoice:</b>
          <ul>
            <li>
              {i.invoice?.lines?.data?.map(x => (
                <div key={x.id}>description: {x.description}</div>
              ))}
            </li>
            <li>Paid: {i.invoice?.paid ? 'Yes' : 'No'}</li>
            <li>Status: {i.invoice?.status}</li>
            <li>
              <a target="_blank" href={i.invoice?.hosted_invoice_url as string}>
                hosted_invoice_url
              </a>
            </li>
            <li>
              <a target="_blank" href={i.invoice?.invoice_pdf as string}>
                invoice_pdf
              </a>
            </li>
          </ul>
        </div>
      ))}
    </div>
  )
}

export default PaymentsRoute
