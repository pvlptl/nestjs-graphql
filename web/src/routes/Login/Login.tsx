import React from 'react'
// import { Props } from './types'
import TextField from '@mui/material/TextField'
import Button from '@mui/material/Button'
import Grid from '@mui/material/Grid'
import Paper from '@mui/material/Paper'
import { Theme } from '@mui/material'
import { useSnackbar } from 'notistack'
import { useNavigate } from 'react-router-dom'
import { useFormik } from 'formik'
import { useSetRecoilState } from 'recoil'
import authTokenAtom from '../../recoil/atoms/authTokenAtom'
import login from '../../rest/login'

function Login() {
  const { enqueueSnackbar } = useSnackbar()
  const navigate = useNavigate()
  const setAuthToken = useSetRecoilState(authTokenAtom)

  const formik = useFormik({
    initialValues: {
      email: '',
      password: ''
    },
    onSubmit: async values => {
      try {
        const response = await login(values)

        if (response) {
          const { secret } = response
          localStorage.setItem('auth_token', secret)
          setAuthToken(secret)

          enqueueSnackbar('Success', { variant: 'success' })
          navigate('/')
        }
      } catch (error: any) {
        enqueueSnackbar(error.message, { variant: 'error' })
      }
    }
  })

  return (
    <Paper
      sx={{
        maxWidth: 450
      }}
    >
      <Grid
        container
        direction="column"
        gap={(theme: Theme) => theme.spacing(3)}
        sx={{
          p: (theme: Theme) => theme.spacing(3)
        }}
      >
        <TextField
          fullWidth
          label="Email"
          variant="outlined"
          placeholder="Enter Email"
          name="email"
          type="email"
          value={formik.values.email}
          onChange={formik.handleChange}
        />

        <TextField
          fullWidth
          label="Password"
          variant="outlined"
          placeholder="Enter Password"
          name="password"
          type="password"
          value={formik.values.password}
          onChange={formik.handleChange}
        />

        <Button
          disabled={formik.isSubmitting}
          onClick={formik.submitForm}
          variant="contained"
        >
          Login
        </Button>
      </Grid>
    </Paper>
  )
}

export default Login
