import React from 'react'
import SettingsConnected from '../../containers/SettingsConnected'

function SettingsRoute() {
  return <SettingsConnected />
}

export default SettingsRoute
