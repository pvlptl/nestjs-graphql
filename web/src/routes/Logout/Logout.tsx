import React, { useEffect } from 'react'
import { Navigate } from 'react-router-dom'
import useLogout from '../../hooks/useLogout'

function Logout() {
  const logout = useLogout()

  useEffect(() => {
    logout()
  }, [])

  return <Navigate to="/" />
}

export default Logout
