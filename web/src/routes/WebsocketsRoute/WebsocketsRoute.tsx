import { useSubscription } from '@apollo/client'
import React from 'react'
import COMMENT_ADDED_SUBSCRIPTION from '../../graphql/subscription/commentAddedSubscription'

function WebsocketsRoute() {
  const { data } = useSubscription(COMMENT_ADDED_SUBSCRIPTION)

  console.log('COMMENT_ADDED_SUBSCRIPTION', data?.commentAdded)

  return <div>commentAdded: {data?.commentAdded}</div>
}

export default WebsocketsRoute
