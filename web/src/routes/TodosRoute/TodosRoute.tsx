import React from 'react'
import TodosContainer from '../../containers/TodosContainer'

function TodosRoute() {
  return <TodosContainer />
}

export default TodosRoute
