import React from 'react'
import StripeElementsContainer from '../../containers/StripeElementsContainer'
import CardRouteConnected from '../../containers/CardRouteConnected'

function CardRoute() {
  return (
    <StripeElementsContainer>
      <CardRouteConnected />
    </StripeElementsContainer>
  )
}

export default CardRoute
