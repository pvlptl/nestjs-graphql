import React from 'react'
import TextField from '@mui/material/TextField'
import Button from '@mui/material/Button'
import Grid from '@mui/material/Grid'
import Paper from '@mui/material/Paper'
import { Theme } from '@mui/material'
import { useFormik } from 'formik'
import createUserMutation from '../../graphql/mutation/createUser'
import { useSnackbar } from 'notistack'
import { useNavigate } from 'react-router-dom'
import { useMutation } from '@apollo/client'

function Signup() {
  const { enqueueSnackbar } = useSnackbar()
  const [mutateFunction] = useMutation(createUserMutation)
  const navigate = useNavigate()

  const formik = useFormik({
    initialValues: {
      firstName: '',
      lastName: '',
      email: '',
      password: ''
    },
    onSubmit: async values => {
      try {
        const response = await mutateFunction({
          variables: {
            input: values
          }
        })

        if (response.data) {
          enqueueSnackbar('Success', { variant: 'success' })
          navigate('/login')
        }
      } catch (error: any) {
        enqueueSnackbar(error.message, { variant: 'error' })
      }
    }
  })

  return (
    <Paper
      sx={{
        maxWidth: 450
      }}
    >
      <Grid
        container
        direction="column"
        gap={(theme: Theme) => theme.spacing(3)}
        sx={{
          p: (theme: Theme) => theme.spacing(3)
        }}
      >
        <TextField
          fullWidth
          label="First Name"
          variant="outlined"
          placeholder="Enter First Name"
          name="firstName"
          value={formik.values.firstName}
          onChange={formik.handleChange}
        />

        <TextField
          fullWidth
          label="Last Name"
          variant="outlined"
          placeholder="Enter Last Name"
          name="lastName"
          value={formik.values.lastName}
          onChange={formik.handleChange}
        />

        <TextField
          fullWidth
          label="Email"
          variant="outlined"
          placeholder="Enter Email"
          name="email"
          type="email"
          value={formik.values.email}
          onChange={formik.handleChange}
        />

        <TextField
          fullWidth
          label="Password"
          variant="outlined"
          placeholder="Enter Password"
          name="password"
          type="password"
          value={formik.values.password}
          onChange={formik.handleChange}
        />

        <Button
          disabled={formik.isSubmitting}
          onClick={formik.submitForm}
          variant="contained"
        >
          Signup
        </Button>
      </Grid>
    </Paper>
  )
}

export default Signup
