import axios from 'axios'
import getAxiosError from '../utils/getAxiosError'
import { UserAvatar } from '../graphql/generated'

const uploadCurrentUserAvatar = async (data: FormData): Promise<UserAvatar> => {
  try {
    const response = await axios.post(
      `${process.env.REACT_APP_REST_API_URL}/user/avatar`,
      data
    )
    return response.data as any
  } catch (error) {
    throw getAxiosError(error)
  }
}

export default uploadCurrentUserAvatar
