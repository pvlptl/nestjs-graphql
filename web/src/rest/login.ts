import axios from 'axios'
import getAxiosError from '../utils/getAxiosError'

const login = async (data: any): Promise<any> => {
  try {
    const response = await axios.post(
      `${process.env.REACT_APP_REST_API_URL}/auth/login`,
      data
    )
    return response.data as any
  } catch (error) {
    throw getAxiosError(error)
  }
}

export default login
