import { atom } from 'recoil'
import recoilKeys from '../keys'
import { GetCurrentUserTodosQueryVariables } from '../../graphql/generated'

const DEFAULT_VARIABLES: GetCurrentUserTodosQueryVariables = {
  filters: {
    limit: 25,
    offset: 0
  }
}

const getTodosVariablesAtom = atom({
  key: recoilKeys.GET_TODOS_VARIABLES,
  default: DEFAULT_VARIABLES
})

export default getTodosVariablesAtom
