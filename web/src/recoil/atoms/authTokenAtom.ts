import { atom } from 'recoil'
import recoilKeys from '../keys'

const authTokenAtom = atom({
  key: recoilKeys.AUTH_TOKEN,
  default: (localStorage.getItem('auth_token') || null) as null | string
})

export default authTokenAtom
