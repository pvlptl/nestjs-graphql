import { selector } from 'recoil'
import recoilKeys from '../keys'
import authTokenAtom from '../atoms/authTokenAtom'

const authTokenSelector = selector({
  key: recoilKeys.AUTH_TOKEN_SELECTOR,
  get: ({ get }) => {
    const authToken = get(authTokenAtom)
    return {
      authToken
    }
  }
})

export default authTokenSelector
