import { selector } from 'recoil'
import recoilKeys from '../keys'
import getTodosVariablesAtom from '../atoms/getTodosVariablesAtom'

const getTodosVariablesSelector = selector({
  key: recoilKeys.GET_TODOS_VARIABLES_SELECTOR,
  get: ({ get }) => {
    const data = get(getTodosVariablesAtom)
    return {
      data
    }
  }
})

export default getTodosVariablesSelector
