import React, { useState } from 'react'
import { useMutation, useQuery } from '@apollo/client'
import productsListQuery from '../../graphql/query/productsList'
import PlansRouteUi from '../../components/PlansRouteUi'
import subscriptionCreateMutation from '../../graphql/mutation/subscriptionCreate'
import { useSnackbar } from 'notistack'
import paymentMethodsListQuery from '../../graphql/query/paymentMethodsList'
import { useStripe } from '@stripe/react-stripe-js'
import { useNavigate } from 'react-router-dom'

function PlansRouteConnected() {
  const navigate = useNavigate()
  const stripe = useStripe()
  const { enqueueSnackbar } = useSnackbar()
  const plq = useQuery(productsListQuery)
  const nodes = plq?.data?.productsList || []
  const [selectedPlan, setSelectedPlan] = useState<null | string>(null)

  const pmlq = useQuery(paymentMethodsListQuery)
  const paymentMethodsList = pmlq?.data?.paymentMethodsList || []
  const hasPaymentMethods = paymentMethodsList.length > 0

  const [subscriptionCreateMutate, { loading: subscriptionCreateLoading }] =
    useMutation(subscriptionCreateMutation)

  const handleSubmit = async (planId: string, priceId: string) => {
    try {
      if (!stripe) return null

      setSelectedPlan(planId)
      const res = await subscriptionCreateMutate({
        variables: {
          input: {
            priceId
          }
        }
      })

      const secret =
        res?.data?.subscriptionsCreate?.paymentIntent?.client_secret

      const status = res?.data?.subscriptionsCreate?.paymentIntent?.status

      if (status === 'requires_action' && secret) {
        const confirmResponse = await stripe.confirmCardPayment(secret, {
          payment_method: paymentMethodsList[0].id
        })

        if (confirmResponse.error) {
          navigate('/subscriptions')
          return enqueueSnackbar(confirmResponse.error.message, {
            variant: 'error'
          })
        }
      }

      enqueueSnackbar('Success', { variant: 'success' })

      navigate('/subscriptions')
    } catch (error: any) {
      enqueueSnackbar(error.message, { variant: 'error' })
    } finally {
      setSelectedPlan(null)
    }
  }

  return (
    <PlansRouteUi
      nodes={nodes}
      selectedPlan={selectedPlan}
      submitLoading={subscriptionCreateLoading}
      onSubmit={handleSubmit}
      hasPaymentMethods={hasPaymentMethods}
    />
  )
}

export default PlansRouteConnected
