import React from 'react'
import { ApolloProvider } from '@apollo/client'
import App from '../../App'
import useGraphqlClient from '../../hooks/useGraphqlClient'

function AppContainer() {
  const client = useGraphqlClient()
  return (
    <ApolloProvider client={client}>
      <App />
    </ApolloProvider>
  )
}

export default AppContainer
