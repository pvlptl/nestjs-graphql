import React from 'react'
import { useMutation, useQuery } from '@apollo/client'
import subsListQuery from '../../graphql/query/subsList'
import SubsRouteUi from '../../components/SubsRouteUi'
import subDelMutation from '../../graphql/mutation/subDel'
import { useSnackbar } from 'notistack'
import payInvoiceMutation from '../../graphql/mutation/payInvoice'
import { useStripe } from '@stripe/react-stripe-js'
import paymentMethodsListQuery from '../../graphql/query/paymentMethodsList'

function SubsRouteConnected() {
  const stripe = useStripe()
  const { enqueueSnackbar } = useSnackbar()
  const slq = useQuery(subsListQuery, { fetchPolicy: 'cache-and-network' })
  const subs = slq?.data?.subsList || []

  const [subDelMutate] = useMutation(subDelMutation)
  const [payInvoiceMutate] = useMutation(payInvoiceMutation)

  const pmlq = useQuery(paymentMethodsListQuery)
  const paymentMethodsList = pmlq?.data?.paymentMethodsList || []

  const handleDelete = async ({ id }: any) => {
    try {
      await subDelMutate({
        variables: {
          input: {
            subId: id
          }
        }
      })

      await slq.refetch()

      enqueueSnackbar('Success', { variant: 'success' })
    } catch (e: any) {
      enqueueSnackbar(e.message, { variant: 'error' })
    }
  }

  const handleRetryPay = async ({ latest_invoice }: any) => {
    try {
      if (!stripe) return null

      const res = await payInvoiceMutate({
        variables: {
          input: {
            invoiceId: latest_invoice
          }
        }
      })

      const secret = res.data.payInvoice.paymentIntent.client_secret

      const confirmResponse = await stripe.confirmCardPayment(secret, {
        payment_method: paymentMethodsList[0].id
      })

      if (confirmResponse.error) {
        return enqueueSnackbar(confirmResponse.error.message, {
          variant: 'error'
        })
      }

      await slq.refetch()

      enqueueSnackbar('Success', { variant: 'success' })
    } catch (e: any) {
      enqueueSnackbar(e.message, { variant: 'error' })
    }
  }

  return (
    <SubsRouteUi
      nodes={subs}
      onDelete={handleDelete}
      onRetryPay={handleRetryPay}
    />
  )
}

export default SubsRouteConnected
