import React from 'react'
import Todos from '../../components/Todos'
import useFetchTodos from '../../hooks/useFetchTodos'
import useCreateTodo from '../../hooks/useCreateTodo'
import useDeleteTodo from '../../hooks/useDeleteTodo'
import useToggleTodo from '../../hooks/useToggleTodo'
import useDeleteTodoUpdateTodosCache from '../../hooks/useDeleteTodoUpdateCache'
import { useRecoilValue, useSetRecoilState } from 'recoil'
import getTodosVariablesSelector from '../../recoil/selectors/getTodosVariablesSelector'
import getTodosVariablesAtom from '../../recoil/atoms/getTodosVariablesAtom'

function TodosContainer() {
  const setVariables = useSetRecoilState(getTodosVariablesAtom)

  const { data: GET_TODOS_VARIABLES } = useRecoilValue(
    getTodosVariablesSelector
  )

  const fetchTodosHook = useFetchTodos({
    variables: GET_TODOS_VARIABLES
  })

  const createTodoHook = useCreateTodo({
    getTodosVariables: GET_TODOS_VARIABLES,
    onRefetch: fetchTodosHook.onRefetch
  })

  const deleteTodoUpdateTodosCache = useDeleteTodoUpdateTodosCache({
    variables: GET_TODOS_VARIABLES
  })

  const deleteTodoHook = useDeleteTodo({
    onSuccess: deleteTodoUpdateTodosCache.cb,
    onRefetch: fetchTodosHook.onRefetch
  })

  const toggleTodoHook = useToggleTodo()

  const handleChangeLimit = (e: any) => {
    setVariables({
      ...GET_TODOS_VARIABLES,
      filters: {
        ...GET_TODOS_VARIABLES.filters,
        limit: parseInt(e.target.value)
      }
    })
  }

  return (
    <Todos
      onCreateTodo={createTodoHook.onCreate}
      onAddRandomTodo={createTodoHook.onRandomCreate}
      createTodoLoading={createTodoHook.loading}
      onDeleteTodo={deleteTodoHook.onDelete}
      onToggle={toggleTodoHook.onToggle}
      isFetchingMore={fetchTodosHook.isFetchingMore}
      hasMoreTodos={fetchTodosHook.hasMore}
      onLoadMoreTodos={fetchTodosHook.onLoadMore}
      onRefetch={fetchTodosHook.onRefetch}
      rows={fetchTodosHook.rows}
      totalRowsCount={fetchTodosHook.count || fetchTodosHook.rows.length}
      limit={GET_TODOS_VARIABLES.filters.limit as number}
      onChangeLimit={handleChangeLimit}
    />
  )
}

export default TodosContainer
