import React, { useState } from 'react'
import CardRouteUi from '../../components/CardRouteUi'
import { useSnackbar } from 'notistack'
import { CardElement, useElements, useStripe } from '@stripe/react-stripe-js'
import { useMutation, useQuery } from '@apollo/client'
import createSetupIntentMutation from '../../graphql/mutation/createSetupIntent'
import paymentMethodsListQuery from '../../graphql/query/paymentMethodsList'
import useCurrentUser from '../../hooks/useCurrentUser'
import stripeCustomerUpdateMutation from '../../graphql/mutation/stripeCustomerUpdate'
import paymentMethodDetachMutation from '../../graphql/mutation/paymentMethodDetach'

function CardRouteConnected() {
  const currentUser = useCurrentUser()
  const { enqueueSnackbar } = useSnackbar()

  const stripe = useStripe()
  const elements = useElements()

  const [loading, setLoading] = useState(false)

  const pmlq = useQuery(paymentMethodsListQuery)

  const paymentMethodsList = pmlq?.data?.paymentMethodsList || []

  const [createSetupIntentMutate] = useMutation(createSetupIntentMutation)
  const [stripeCustomerUpdateMutate] = useMutation(stripeCustomerUpdateMutation)
  const [paymentMethodDetachMutate] = useMutation(paymentMethodDetachMutation)

  const handleSubmit = async (e: any) => {
    try {
      setLoading(true)

      // We don't want to let default form submission happen here,
      // which would refresh the page.
      e.preventDefault()

      const cardElement = elements?.getElement(CardElement)

      if (!stripe || !elements || !cardElement) {
        // Stripe.js has not yet loaded.
        // Make sure to disable form submission until Stripe.js has loaded.
        return
      }

      const {
        data: {
          createSetupIntent: { client_secret }
        }
      } = await createSetupIntentMutate()

      const { paymentMethod } = await stripe.createPaymentMethod({
        type: 'card',
        card: cardElement,
        billing_details: {
          name: `${currentUser.user.firstName} ${currentUser.user.lastName}`
        }
      })

      const result = await stripe.confirmCardSetup(client_secret, {
        payment_method: paymentMethod?.id
      })

      if (result.error) {
        return enqueueSnackbar(result.error.message, { variant: 'error' })
      }

      await stripeCustomerUpdateMutate({
        variables: {
          input: {
            invoice_settings: {
              default_payment_method: paymentMethod?.id
            }
          }
        }
      })

      await pmlq.refetch()

      enqueueSnackbar('Success', { variant: 'success' })
    } catch (error: any) {
      enqueueSnackbar(error.message, { variant: 'error' })
    } finally {
      setLoading(false)
    }
  }

  const handleDelete = async ({ data }: any) => {
    try {
      await paymentMethodDetachMutate({
        variables: {
          input: {
            paymentMethodId: data.id
          }
        }
      })

      await pmlq.refetch()

      enqueueSnackbar('Success', { variant: 'success' })
    } catch (error: any) {
      enqueueSnackbar(error.message, { variant: 'error' })
    }
  }

  return (
    <CardRouteUi
      onSubmit={handleSubmit}
      submitLoading={loading}
      paymentMethods={paymentMethodsList}
      onDelete={handleDelete}
    />
  )
}

export default CardRouteConnected
