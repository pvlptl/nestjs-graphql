import React from 'react'
import Settings from '../../components/Settings'
import useCurrentUser from '../../hooks/useCurrentUser'
import useSaveUserSettings from '../../hooks/useSaveUserSettings'
import useUserAvatarUpload from '../../hooks/useUserAvatarUpload'
import { useMutation } from '@apollo/client'
import deleteCurrentUserAvatarMutation from '../../graphql/mutation/deleteCurrentUserAvatar'
import useUploadUserAvatar from '../../hooks/useUploadUserAvatar'
import useUploadUserAvatarUpdateCache from '../../hooks/useUploadUserAvatarUpdateCache'
import { UpdateUserByIdInput } from '../../graphql/generated'

function SettingsConnected() {
  const [deleteUserAvatarMutate] = useMutation(deleteCurrentUserAvatarMutation)
  const userAvatarUploadHook = useUserAvatarUpload()
  const currentUser = useCurrentUser()
  const saveUserSettingsHook = useSaveUserSettings()
  const uploadUserAvatarHook = useUploadUserAvatar()
  const uploadUserAvatarUpdateCache = useUploadUserAvatarUpdateCache()

  // todo set settings route as private
  if (!currentUser.user) return <div />

  const handleDelete = () => {
    if (currentUser.user.avatar) {
      deleteUserAvatarMutate()
    }
    userAvatarUploadHook.onReset()
  }

  const handleSubmit = async (data: UpdateUserByIdInput) => {
    saveUserSettingsHook.onSubmit(data)

    if (userAvatarUploadHook.file) {
      if (userAvatarUploadHook.src) {
        uploadUserAvatarUpdateCache.cb({ url: userAvatarUploadHook.src })
      }

      await uploadUserAvatarHook.cb({ file: userAvatarUploadHook.file })
      currentUser.onRefetch()
    }
  }

  return (
    <Settings
      isDeletable={Boolean(
        currentUser.user?.avatar || userAvatarUploadHook.src
      )}
      onDelete={handleDelete}
      onUpload={userAvatarUploadHook.onUpload}
      avatarSrc={userAvatarUploadHook.src || currentUser.user?.avatar?.url}
      onSubmit={handleSubmit}
      initialValues={{
        firstName: currentUser.user.firstName,
        lastName: currentUser.user.lastName,
        email: currentUser.user.email
      }}
    />
  )
}

export default SettingsConnected
