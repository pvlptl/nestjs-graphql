import React from 'react'
import { useNavigate } from 'react-router-dom'
import { Props } from './types'
import { useMutation, useQuery } from '@apollo/client'
import findCurrentUserTodoByIdMutation from '../../graphql/query/findCurrentUserTodoById'
import { CircularProgress } from '@mui/material'
import { Todo as TodoEntity } from '../../graphql/generated'
import Todo from '../../components/Todo'
import { InitialValues } from '../../components/Todo/types'
import updateTodoByIdMutation from '../../graphql/mutation/updateTodoById'
import { useSnackbar } from 'notistack'
import useDeleteTodo from '../../hooks/useDeleteTodo'
import useDeleteTodoUpdateTodosCache from '../../hooks/useDeleteTodoUpdateCache'
import { useRecoilValue } from 'recoil'
import getTodosVariablesSelector from '../../recoil/selectors/getTodosVariablesSelector'

function TodoContainer(props: Props) {
  const { id } = props

  const navigate = useNavigate()

  const { data: GET_TODOS_VARIABLES } = useRecoilValue(
    getTodosVariablesSelector
  )

  const deleteTodoUpdateTodosCache = useDeleteTodoUpdateTodosCache({
    variables: GET_TODOS_VARIABLES
  })

  const deleteTodoHook = useDeleteTodo({
    onSuccess: deleteTodoUpdateTodosCache.cb
  })

  const [mutate] = useMutation(updateTodoByIdMutation)
  const { enqueueSnackbar } = useSnackbar()

  const { data, loading, error } = useQuery(findCurrentUserTodoByIdMutation, {
    variables: {
      id
    }
  })

  const todo: TodoEntity = data?.findCurrentUserTodoById

  const handleDelete = () => {
    deleteTodoHook.onDelete(todo)
    navigate('/todos')
  }

  const handleSubmit = (data: InitialValues) => {
    try {
      mutate({
        variables: {
          id,
          data
        },
        optimisticResponse: () => {
          return {
            updateTodoById: {
              ...todo,
              ...data
            }
          }
        }
      })
      enqueueSnackbar('Success', { variant: 'success' })
    } catch (error: any) {
      enqueueSnackbar(error.message, { variant: 'error' })
    }
  }

  if (error) {
    return <div>{error.message}</div>
  }

  if (loading || !todo) {
    return <CircularProgress />
  }

  return (
    <Todo
      onDelete={handleDelete}
      initialValues={{ name: todo.name, isCompleted: todo.isCompleted }}
      onSubmit={handleSubmit}
    />
  )
}

export default TodoContainer
