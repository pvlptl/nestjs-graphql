import React, { useState } from 'react'
import Balance from '../../components/Balance'
import { useMutation, useQuery } from '@apollo/client'
import createChargeMutation from '../../graphql/mutation/createCharge'
import { get } from 'lodash/fp'
import { useSnackbar } from 'notistack'
import paymentMethodsListQuery from '../../graphql/query/paymentMethodsList'
import stripeCustomerQuery from '../../graphql/query/stripeCustomer'

function BalanceRouteConnected() {
  const { enqueueSnackbar } = useSnackbar()

  const [loading, setLoading] = useState(false)

  const SC = useQuery(stripeCustomerQuery)
  const pmlq = useQuery(paymentMethodsListQuery)
  const paymentMethodsList = pmlq?.data?.paymentMethodsList || []
  const hasPaymentMethods = paymentMethodsList.length > 0

  const [createCharge, { loading: mutationLoading }] =
    useMutation(createChargeMutation)

  const handleSubmit = async ({ event, amount }: any) => {
    try {
      setLoading(true)

      // We don't want to let default form submission happen here,
      // which would refresh the page.
      event.preventDefault()

      const res = await createCharge({
        variables: {
          input: {
            paymentMethodId: paymentMethodsList[0]?.id,
            amount: parseFloat(amount)
          }
        }
      })

      const redirect_to_url = get(
        ['data', 'createCharge', 'redirect_to_url'],
        res
      )

      if (redirect_to_url) {
        return window.location.replace(redirect_to_url)
      }

      await SC.refetch()

      enqueueSnackbar('Success', { variant: 'success' })
    } catch (error: any) {
      enqueueSnackbar(error.message, { variant: 'error' })
    } finally {
      setLoading(false)
    }
  }

  return (
    <Balance
      hasPaymentMethods={hasPaymentMethods}
      onSubmit={handleSubmit}
      submitLoading={loading || mutationLoading}
    />
  )
}

export default BalanceRouteConnected
